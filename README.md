# README #

Refer to this README for how to setup Git on your Visual Studio Workspace.

### What is this repository for? ###

* HKeInvest Web Application Project for COMP3111 Course
* Preliminary Version 1.0 (Current)


### How do I get set up? ###

* [Github Guideline on Repo/Branch/Pull](https://guides.github.com/activities/hello-world/)
* [CodeProject Guideline on Git setup in Visual Studio](http://www.codeproject.com/Tips/900204/Gettingstarted-with-GIT-Visual-Studio-and-BitBuck)
* [MSDN Guideline on Git setup in Visual Studio](https://msdn.microsoft.com/en-us/library/hh850445.aspx)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact