﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HKeInvestWebApplication;
using NUnit.Core;
using NUnit.Framework;
using NUnit.Core.Extensibility;
using System.Data;

namespace HKeInvestWebApplication.Tests.ClientTest
{
    [TestClass]
    public class ProfitLossTrackingTest
    {
        HKeInvestWebApplication.Client.ProfitLossTracking plTest = new HKeInvestWebApplication.Client.ProfitLossTracking();
        [TestMethod]
        public void Test_addRowInDT()
        {
            DataTable originalTable = new DataTable();
            DataColumn col = new DataColumn();
            col.DataType = System.Type.GetType("System.String");
            col.ColumnName = "type";
            col.AutoIncrement = false;
            col.Caption = "type";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            col = new DataColumn();
            col.DataType = System.Type.GetType("System.String");
            col.ColumnName = "code";
            col.AutoIncrement = false;
            col.Caption = "code";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            col = new DataColumn();
            col.DataType = System.Type.GetType("System.String");
            col.ColumnName = "name";
            col.AutoIncrement = false;
            col.Caption = "name";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);


            col = new DataColumn();
            col.DataType = System.Type.GetType("System.Decimal");
            col.ColumnName = "shares";
            col.AutoIncrement = false;
            col.Caption = "shares";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            col = new DataColumn();
            col.DataType = System.Type.GetType("System.Decimal");
            col.ColumnName = "totalbuyamount";
            col.AutoIncrement = false;
            col.Caption = "totalbuyamount";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            col = new DataColumn();
            col.DataType = System.Type.GetType("System.Decimal");
            col.ColumnName = "totalsellamount";
            col.AutoIncrement = false;
            col.Caption = "totalsellamount";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            col = new DataColumn();
            col.DataType = System.Type.GetType("System.Decimal");
            col.ColumnName = "totalfeespaid";
            col.AutoIncrement = false;
            col.Caption = "totalfeespaid";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            col = new DataColumn();
            col.DataType = System.Type.GetType("System.String");
            col.ColumnName = "totalpl";
            col.AutoIncrement = false;
            col.Caption = "totalpl";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            

            Object obj = TestUtilities.RunInstanceMethod(typeof(HKeInvestWebApplication.Client.ProfitLossTracking), "addRowInDT", plTest, new Object[10] {originalTable, "bond","1111","ABCBond", (decimal)10, (decimal)20, (decimal)30, (decimal)40, (decimal)5, (decimal)6 });
            DataRow row = originalTable.Rows[0];
            NUnit.Framework.Assert.AreEqual("bond",row["type"]);
            NUnit.Framework.Assert.AreEqual("1111", row["code"]);
            NUnit.Framework.Assert.AreEqual("ABCBond", row["name"]);

        }

        [TestMethod]
        public void Test_addRowInTypeDT()
        {
            DataTable originalTable = new DataTable();
            DataColumn col = new DataColumn();
            col.DataType = System.Type.GetType("System.String");
            col.ColumnName = "type";
            col.AutoIncrement = false;
            col.Caption = "type";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            col = new DataColumn();
            col.DataType = System.Type.GetType("System.Decimal");
            col.ColumnName = "typetotalbuyamount";
            col.AutoIncrement = false;
            col.Caption = "typetotalbuyamount";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            col = new DataColumn();
            col.DataType = System.Type.GetType("System.Decimal");
            col.ColumnName = "typetotalsellamount";
            col.AutoIncrement = false;
            col.Caption = "typetotalsellamount";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            col = new DataColumn();
            col.DataType = System.Type.GetType("System.Decimal");
            col.ColumnName = "typetotalfeespaid";
            col.AutoIncrement = false;
            col.Caption = "typetotalfeespaid";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            col = new DataColumn();
            col.DataType = System.Type.GetType("System.String");
            col.ColumnName = "typetotalpl";
            col.AutoIncrement = false;
            col.Caption = "typetotalpl";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            Object obj = TestUtilities.RunInstanceMethod(typeof(HKeInvestWebApplication.Client.ProfitLossTracking), "addRowInTypeDT", plTest, new Object[7] { originalTable, "bond", (decimal)10, (decimal)20, (decimal)30, (decimal)40, (decimal)50 });
            DataRow row = originalTable.Rows[0];
            NUnit.Framework.Assert.AreEqual("bond", row["type"]);
            NUnit.Framework.Assert.AreEqual(10, row["typetotalbuyamount"]);
            NUnit.Framework.Assert.AreEqual(20, row["typetotalsellamount"]);

        }
    }
}
