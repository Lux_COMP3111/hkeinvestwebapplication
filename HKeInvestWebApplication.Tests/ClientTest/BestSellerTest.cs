﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HKeInvestWebApplication;
using NUnit.Core;
using NUnit.Framework;
using NUnit.Core.Extensibility;
using System.Data;
using System.IO;

namespace HKeInvestWebApplication.Tests.ClientTest
{
    [TestClass]
    public class BestSellerTest
    {
        HKeInvestWebApplication.Client.BestSeller BSTest = new HKeInvestWebApplication.Client.BestSeller();
        HKeInvestWebApplication.Code_File.HKeInvestService service = new HKeInvestWebApplication.Code_File.HKeInvestService();

        [TestMethod]
        public void Test_calculateHKD()
        {
            //var appDataDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin", ""), "App_Data");
            //AppDomain.CurrentDomain.SetData("DataDirectory", appDataDirectory);

            DataRow nullRow = null;
            object BSObj = TestUtilities.RunInstanceMethod(typeof(HKeInvestWebApplication.Client.BestSeller), "calculateHKD", BSTest, new object[1] { nullRow });
            NUnit.Framework.Assert.AreEqual(null, nullRow);

            DataTable originalTable = new DataTable();
            DataColumn col = new DataColumn();
            col.DataType = System.Type.GetType("System.String");
            col.ColumnName = "type";
            col.AutoIncrement = false;
            col.Caption = "type";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            col = new DataColumn();
            col.DataType = System.Type.GetType("System.String");
            col.ColumnName = "code";
            col.AutoIncrement = false;
            col.Caption = "code";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            col = new DataColumn();
            col.DataType = System.Type.GetType("System.Decimal");
            col.ColumnName = "highPrice";
            col.AutoIncrement = false;
            col.Caption = "highPrice";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            col = new DataColumn();
            col.DataType = System.Type.GetType("System.Decimal");
            col.ColumnName = "lowPrice";
            col.AutoIncrement = false;
            col.Caption = "lowPrice";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            col = new DataColumn();
            col.DataType = System.Type.GetType("System.Decimal");
            col.ColumnName = "volume";
            col.AutoIncrement = false;
            col.Caption = "volume";
            col.ReadOnly = false;
            col.Unique = false;
            originalTable.Columns.Add(col);

            DataRow row = originalTable.NewRow();
            row["type"] = "stock";
            row["code"] = "2000";
            row["highPrice"] = "20";
            row["lowPrice"] = "10";
            row["volume"] = "15";
            originalTable.Rows.Add(row);

            DataTable testTable1 = originalTable.Copy();
            DataRow originalRow1 = originalTable.Rows[0];
            DataRow testRow1 = testTable1.Rows[0];

            BSObj = TestUtilities.RunInstanceMethod(typeof(HKeInvestWebApplication.Client.BestSeller), "calculateHKD", BSTest, new object[1] { testRow1 });
            NUnit.Framework.Assert.AreEqual(originalRow1["highPrice"], testRow1["highPrice"]);
            NUnit.Framework.Assert.AreEqual(originalRow1["lowPrice"], testRow1["lowPrice"]);
            NUnit.Framework.Assert.AreEqual(originalRow1["volume"], testRow1["volume"]);

            originalRow1["type"] = "bond";
            originalRow1["code"] = "281";
            testTable1 = originalTable.Copy();
            testRow1 = testTable1.Rows[0];
            BSObj = TestUtilities.RunInstanceMethod(typeof(HKeInvestWebApplication.Client.BestSeller), "calculateHKD", BSTest, new object[1] { testRow1 });
            NUnit.Framework.Assert.AreNotEqual(originalRow1["highPrice"], testRow1["highPrice"]);
            NUnit.Framework.Assert.AreNotEqual(originalRow1["lowPrice"], testRow1["lowPrice"]);
            NUnit.Framework.Assert.AreNotEqual(originalRow1["volume"], testRow1["volume"]);

            decimal rate = service.getCurrency("bond", "281");
            NUnit.Framework.Assert.AreEqual(Math.Round(20 * rate, 2), testRow1["highPrice"]);
            NUnit.Framework.Assert.AreEqual(Math.Round(10 * rate, 2), testRow1["lowPrice"]);
            NUnit.Framework.Assert.AreEqual(Math.Round(15* rate, 2), testRow1["volume"]);


            
        }
        
    }
}
