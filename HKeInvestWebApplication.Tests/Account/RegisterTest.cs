﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HKeInvestWebApplication;
using NUnit.Core;
using NUnit.Framework;
using NUnit.Core.Extensibility;
using System.Data;

namespace HKeInvestWebApplication.Tests.Account
{
    [TestClass]
    public class RegisterTest
    {
        HKeInvestWebApplication.Account.Register rgTest = new HKeInvestWebApplication.Account.Register();
        [TestMethod]
        public void Test_Add_Username_to_Account()
        {
            HKeInvestWebApplication.Code_File.HKeInvestData data = new HKeInvestWebApplication.Code_File.HKeInvestData();
            DataTable dt = new DataTable();

            string acNo = "YU00000001";
            object BSObj = TestUtilities.RunInstanceMethod(typeof(HKeInvestWebApplication.Account.Register), "Add_Username_to_Account", rgTest, new object[2] { acNo, "abcdef" });

            DataTable result = data.getData("select [userName] from [Account] where [accountNumber] = '"+acNo.Trim() +"';");
            DataRow row = result.Rows[0];
            NUnit.Framework.Assert.AreEqual("abcdef", row["userName"].ToString().Trim());
            
        }

        [TestMethod]
        public void Test_check_Client_serverSide()
        {
            //NUnit.Framework.Assert.AreEqual();
            bool result = Convert.ToBoolean(TestUtilities.RunInstanceMethod(typeof(HKeInvestWebApplication.Account.Register), "check_Client_serverSide", rgTest, new Object[6] {"Yuki", "Yuen", "10/1/1960","comp3111_team122@cse.ust.hk", "A5647825", "YU00000001"}));
            NUnit.Framework.Assert.AreEqual(true, result);

            result = Convert.ToBoolean(TestUtilities.RunInstanceMethod(typeof(HKeInvestWebApplication.Account.Register), "check_Client_serverSide", rgTest, new Object[6] { "Yu", "Yen", "10/1/1960", "comp3m12k", "A5647826", "001" }));
            NUnit.Framework.Assert.AreNotEqual(true, result);

        }
    }
}
