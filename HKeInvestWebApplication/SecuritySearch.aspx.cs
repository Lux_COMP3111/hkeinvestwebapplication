﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HKeInvestWebApplication.Code_File;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using System.Data;

namespace HKeInvestWebApplication
{
    public partial class SecuritySearch : System.Web.UI.Page
    {
        HKeInvestCode myHKeInvestCode = new HKeInvestCode();
        ExternalFunctions myExternalFunctions = new ExternalFunctions();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void ddlSearchBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblErrorMsg.Visible = false;
            lblSearchResult.Visible = false;
               
            if (ddlSearchBy.SelectedValue=="name")
            {
                pnName.Enabled = true;
                pnName.Visible = true;
                pnCode.Enabled = false;
                pnCode.Visible = false;
            }
            else if (ddlSearchBy.SelectedValue == "code")
            {
                pnCode.Enabled = true;
                pnCode.Visible = true;
                pnName.Enabled = false;
                pnName.Visible = false;
            }
            else
            {
                pnName.Enabled = false;
                pnName.Visible = false;
                pnCode.Enabled = false;
                pnCode.Visible = false;
                if(ddlSearchBy.SelectedValue=="all")
                {
                    string securityType = ddlSecurityType.SelectedValue;
                    if (securityType=="0")
                    {
                        lblErrorMsg.Text = "Please Select the Security Type";
                        lblErrorMsg.Visible = true;
                    }
                    else
                    {
                        searchAll(securityType);
                    }
                }
            }

        }
        protected void btnCodeSearch_Click(object sender, EventArgs e)
        {
            lblErrorMsg.Visible = false;
            lblSearchResult.Visible = false;
            string code = txtCode.Text.Trim();
            if (code=="")
            {
                lblErrorMsg.Text = "Please Insert Security Product Code";
                lblErrorMsg.Visible = true;
                return;
            }
            string securityType = ddlSecurityType.SelectedValue;
            if (securityType == "0")
            {
                lblErrorMsg.Text = "Please Select the Security Type";
                lblErrorMsg.Visible = true;
                return;
            }
            searchCode(securityType, code);
        }

        protected void btnNameSearch_Click(object sender, EventArgs e)
        {
            lblErrorMsg.Visible = false;
            lblSearchResult.Visible = false;
            string name = txtName.Text.Trim();
            if (name == "")
            {
                lblErrorMsg.Text = "Please Insert Security Product Name";
                lblErrorMsg.Visible = true;
                return;
            }
            string securityType = ddlSecurityType.SelectedValue;
            if (securityType == "0")
            {
                lblErrorMsg.Text = "Please Select the Security Type";
                lblErrorMsg.Visible = true;
                return;
            }
            searchName(securityType, name);
        }

        private void searchAll(string securityType)
        {
            DataTable dtSearchAll = myExternalFunctions.getSecuritiesData(securityType);
            lblSearchResult.Text = "Search of all records of Security Type: " + securityType;
            lblSearchResult.Visible = true;
            printResult(securityType, dtSearchAll);
            return;
        }

        private void searchCode (string securityType, string code)
        {
            DataTable dtSearchAll = myExternalFunctions.getSecuritiesData(securityType);
            DataView dvSearchCode = new DataView(dtSearchAll);
            dvSearchCode.RowFilter = "code = '" + code + "'";
            DataTable dtSearchCode = dvSearchCode.ToTable();
            lblSearchResult.Text = "Search of all records of security type: " + securityType + " with code: " + code;
            lblSearchResult.Visible = true;
            printResult(securityType, dtSearchCode);
            return;
        }

        private void searchName (string securityType, string name)
        {
            DataTable dtSearchAll = myExternalFunctions.getSecuritiesData(securityType);
            DataView dvSearchName = new DataView(dtSearchAll);
            dvSearchName.RowFilter = "name like '%" + name + "%'";
            DataTable dtSearchName = dvSearchName.ToTable();
            lblSearchResult.Text = "Search of all records of security type: " + securityType + " with name: " + name;
            lblSearchResult.Visible = true;
            printResult(securityType, dtSearchName);
            return;
        }

        private void printResult (string securityType, DataTable dtSearchResult)
        {
            
            if (dtSearchResult == null) { return; } // If the DataSet is null, a SQL error occurred.

            // If no result is returned, then display a message that the account does not hold this type of security.
            if (dtSearchResult.Rows.Count == 0)
            {
                lblErrorMsg.Text = "No result found on the server. Please Check your search inputs";
                lblErrorMsg.Visible = true;
                gvBondResult.Visible = false;
                gvStockResult.Visible = false;
                gvUnitTrustResult.Visible = false;
                return;

            }
            if (securityType == "bond")
            {
                gvBondResult.DataSource = dtSearchResult;
                gvBondResult.DataBind();
                gvBondResult.HeaderRow.TableSection = TableRowSection.TableHeader;
                gvStockResult.Dispose();
                gvStockResult.Visible = false;
                gvUnitTrustResult.Dispose();
                gvUnitTrustResult.Visible = false;
                gvBondResult.Visible = true;
            }
            else if (securityType == "stock")
            {
                gvStockResult.DataSource = dtSearchResult;
                gvStockResult.DataBind();
                gvBondResult.Dispose();
                gvBondResult.Visible = false;
                gvUnitTrustResult.Dispose();
                gvUnitTrustResult.Visible = false;
                gvStockResult.Visible = true;
            }
            else
            {
                gvUnitTrustResult.DataSource = dtSearchResult;
                gvUnitTrustResult.DataBind();
                gvBondResult.Dispose();
                gvBondResult.Visible = false;
                gvStockResult.Dispose();
                gvStockResult.Visible = false;
                gvUnitTrustResult.Visible = true;
            }
        }

        protected void ddlSecurityType_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblErrorMsg.Visible = false;
            lblSearchResult.Visible = false;
            pnCode.Visible = false;
            pnName.Visible = false;
            ddlSearchBy.ClearSelection();
            gvBondResult.Dispose();
            gvBondResult.Visible = false;
            gvStockResult.Dispose();
            gvStockResult.Visible = false;
            gvUnitTrustResult.Dispose();
            gvUnitTrustResult.Visible = false;
        }

        protected void gvBondResult_Sorting(object sender, GridViewSortEventArgs e)
        {
            
        }
    }
}