﻿<%@ Master Language="C#" AutoEventWireup="true" CodeBehind="Site.master.cs" Inherits="HKeInvestWebApplication.SiteMaster" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%: Page.Title %> - HKeInvest LLC</title>

    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />

</head>
<body>
    <form runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--To learn more about bundling scripts in ScriptManager see http://go.microsoft.com/fwlink/?LinkID=301884 --%>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="respond" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" runat="server" href="~/">HKeInvest LLC</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Accounts
                            <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a runat="server" href="#">Securities Portfolio Account</a></li>
                                <li><a runat="server" href="~/AccountApplicationForm.pdf">Securities Account Application <span class="label label-danger">pdf</span></a></li> 
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Securities
                            <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a runat="server" href="~/SecuritySearch">Search Securities</a></li>
                                <li><a runat="server" href="~/Client/BestSeller.aspx">Monthly Best Sellers <span class="label label-danger">client only</span></a></li> 
                            </ul>
                        </li>
                        <asp:LoginView runat="server" ViewStateMode="Disabled">
                            <RoleGroups>
                                <asp:RoleGroup Roles="Client">
                                    <ContentTemplate>
                                        <li><a runat="server" href="~/Client/TradeSecurity.aspx">Trading</a></li>
                                        <li class="dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Client Support Tools
                                            <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a runat="server" href="~/Client/ProfitLossTracking.aspx">Profit/Loss Tracking</a></li>
                                                <li><a runat="server" href="~/Client/CurrencyConverter">Currency Conversion</a></li>
                                                <li><a runat="server" href="~/Client/Alert.aspx">Security Alarm Setting</a></li>
                                            </ul>
                                        </li>
                                        <li><a runat="server" href="~/Client/ReportGeneration.aspx">Account Report</a></li>
                                    </ContentTemplate>
                                </asp:RoleGroup>
                                <asp:RoleGroup Roles="Employee">
                                    <ContentTemplate>
                                        <li class="dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Client Management
                                            <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a runat="server" href="~/Employee/AccountApplicationPage.aspx">Account Application Process</a></li>
                                                <li><a runat="server" href="~/Employee/ReportGeneration.aspx">Client Account Report</a></li>
                                                <li><a runat="server" href="#">Client Information Management</a></li>
                                            </ul>
                                        </li>
                                    </ContentTemplate>
                                </asp:RoleGroup>
                            </RoleGroups>
                        </asp:LoginView>
                        <li><a runat="server" href="~/About.aspx">About</a></li>
                    </ul>
                    <asp:LoginView runat="server" ViewStateMode="Disabled">
                        <AnonymousTemplate>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a runat="server" href="~/Account/Register"><span class="glyphicon glyphicon-pencil"></span> Register</a></li>
                                <li><a runat="server" href="~/Account/Login"><span class="glyphicon glyphicon-log-in"></span> Log in</a></li>
                            </ul>
                        </AnonymousTemplate>
                        <RoleGroups>
                            <asp:RoleGroup Roles="Client">
                                <ContentTemplate>
                                    <ul class="nav navbar-nav navbar-right">
                                        <li><a runat="server" href="~/Client/Dashboard.aspx"><span class="glyphicon glyphicon-user"></span> Client Dashboard</a></li>
                                        <li>
                                            <asp:LoginStatus runat="server" LogoutAction="Redirect" LogoutText="Log off" LogoutPageUrl="~/" OnLoggingOut="Unnamed_LoggingOut" />
                                        </li>
                                    </ul>
                                </ContentTemplate>
                            </asp:RoleGroup>
                            <asp:RoleGroup Roles="Employee">
                                <ContentTemplate>
                                    <ul class="nav navbar-nav navbar-right">
                                        <li><a runat="server" href="~/Employee/EmployeeMain.aspx"><span class="glyphicon glyphicon-user"></span> Employee Dashboard</a></li>
                                        <li>
                                            <asp:LoginStatus runat="server" LogoutAction="Redirect" LogoutText="Log off" LogoutPageUrl="~/" OnLoggingOut="Unnamed_LoggingOut" />
                                        </li>
                                    </ul>
                                </ContentTemplate>
                            </asp:RoleGroup>
                        </RoleGroups>
                    </asp:LoginView>
                </div>
            </div>
        </div>
        <div class="container body-content">
            <asp:ContentPlaceHolder ID="MainContent" runat="server">
            </asp:ContentPlaceHolder>
            <hr />
            <footer>
                <p>&copy; <%: DateTime.Now.Year %> - Team Lux (122)</p>
            </footer>
        </div>
    </form>
</body>
</html>
