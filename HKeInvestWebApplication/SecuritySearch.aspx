﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SecuritySearch.aspx.cs" Inherits="HKeInvestWebApplication.SecuritySearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>HKeInvest: Security Search</h2>
    <%--Largest Div Element--%>
    <div class ="form-horizontal panel-group">
        <%--SecurityType ddlist and SearchBy ddlist div--%>
        <asp:Panel runat="server" ID="pnDdlists" CssClass="panel panel-default">
            <div class="form-group panel-body">
                <asp:Label runat="server" AssociatedControlID="ddlSecurityType" Text="Security Type" CssClass="control-label col-md-2"></asp:Label>
                <%--Security Type ddlist--%>
                <div class="col-md-4">
                    <asp:DropDownList runat="server" ID="ddlSecurityType" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlSecurityType_SelectedIndexChanged">
                        <asp:ListItem Value="0">-- Select Security Type --</asp:ListItem>
                        <asp:ListItem Value="bond">Bond</asp:ListItem>
                        <asp:ListItem Value="unit trust">Unit Trust</asp:ListItem>
                        <asp:ListItem Value="stock">Stock</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <asp:Label runat="server" AssociatedControlID="ddlSearchBy" Text="Search By" CssClass="control-label col-md-2"></asp:Label>
                <%--Search By ddlist--%>
                <div class="col-md-4">
                    <asp:DropDownList runat="server" ID="ddlSearchBy" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlSearchBy_SelectedIndexChanged">
                        <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                        <asp:ListItem Value="all">Search All</asp:ListItem>
                        <asp:ListItem Value="name">Name</asp:ListItem>
                        <asp:ListItem Value="code">Code</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </asp:Panel>
        <%--SearchBy Name Panel--%>
        <asp:Panel runat="server" ID="pnName" Enabled="false" Visible="false" CssClass="panel panel-default">
            <div class="panel-heading">Search By Security Product Name</div>
            <div class="form-group panel-body">
                <asp:Label runat="server" AssociatedControlID="txtName" Text="Security Product Name:" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-4">
                    <asp:TextBox runat="server" ID="txtName" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-5"><asp:Button runat="server" ID="btnNameSearch" Text="Search" CssClass="btn btn-primary" OnClick="btnNameSearch_Click" /></div>
            </div>
        </asp:Panel>
        <%--SearchBy Code Panel--%>
        <asp:Panel runat="server" ID="pnCode" Enabled="false" Visible="false" CssClass="panel panel-default">
            <div class="panel-heading">Search By Security Product Code</div>
            <div class="form-group panel-body">
                <asp:Label runat="server" AssociatedControlID="txtCode" Text="Security Product Code:" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-4">
                    <asp:TextBox runat="server" ID="txtCode" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-5"><asp:Button runat="server" ID="btnCodeSearch" Text="Search" CssClass="btn btn-primary" OnClick="btnCodeSearch_Click" /></div>
            </div>
        </asp:Panel>
        <%--Search Result Gridview Panel--%>
        <asp:Panel runat="server" ID="pnResult" CssClass="panel panel-default">
            <div class="panel-heading">Search Result</div>
            <div class="panel-body">
                <div class="form-group col-md-12">
                    <asp:Label runat="server" ID="lblSearchResult" CssClass="label label-primary" Visible="false"></asp:Label>
                    <asp:Label runat="server" ID="lblErrorMsg" CssClass="label label-danger" Visible="false"></asp:Label>
                </div>
                <div class="form-group col-md-12">
                    <asp:GridView runat="server" ID="gvBondResult" AutoGenerateColumns="False" Visible="false" CssClass="table table-bordered table-condensed table-hover" AllowSorting="true" OnSorting="gvBondResult_Sorting" >
                    <Columns>
                        <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="True" SortExpression="code"/>
                        <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True" SortExpression="name"/>
                        <asp:BoundField DataField="launchDate" HeaderText="Launch Date" ReadOnly="True" />
                        <asp:BoundField DataField="base" HeaderText="Currency" ReadOnly="True" />
                        <asp:BoundField DataField="size" HeaderText="Size" ReadOnly="True" />
                        <asp:BoundField DataField="price" HeaderText="Price" ReadOnly="True" />
                        <asp:BoundField DataField="sixMonths" HeaderText="Last 6 Months" ReadOnly="True" />
                        <asp:BoundField DataField="oneYear" HeaderText="Last 1 Year" ReadOnly="True" />
                        <asp:BoundField DataField="threeYears" HeaderText="Last 3 Years" ReadOnly="True" />
                        <asp:BoundField DataField="sinceLaunch" HeaderText="Since Launch" ReadOnly="True" />
                    </Columns>
                </asp:GridView>
                <asp:GridView runat="server" ID="gvStockResult" AutoGenerateColumns="False" Visible="False" CssClass="table">
                    <Columns>
                        <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="True" />
                        <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True" SortExpression="name" />
                        <asp:BoundField DataField="close" HeaderText="Recent Closing Price" ReadOnly="True" />
                        <asp:BoundField DataField="changePercent" HeaderText="Last % Change" ReadOnly="True" />
                        <asp:BoundField DataField="changeDollar" HeaderText="Last HKD Change" ReadOnly="True" />
                        <asp:BoundField DataField="volume" HeaderText="Last Traded Volume" ReadOnly="True" />
                        <asp:BoundField DataField="high" HeaderText="Annual High" ReadOnly="True" />
                        <asp:BoundField DataField="low" HeaderText="Annual Low" ReadOnly="True" />
                        <asp:BoundField DataField="peRatio" HeaderText="P/E Ratio" ReadOnly="True" />
                        <asp:BoundField DataField="yield" HeaderText="Yield" ReadOnly="True" />
                    </Columns>
                </asp:GridView>
                <asp:GridView runat="server" ID="gvUnitTrustResult" AutoGenerateColumns="False" Visible="false" CssClass="table">
                    <Columns>
                        <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="True" />
                        <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True" SortExpression="name" />
                        <asp:BoundField DataField="launchDate" HeaderText="Launch Date" ReadOnly="True" />
                        <asp:BoundField DataField="base" HeaderText="Currency" ReadOnly="True" />
                        <asp:BoundField DataField="size" HeaderText="Size" ReadOnly="True" />
                        <asp:BoundField DataField="price" HeaderText="Price" ReadOnly="True" />
                        <asp:BoundField DataField="sixMonths" HeaderText="Last 6 Months" ReadOnly="True" />
                        <asp:BoundField DataField="oneYear" HeaderText="Last 1 Year" ReadOnly="True" />
                        <asp:BoundField DataField="threeYears" HeaderText="Last 3 Years" ReadOnly="True" />
                        <asp:BoundField DataField="sinceLaunch" HeaderText="Since Launch" ReadOnly="True" />
                    </Columns>
                </asp:GridView>
                </div>
                
            </div>
        </asp:Panel>
    </div>
</asp:Content>