﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using HKeInvestWebApplication.Code_File;
using HKeInvestWebApplication.ExternalSystems.Code_File;

namespace HKeInvestWebApplication.Code_File
{
    public class HKeInvestService
    {
        HKeInvestCode myHKeInvestCode = new HKeInvestCode();
        HKeInvestData myHKeInvestData = new HKeInvestData();
        ExternalFunctions myExternalFunctions = new ExternalFunctions();

        //Method to update the invoice table with initial order status only. The inserted data will be altered by future transactions.
        public void updateOrderInitial(string accountNumber, string referenceNumber, string buyOrSell, string securityCode, string securityType, string amount, string stockOrderType = "", string expiryDay = "", string allOrNone = "", string limitPrice = "", string stopPrice = "")
        {
            DataTable dtSecurity = myExternalFunctions.getSecuritiesByCode(securityType, securityCode);
            dtSecurity = dtSecurity.Select("[code] = " + securityCode).CopyToDataTable();
            string securityName;
            if (dtSecurity == null) return;
            else
            {
                securityName = dtSecurity.Rows[0]["name"].ToString().Trim();
            }
            string submitDate = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt");
            string sql;
            if (securityType == "stock")
            {
                sql = "Insert into [Invoice] values ('" + accountNumber + "', '" + referenceNumber + "', '0', '" + securityCode + "', '" + securityName + "', '" + securityType + "', '" + buyOrSell + "', '" + submitDate + "', 'pending' ,'" + amount + "', '" + stockOrderType + "', '" + expiryDay + "','" +allOrNone + "', null, null, null, null, null, '0')";
                SqlTransaction trans = myHKeInvestData.beginTransaction();
                myHKeInvestData.setData(sql, trans);
                myHKeInvestData.commitTransaction(trans);

                if (stockOrderType == "limit") sql = "update [Invoice] set [limitPrice] = '" + limitPrice + "' where [referenceNumber] = '" + referenceNumber + "'";
                else if (stockOrderType == "stop") sql = "update [Invoice] set [stopPrice] = '" + stopPrice + "' where [referenceNumber] = '" + referenceNumber + "'";
                else if (stockOrderType == "stop limit") sql = "update [Invoice] set [limitPrice] = '" + limitPrice + "', [stopPrice] = '" + stopPrice + "' where [referenceNumber] = '" + referenceNumber + "'";
                else return;
                trans = myHKeInvestData.beginTransaction();
                myHKeInvestData.setData(sql, trans);
                myHKeInvestData.commitTransaction(trans);
            }
            else
            {
                sql = "Insert into [Invoice] values ('" + accountNumber + "', '" + referenceNumber + "', '0', '" + securityCode + "', '" + securityName + "', '" + securityType + "', '" + buyOrSell + "', '" + submitDate + "', 'pending','" + amount + "', null, null, null, null, null, null, null, null, '0')";
                SqlTransaction trans = myHKeInvestData.beginTransaction();
                myHKeInvestData.setData(sql, trans);
                myHKeInvestData.commitTransaction(trans);

            }
        }

        /** Function Implemented to update the Invoice Table regularly. Update Database Function will be implemented to trigger all the update functions. **/
        //Start update Invoice Table: Create Datatable of distinct referenceNumber of Orders with 'pending' status and start Query.
        public void queryInvoiceStatus()
        {
            DataTable dtOrdersToQuery = myHKeInvestData.getData("select distinct referenceNumber from Invoice where [serviceFee] = 0 and not [status] = 'completed'");
            if (dtOrdersToQuery == null || dtOrdersToQuery.Rows.Count == 0) return;
            else
            {
                queryOrders(dtOrdersToQuery);
            }
        }

        //Query Order Status and see if the status is updated. If so, starts Query on Transactions.
        public void queryOrders(DataTable dtOrdersToQuery)
        {
            if (dtOrdersToQuery == null || dtOrdersToQuery.Rows.Count == 0) return;
            else
            {
                foreach (DataRow row in dtOrdersToQuery.Rows)
                {
                    string referenceNumber = row["referenceNumber"].ToString().Trim();
                    string status = myExternalFunctions.getOrderStatus(referenceNumber).Trim();
                    if (status == "pending")
                    {
                        row.Delete();
                    }
                    else continue;
                }
                dtOrdersToQuery.AcceptChanges();
                queryTransactions(dtOrdersToQuery);
            }
        }

        //Query the transaction records for orders that requires update, triggers UpdateInvoice method to update individual orders.
        public void queryTransactions(DataTable dtTransToQuery)
        {
            if (dtTransToQuery == null || dtTransToQuery.Rows.Count == 0) return;
            else
            {
                foreach (DataRow row in dtTransToQuery.Rows)
                {
                    string referenceNumber = row["referenceNumber"].ToString().Trim();
                    DataTable dtTransToUpdate = myExternalFunctions.getOrderTransaction(referenceNumber);
                    if (dtTransToUpdate == null || dtTransToUpdate.Rows.Count == 0) continue;
                    else
                    {
                        updateInvoice(referenceNumber, dtTransToUpdate);
                    }
                }
            }
        }

        //Query to update the Invoice Table : End of updating Invoice Table
        public void updateInvoice(string referenceNumber, DataTable dtTransToUpdate)
        {
            string status = myExternalFunctions.getOrderStatus(referenceNumber).Trim();
            string sql = "";
            int transCount = dtTransToUpdate.Rows.Count;
            int transCountInvoice = Convert.ToInt32(myHKeInvestData.getAggregateValue("select count([transactionNumber]) from [Invoice] where [referenceNumber] = '" + referenceNumber + "'"));
            SqlTransaction trans;
            //if invoice is updated with one transaction
            if (transCount == transCountInvoice)
            {
                foreach (DataRow row in dtTransToUpdate.Rows)
                {
                    sql = "Update [Invoice] set [transactionNumber] = '" + row["transactionNumber"].ToString().Trim() + "', [executeDate] = '" + row["executeDate"].ToString().Trim() + "', [executeShares] = '" + row["executeShares"].ToString().Trim() + "', [executePrice] = '" + row["executePrice"].ToString().Trim() + "', [status] ='" + status + "' where [referenceNumber] = '" + referenceNumber + "' and ([transactionNumber] = '" + row["transactionNumber"].ToString().Trim() +"' or [transactionNumber] = '0')";
                    trans = myHKeInvestData.beginTransaction();
                    myHKeInvestData.setData(sql, trans);
                    myHKeInvestData.commitTransaction(trans);
                }
            }
            //if invoice is updated with multiple transactions
            else
            {
                DataTable dtTemp = myHKeInvestData.getData("select * from [Invoice] where [referenceNumber] = '" + referenceNumber + "'");
                DataRow rowInvo;
                string stockOrderType = "";
                if (dtTemp.Rows.Count == 0 || dtTemp == null) return;
                else
                {
                    rowInvo = dtTemp.Rows[0];
                    if (!string.IsNullOrEmpty(rowInvo["stockOrderType"].ToString())) stockOrderType = rowInvo["stockOrderType"].ToString().Trim();
                    sql = "Delete from [Invoice] where [referenceNumber] = '" + referenceNumber + "'";
                    trans = myHKeInvestData.beginTransaction();
                    myHKeInvestData.setData(sql, trans);
                    myHKeInvestData.commitTransaction(trans);
                }

                foreach (DataRow row in dtTransToUpdate.Rows)
                {

                    sql = "Insert into [Invoice] values ('" + rowInvo["accountNumber"].ToString().Trim() + "', '" + referenceNumber + "', '" + row["transactionNumber"].ToString().Trim() + "', '" + rowInvo["code"].ToString().Trim() + "', '" + rowInvo["name"].ToString().Trim() + "', '" + rowInvo["type"].ToString().Trim() + "', '" + rowInvo["buyOrSell"].ToString().Trim() + "', '" + rowInvo["dateSubmitted"].ToString().Trim() + "', '" + status + "', '" + rowInvo["orderAmount"].ToString().Trim() + "', '" + rowInvo["stockOrderType"].ToString().Trim() + "', '" + rowInvo["expiryDay"].ToString().Trim() + "', '" + rowInvo["allOrNone"].ToString().Trim();
                    if (stockOrderType == "limit") sql = sql + "', '" + rowInvo["limitPrice"].ToString().Trim() + "', null, '" + row["executeDate"].ToString().Trim() + "', '" + row["executeShares"].ToString().Trim() + "', '" + row["executePrice"].ToString().Trim() + "', '0' )";
                    else if (stockOrderType == "stop") sql = sql + "', null, '" + rowInvo["stopPrice"].ToString().Trim() + "', '" + row["executeDate"].ToString().Trim() + "', '" + row["executeShares"].ToString().Trim() + "', '" + row["executePrice"].ToString().Trim() + "', '0' )";
                    else if (stockOrderType == "stop limit") sql = sql + "', '" + rowInvo["limitPrice"].ToString().Trim() + "', '" + rowInvo["stopPrice"].ToString().Trim() + "', '" + row["executeDate"].ToString().Trim() + "', '" + row["executeShares"].ToString().Trim() + "', '" + row["executePrice"].ToString().Trim() + "', '0' )";
                    else sql = sql + "', null, null, '" + row["executeDate"].ToString().Trim() + "', '" + row["executeShares"].ToString().Trim() + "', '" + row["executePrice"].ToString().Trim() + "', '0' )";
                    trans = myHKeInvestData.beginTransaction();
                    myHKeInvestData.setData(sql, trans);
                    myHKeInvestData.commitTransaction(trans);

                }

            }

        }

        //Initial function to update both [SecurityHolding] and [Account] table.
        //Calls .professSecurityHolding (string accountNumber) with foreach loop for accountNumbers that requires update.
        //Invoice row with serviceFee = '0' AND status = 'completed' will indicate invoice instance which requires processing.
        public void updateHoldingAccount()
        {
            string sql = "select distinct [accountNumber] from [Invoice] where (([status] = 'completed' and [serviceFee] = '0') or ([status] = 'cancelled' and [serviceFee] = '0' and [allOrNone]='N')) ";
            DataTable dtAccountsToProcess = myHKeInvestData.getData(sql);
            if (dtAccountsToProcess == null || dtAccountsToProcess.Rows.Count == 0) return;
            else
            {
                foreach (DataRow row in dtAccountsToProcess.Rows)
                {
                    string accountNumber = row["accountNumber"].ToString().Trim();
                    if (string.IsNullOrEmpty(accountNumber)) continue;
                    else
                    {
                        processSecurityHolding(accountNumber);
                    }
                }
            }

        }

        //Receives accountNumber that requires update and updates all securityHolding instances related to the invoice.
        //calls processAccount (string accountNumber) with passon variable.
        public void processSecurityHolding(string accountNumber)
        {
            string sql = "select * from [Invoice] where (([status] = 'completed' and [serviceFee] = '0') or ([status] = 'cancelled' and [serviceFee] = '0' and [allOrNone]='N')) and [accountNumber] = '"+accountNumber+"'";
            DataTable dtOrdersToProcess = myHKeInvestData.getData(sql);
            SqlTransaction trans;
            if (dtOrdersToProcess == null || dtOrdersToProcess.Rows.Count == 0) return;
            else
            {
                foreach (DataRow row in dtOrdersToProcess.Rows)
                {
                    string referenceNumber = row["referenceNumber"].ToString().Trim();

                    if (myHKeInvestData.getAggregateValue("select max([serviceFee]) from [Invoice] where [referenceNumber] = '" + referenceNumber + "' group by [referenceNumber]") == 0) //This will prevent update of securityHolding and Account if Invoice is not properly generated (requirement)
                    {
                        decimal serviceFee = calculateServiceFee(referenceNumber, calculateAsset(accountNumber), false);
                        if (!sendInvoice(accountNumber, referenceNumber, Math.Round(serviceFee, 3))) continue;
                    }
                    processAccount(accountNumber);
                    if (row["buyOrSell"].ToString().Trim() == "buy")
                    {
                        sql = "select * from [SecurityHolding] where [accountNumber] = '" + accountNumber + "' and [code] = '" + row["code"].ToString().Trim() + "'";
                        DataTable dtHolding = myHKeInvestData.getData(sql);
                        if (dtHolding == null) return;
                        if (row["type"].ToString().Trim() == "stock")
                        {
                            if (dtHolding.Rows.Count == 0)
                            {
                                sql = "Insert into [SecurityHolding] values ('" + accountNumber + "', '" + row["type"].ToString().Trim() + "', '" + row["code"].ToString().Trim() + "', '" + row["name"].ToString().Trim() + "', '" + row["executeShares"].ToString().Trim() + "', 'HKD', null, 'D', null, 'D')";
                                trans = myHKeInvestData.beginTransaction();
                                myHKeInvestData.setData(sql, trans);
                                myHKeInvestData.commitTransaction(trans);
                            }
                            else if (dtHolding.Rows.Count > 0)
                            {
                                decimal shares = Decimal.Parse(dtHolding.Rows[0]["shares"].ToString());
                                shares = shares + Decimal.Parse(row["executeShares"].ToString());

                                sql = "Update [SecurityHolding] set [shares] = '" + shares + "' where [accountNumber] ='" + accountNumber + "' and [code] ='" + row["code"].ToString().Trim() + "'";
                                trans = myHKeInvestData.beginTransaction();
                                myHKeInvestData.setData(sql, trans);
                                myHKeInvestData.commitTransaction(trans);
                            }
                        }
                        else
                        {
                            if (dtHolding.Rows.Count == 0)
                            {
                                DataTable dtBondUnit = myExternalFunctions.getSecuritiesByCode(row["type"].ToString().Trim(), row["code"].ToString().Trim());
                                sql = "Insert into [SecurityHolding] values ('" + accountNumber + "', '" + row["type"].ToString().Trim() + "', '" + row["code"].ToString().Trim() + "', '" + row["name"].ToString().Trim() + "', '" + row["executeShares"].ToString().Trim() + "', '" + dtBondUnit.Rows[0]["base"].ToString().Trim() + "', null, 'D', null, 'D')";
                                trans = myHKeInvestData.beginTransaction();
                                myHKeInvestData.setData(sql, trans);
                                myHKeInvestData.commitTransaction(trans);
                            }
                            else if (dtHolding.Rows.Count > 0)
                            {
                                decimal shares = Decimal.Parse(dtHolding.Rows[0]["shares"].ToString());
                                shares = shares + Decimal.Parse(row["executeShares"].ToString());

                                sql = "Update [SecurityHolding] set [shares] = '" + shares + "' where [accountNumber] ='" + accountNumber + "' and [code] ='" + row["code"].ToString().Trim() + "'";
                                trans = myHKeInvestData.beginTransaction();
                                myHKeInvestData.setData(sql, trans);
                                myHKeInvestData.commitTransaction(trans);
                            }
                        }
                    }
                    else if (row["buyOrSell"].ToString().Trim() == "sell")
                    {
                        sql = "select * from [SecurityHolding] where [accountNumber] = '" + accountNumber + "' and [code] = '" + row["code"].ToString().Trim() + "'";
                        DataTable dtHolding = myHKeInvestData.getData(sql);
                        if (dtHolding == null || dtHolding.Rows.Count == 0) return;
                        else
                        {
                            decimal shares = Decimal.Parse(dtHolding.Rows[0]["shares"].ToString());
                            shares = shares - Decimal.Parse(row["executeShares"].ToString());
                            if (shares == 0)
                            {
                                sql = "delete from [SecurityHolding] where [accountNumber] = '" + accountNumber + "' and [code] = '" + row["code"].ToString().Trim() + "'";
                                trans = myHKeInvestData.beginTransaction();
                                myHKeInvestData.setData(sql, trans);
                                myHKeInvestData.commitTransaction(trans);
                            }
                            else if (shares > 0)
                            {
                                sql = "Update [SecurityHolding] set [shares] = '" + shares + "' where [accountNumber] ='" + accountNumber + "' and [code] ='" + row["code"].ToString().Trim() + "'";
                                trans = myHKeInvestData.beginTransaction();
                                myHKeInvestData.setData(sql, trans);
                                myHKeInvestData.commitTransaction(trans);
                            }
                        }
                    }
                    else return;
                }

            }

        }

        //Receives accountNumber that requires update and updates all [Account].[balance] instances related to the invoice.
        //processes per referenceNumber (invoice instance).
        //calls calculateAsset (string accountNumber) to calculate Asset before the update is actually occured.
        //calls calculateServiceFee (string referenceNumber, string asset) to calculate the service fee for an order and returns total balance update amount. 
        public void processAccount(string accountNumber)
        {
            string sql = "select distinct [referenceNumber] from [Invoice] where (([status] = 'completed' and [serviceFee] = '0') or ([status] = 'cancelled' and [serviceFee] = '0' and [allOrNone]='N')) and [accountNumber] = '" + accountNumber + "'";
            DataTable dtOrders = myHKeInvestData.getData(sql);
            if (dtOrders == null || dtOrders.Rows.Count == 0) return;
            else
            {
                foreach (DataRow row in dtOrders.Rows)
                {
                    string referenceNumber = row["referenceNumber"].ToString().Trim();
                    decimal asset = calculateAsset(accountNumber);
                    decimal transaction;
                    if (asset == -1) return;
                    else
                    {
                        transaction = calculateServiceFee(referenceNumber, asset, true);
                        if (transaction == -1) continue;
                        else
                        {
                            sql = "select sum([balance]) from [Account] where [accountNumber] = '" + accountNumber + "'";
                            decimal currentBalance = myHKeInvestData.getAggregateValue(sql);
                            currentBalance = Convert.ToDecimal(currentBalance + transaction);
                            sql = "update [Account] set [balance] = '" + currentBalance + "' where [accountNumber] = '" + accountNumber + "'";
                            SqlTransaction trans = myHKeInvestData.beginTransaction();
                            myHKeInvestData.setData(sql, trans);
                            myHKeInvestData.commitTransaction(trans);
                        }
                    }

                }
            }
        }

        //Programmed to omit the Free Credit Balance since it is not defined in the domain.
        public decimal calculateAsset(string accountNumber)
        {
            decimal asset = -1;
            DataTable dtCurrent = myHKeInvestData.getData("select [balance] from [Account] where [accountNumber] = '" + accountNumber + "'");
            if (dtCurrent == null || dtCurrent.Rows.Count == 0) return asset;
            else
            {
                asset = Decimal.Parse(dtCurrent.Rows[0]["balance"].ToString());
            }
            DataTable dtHolding = myHKeInvestData.getData("select * from [SecurityHolding] where [accountNumber] = '" + accountNumber + "'");
            if (dtHolding == null || dtHolding.Rows.Count == 0) return asset;
            else
            {
                foreach (DataRow row in dtHolding.Rows)
                {
                    string type = row["type"].ToString().Trim();
                    string code = row["code"].ToString().Trim();
                    decimal shares = Decimal.Parse(row["shares"].ToString().Trim());
                    decimal price = convertPrice(type, code);
                    if (price == -1) { continue; }
                    asset = asset + Convert.ToDecimal(shares * price);
                }
            }
            return Math.Round(asset, 3);

        }
        //if updateDB is true (default) calculateServiceFee returns OrderAmount+ Fee and updates the DB.
        //if updateDB is false calculateServiceFee returns Fee only.
        public decimal calculateServiceFee(string referenceNumber, decimal assets, bool updateDB = true)
        {
            string sql = "select [type], [code], [buyOrSell], [stockOrderType], ([executeShares]*[executePrice]) as executeAmount from [Invoice] where [referenceNumber] = '" + referenceNumber + "' and [serviceFee] = '0'";
            DataTable dtExecution = myHKeInvestData.getData(sql);
            decimal orderAmount = 0;
            decimal fee = 0;
            string buyOrSell;
            if (dtExecution == null || dtExecution.Rows.Count == 0) return -1;
            else
            {
                buyOrSell = dtExecution.Rows[0]["buyOrSell"].ToString().Trim();
                if (string.IsNullOrEmpty(buyOrSell)) return -1;

                foreach (DataRow row in dtExecution.Rows)
                {
                    if (string.IsNullOrEmpty(row["executeAmount"].ToString())) return -1;
                    orderAmount = orderAmount + (Decimal.Parse(row["executeAmount"].ToString().Trim())*getCurrency(row["type"].ToString().Trim(),row["code"].ToString().Trim()));
                }

                if (string.IsNullOrEmpty(dtExecution.Rows[0]["type"].ToString())) fee = 0;
                else if (dtExecution.Rows[0]["type"].ToString().Trim() == "stock")
                {
                    if (assets < Convert.ToDecimal(1000000))
                    {
                        if (string.IsNullOrEmpty(dtExecution.Rows[0]["stockOrderType"].ToString())) return -1;
                        else if (dtExecution.Rows[0]["stockOrderType"].ToString().Trim() == "market") fee = orderAmount * Convert.ToDecimal(0.004);
                        else if (dtExecution.Rows[0]["stockOrderType"].ToString().Trim() == "stop limit") fee = orderAmount * Convert.ToDecimal(0.008);
                        else fee = fee + orderAmount * (decimal)(0.006);

                        if (fee < 150) fee = 150;
                    }
                    else if (assets >= Convert.ToDecimal(1000000))
                    {
                        if (string.IsNullOrEmpty(dtExecution.Rows[0]["stockOrderType"].ToString())) return -1;
                        else if (dtExecution.Rows[0]["stockOrderType"].ToString().Trim() == "market") fee = orderAmount * Convert.ToDecimal(0.002);
                        else if (dtExecution.Rows[0]["stockOrderType"].ToString().Trim() == "stop limit") fee = orderAmount * Convert.ToDecimal(0.006);
                        else fee = fee + orderAmount * (decimal)(0.004);

                        if (fee < 100) fee = 100;
                    }

                }
                else
                {
                    if (assets < Convert.ToDecimal(500000))
                    {
                        if (buyOrSell == "buy") fee = orderAmount * Convert.ToDecimal(0.05);
                        else fee = 100;
                    }
                    else if (assets >= Convert.ToDecimal(500000))
                    {
                        if (buyOrSell == "buy") fee = orderAmount * Convert.ToDecimal(0.03);
                        else fee = 50;
                    }
                }

                if (updateDB)
                {
                    sql = "update [Invoice] set [serviceFee] = '" + Convert.ToDecimal(fee) + "' where [referenceNumber] ='" + referenceNumber + "'";
                    SqlTransaction trans = myHKeInvestData.beginTransaction();
                    myHKeInvestData.setData(sql, trans);
                    myHKeInvestData.commitTransaction(trans);
                }
                if (buyOrSell == "buy")
                {
                    if (!updateDB) return Convert.ToDecimal(fee);
                    else return Convert.ToDecimal(0 - (orderAmount + fee));
                }
                else if (buyOrSell == "sell")
                {
                    if (!updateDB) return Convert.ToDecimal(fee);
                    else return Convert.ToDecimal(orderAmount - fee);
                }
                else return -1;
            }
        }

        private bool sendInvoice(string accountNumber, string referenceNumber, decimal serviceFee)
        {
            if (serviceFee == -1) return true;
            string sql = "select * from [Client] where [accountNumber] = '" + accountNumber + "'";
            DataTable dtAccount = myHKeInvestData.getData(sql);
            if (dtAccount == null || dtAccount.Rows.Count == 0) return false;
            else
            {
                string toAddress = dtAccount.Rows[0]["email"].ToString().Trim();
                string subject = "Invoice for Security Trade Order [Reference Number: " + referenceNumber + "]";
                //mail.IsBodyHtml = true;
                string mailBody = "This Email is generated by the system. Please do not reply <br /> Dear Client (Account Number: " + accountNumber + "), <br /> Your order (Reference Number: " + referenceNumber + ") has been successfully processed. <br /> <br />";
                sql = "select * from [Invoice] where [accountNumber] = '" + accountNumber + "' and [referenceNumber] = '" + referenceNumber + "'";
                DataTable dtInvoice = myHKeInvestData.getData(sql);
                if (dtInvoice == null || dtInvoice.Rows.Count == 0) return false;
                else
                {
                    mailBody = mailBody + "[Order Information] <br /> Order Type: " + dtInvoice.Rows[0]["buyOrSell"].ToString() + "<br />";
                    mailBody = mailBody + "Security Code: " + dtInvoice.Rows[0]["code"].ToString() + "<br />";
                    mailBody = mailBody + "Security Name: " + dtInvoice.Rows[0]["name"].ToString() + "<br />";
                    if (dtInvoice.Rows[0]["type"].ToString().Trim() == "stock") mailBody = mailBody + "Stock Order Type: " + dtInvoice.Rows[0]["stockOrderType"].ToString() + "<br />";
                    mailBody = mailBody + "Submitted Date: " + dtInvoice.Rows[0]["dateSubmitted"].ToString() + "<br />";
                    if (dtInvoice.Rows.Count == 1)
                    {
                        mailBody = mailBody + "Total Number of Shares Traded: " + dtInvoice.Rows[0]["executeShares"].ToString() + "<br />";
                        decimal totalExecuteAmount = Math.Round(Decimal.Parse(dtInvoice.Rows[0]["executePrice"].ToString()) * Decimal.Parse(dtInvoice.Rows[0]["executeShares"].ToString()) * getCurrency(dtInvoice.Rows[0]["type"].ToString().Trim(), dtInvoice.Rows[0]["code"].ToString().Trim()), 3);
                        mailBody = mailBody + "Total Executed Amount of Order (HKD): " + totalExecuteAmount.ToString() + "<br />";
                        mailBody = mailBody + "Total Service Fee: " + Convert.ToString(serviceFee) + "<br /><br />[Transactions]";

                        mailBody = mailBody + "<br />================================================<br />";
                        mailBody = mailBody + "Transaction Number: " + dtInvoice.Rows[0]["transactionNumber"].ToString() + "<br />";
                        mailBody = mailBody + "Execute Date: " + dtInvoice.Rows[0]["executeDate"].ToString() + "<br />";
                        mailBody = mailBody + "Executed Shares: " + dtInvoice.Rows[0]["executeShares"].ToString() + "<br />";
                        mailBody = mailBody + "Executed Price per Share: " + dtInvoice.Rows[0]["executePrice"].ToString() + "<br />";
                    }
                    else
                    {
                        string transactionBody = "";
                        decimal totalShares = 0;
                        decimal totalExecuteAmount = 0;
                        foreach (DataRow row in dtInvoice.Rows)
                        {
                            totalShares = totalShares + Decimal.Parse(row["executeShares"].ToString());
                            totalExecuteAmount = totalExecuteAmount + Math.Round(Decimal.Parse(row["executePrice"].ToString()) * Decimal.Parse(row["executeShares"].ToString()) * getCurrency(row["type"].ToString().Trim(), row["code"].ToString().Trim()), 3);
                            transactionBody = transactionBody + "<br />================================================<br />";
                            transactionBody = transactionBody + "Transaction Number: " + row["transactionNumber"].ToString() + "<br />";
                            transactionBody = transactionBody + "Execute Date: " + row["executeDate"].ToString() + "<br />";
                            transactionBody = transactionBody + "Executed Shares: " + row["executeShares"].ToString() + "<br />";
                            transactionBody = transactionBody + "Executed Price per Share: " + row["executePrice"].ToString() + "<br />";
                        }
                        mailBody = mailBody + "Total Number of Shares Traded: " + totalShares.ToString() + "<br />";
                        mailBody = mailBody + "Total Executed Amount of Order (HKD): " + totalExecuteAmount.ToString() + "<br />";
                        mailBody = mailBody + "Total Service Fee: " + Convert.ToString(serviceFee) + "<br /><br />[Transactions]";
                        mailBody = mailBody + transactionBody;
                    }
                    bool emailStatus = emailHandler(toAddress, subject, mailBody);
                    return emailStatus;
                }
            }



        }

        public bool emailHandler(string toAddress, string subject, string bodyText)
        {
            if (string.IsNullOrEmpty(toAddress) || string.IsNullOrEmpty(subject) || string.IsNullOrEmpty(bodyText)) return false;
            MailMessage mail = new MailMessage();
            SmtpClient emailServer = new SmtpClient();
            //mail.From = new MailAddress("comp3311_team122@cse.ust.hk", "HKeInvest");
            mail.To.Add(toAddress);
            mail.Subject = subject;
            mail.Body = bodyText;
            mail.IsBodyHtml = true;
            bool emailSucceed = true;
            try
            {
                emailServer.Send(mail);
            }
            catch (SmtpException)
            {
                emailSucceed = false;
            }

            return emailSucceed;
        }

        //returns DataTable with type, code, name,  price, total values traded, base of top 20 best sellers.
        //returns null if sql errors occur or there are no securities being traded in the specified month.
        public DataTable queryBestSeller(string month, string year)
        {
            string sql = "select top 20 temp.type AS type, temp.code AS code, temp.name AS name, temp.sellings AS orders, round(temp.highPrice, 2) AS highPrice, round(temp.lowPrice, 2) AS lowPrice, round((temp.shares * temp.avgprice), 2) AS volume from (select max([type]) AS type, [code], max([name]) AS name, count(distinct [referenceNumber]) AS sellings, sum([executeShares]) AS shares, avg([executePrice]) AS avgprice, max([executePrice]) AS highPrice, min([executePrice]) AS lowPrice from [Invoice] where (year([executeDate]) = '" + year + "' and month([executeDate]) = '" + month + "') and ([buyOrSell] = 'buy') and (([status] = 'completed' and not [serviceFee] = '0') or ([status] = 'cancelled' and not [serviceFee] = '0' and [allOrNone]='N')) group by [code]) temp order by sellings desc";
            DataTable dtBestSeller = myHKeInvestData.getData(sql);

            if (dtBestSeller == null || dtBestSeller.Rows.Count == 0) return null;
            else return dtBestSeller;
        }
        //convert the security price to HKD
        public decimal convertPrice(string securityType, string securityCode)
        {
            DataTable dtSecurity = myExternalFunctions.getSecuritiesByCode(securityType, securityCode);
            if (dtSecurity == null || dtSecurity.Rows.Count == 0) return -1;
            else
            {
                if (securityType == "stock") return myExternalFunctions.getSecuritiesPrice(securityType, securityCode);
                else
                {
                    decimal rate = getCurrency(dtSecurity);
                    decimal price = myExternalFunctions.getSecuritiesPrice(securityType, securityCode);

                    return Math.Round(rate * price, 3);
                }
            }
        }

        //assume the security exists; (WARNING)
        public decimal getCurrency(DataTable dtSecurity)
        {
            string currency = dtSecurity.Rows[0]["base"].ToString().Trim();
            if (currency == "HKD") return 1;
            else
            {
                decimal rate = myExternalFunctions.getCurrencyRate(currency);
                return rate;
            }
        }

        public decimal getCurrency(string securityType, string securityCode)
        {
            if (securityType == "stock") return 1;
            DataTable dtSecurity = myExternalFunctions.getSecuritiesByCode(securityType, securityCode);
            string currency = dtSecurity.Rows[0]["base"].ToString().Trim();
            if (currency == "HKD") return 1;
            else
            {
                decimal rate = myExternalFunctions.getCurrencyRate(currency);
                return rate;
            }
        }
        
        public void triggerAlert()
        {
            string sql = "select * from [SecurityHolding] where not ([lowAlarm] IS NULL and [highAlarm] IS NULL)";
            DataTable dtAlert = myHKeInvestData.getData(sql);
            if (dtAlert == null || dtAlert.Rows.Count == 0) return;
            else
            {
                foreach(DataRow row in dtAlert.Rows)
                {
                    string accountNumber = row["accountNumber"].ToString().Trim();
                    string securityType = row["type"].ToString().Trim();
                    string securityCode = row["code"].ToString().Trim();
                    if (row.IsNull("highAlarm"))
                    {
                        if(checkAlertTrigger("low", accountNumber, securityCode, securityType))
                        {
                            sendAlert(accountNumber, securityType, securityCode, "low");
                            SqlTransaction trans = myHKeInvestData.beginTransaction();
                            myHKeInvestData.setData("update [SecurityHolding] set [lowAlarm] = null, [lowComparator] = 'D' where [accountNumber] = '" + accountNumber + "' and [code] = '" + securityCode + "' and [type] = '" + securityType + "'", trans);
                            myHKeInvestData.commitTransaction(trans);
                        }
                    }
                    else if (row.IsNull("lowAlarm"))
                    {
                        if(checkAlertTrigger("high", accountNumber, securityCode, securityType))
                        {
                            sendAlert(accountNumber, securityType, securityCode, "high");
                            SqlTransaction trans = myHKeInvestData.beginTransaction();
                            myHKeInvestData.setData("update [SecurityHolding] set [highAlarm] = null, [highComparator] = 'D' where [accountNumber] = '" + accountNumber + "' and [code] = '" + securityCode + "' and [type] = '" + securityType + "'", trans);
                            myHKeInvestData.commitTransaction(trans);
                        }
                    }
                    else
                    {
                        if (checkAlertTrigger("low", accountNumber, securityCode, securityType))
                        {
                            sendAlert(accountNumber, securityType, securityCode, "low");
                            SqlTransaction trans = myHKeInvestData.beginTransaction();
                            myHKeInvestData.setData("update [SecurityHolding] set [lowAlarm] = null, [lowComparator] = 'D' where [accountNumber] = '" + accountNumber + "' and [code] = '" + securityCode + "' and [type] = '" + securityType + "'", trans);
                            myHKeInvestData.commitTransaction(trans);
                        }

                        if (checkAlertTrigger("high", accountNumber, securityCode, securityType))
                        {
                            sendAlert(accountNumber, securityType, securityCode, "high");
                            SqlTransaction trans = myHKeInvestData.beginTransaction();
                            myHKeInvestData.setData("update [SecurityHolding] set [highAlarm] = null, [highComparator] = 'D' where [accountNumber] = '" + accountNumber + "' and [code] = '" + securityCode + "' and [type] = '" + securityType + "'", trans);
                            myHKeInvestData.commitTransaction(trans);
                        }
                    }
                    
                }
            }
        }

        public bool checkAlertTrigger (string highOrLow, string accountNumber, string securityCode, string securityType)
        {
            if (highOrLow == "high")
            {
                string currentComparator = getCurrentAlertComparator(highOrLow, accountNumber, securityCode).Trim();
                if (string.IsNullOrEmpty(currentComparator)) return false;
                decimal currentPrice = myExternalFunctions.getSecuritiesPrice(securityType, securityCode);
                if (currentPrice == -1) return false;
                decimal triggerPrice = getTriggerValue(highOrLow, accountNumber, securityCode);
                if (triggerPrice == -1) return false;
                string sql = "";
                bool trigger = false;
                if(triggerPrice <= currentPrice)
                {
                    if(currentComparator == "H") trigger = true; //If triggerPrice was 'higher' than the currentPrice and now it is reversed;
                    sql = "update [SecurityHolding] set [highComparator] = 'L' where [accountNumber] = '" + accountNumber + "' and [code] ='" + securityCode + "'";
                }
                else
                {
                    trigger = false;
                    sql = "update [SecurityHolding] set [highComparator] = 'H' where [accountNumber] = '" + accountNumber + "' and [code] ='" + securityCode + "'";
                }

                SqlTransaction trans = myHKeInvestData.beginTransaction();
                myHKeInvestData.setData(sql, trans);
                myHKeInvestData.commitTransaction(trans);
                return trigger;
            }
            else if (highOrLow == "low")
            {
                string currentComparator = getCurrentAlertComparator(highOrLow, accountNumber, securityCode).Trim();
                if (string.IsNullOrEmpty(currentComparator)) return false;
                decimal currentPrice = myExternalFunctions.getSecuritiesPrice(securityType, securityCode);
                if (currentPrice == -1) return false;
                decimal triggerPrice = getTriggerValue(highOrLow, accountNumber, securityCode);
                if (triggerPrice == -1) return false;
                string sql = "";
                bool trigger = false;
                if (triggerPrice >= currentPrice)
                {
                    if (currentComparator == "L") trigger = true; //If triggerPrice was 'lower' than the currentPrice and now it is reversed;
                    sql = "update [SecurityHolding] set [lowComparator] = 'H' where [accountNumber] = '" + accountNumber + "' and [code] ='" + securityCode + "'";
                }
                else
                {
                    trigger = false;
                    sql = "update [SecurityHolding] set [lowComparator] = 'L' where [accountNumber] = '" + accountNumber + "' and [code] ='" + securityCode + "'";
                }

                SqlTransaction trans = myHKeInvestData.beginTransaction();
                myHKeInvestData.setData(sql, trans);
                myHKeInvestData.commitTransaction(trans);
                return trigger;
            }
            else return false;
        }

        public string getCurrentAlertComparator (string highOrLow, string accountNumber, string securityCode)
        {
            string sql = "";
            if (highOrLow == "high") sql = "select [highComparator] as Comparator from [SecurityHolding] where [accountNumber] = '" + accountNumber + "' and [code] = '" + securityCode + "'";
            else if (highOrLow == "low") sql = "select [lowComparator] as Comparator from [SecurityHoding where [accountNumber] = '" + accountNumber + "' and [code] = '" + securityCode + "'";
            else return null;
            DataTable dtComparator = myHKeInvestData.getData(sql);
            if (dtComparator == null || dtComparator.Rows.Count == 0) return null;
            else
            {
                string comparator = dtComparator.Rows[0]["Comparator"].ToString().Trim();
                if (string.IsNullOrEmpty(comparator)) return null;
                else return comparator;
            }
        }

        public decimal getTriggerValue (string highOrLow, string accountNumber, string securityCode)
        {
            string sql = "";
            if (highOrLow == "high") sql = "select max([highAlarm]) from [SecurityHolding] where [accountNumber] = '" + accountNumber + "' and [code] = '" + securityCode + "'";
            else if (highOrLow == "low") sql = "select min([lowAlarm]) from [SecurityHolding] where [accountNumber] = '" + accountNumber + "' and [code] = '" + securityCode + "'";
            else return -1;
            decimal triggerValue = myHKeInvestData.getAggregateValue(sql);
            return triggerValue;
        }

        public bool sendAlert(string accountNumber, string securityType, string securityCode, string highOrLow)
        {
            DataTable dtClient = myHKeInvestData.getData("select [email] from [Client] where [accountNumber] = '" + accountNumber + "'");
            if(dtClient == null || dtClient.Rows.Count == 0) return false;
            string email = dtClient.Rows[0]["email"].ToString().Trim();
            string subject = "Notification: Alert Triggered";

            string bodyText = "This Email is generated by the system. Please do not reply <br /> Dear Client (Account Number: " + accountNumber + "), <br /> Your";
            if (highOrLow == "high") bodyText = bodyText + "High Value ";
            else bodyText = bodyText + "Low Value ";
            bodyText = bodyText + "Alert for Security (Security Code: " + securityCode + ") has been triggered. <br /> <br />";

            DataTable dtSecurity = myExternalFunctions.getSecuritiesByCode(securityType, securityCode);
            if (dtSecurity == null || dtSecurity.Rows.Count == 0) return false;
            string securityName = dtSecurity.Rows[0]["name"].ToString().Trim();
            bodyText = bodyText + "[Security Information]<br />Security Type: " + securityType + "<br />Security Code: " + securityCode + "<br />Security Name: " + securityName + "<br />Current Price: ";
            decimal currentPrice = myExternalFunctions.getSecuritiesPrice(securityType, securityCode);
            if (currentPrice == -1) return false;
            bodyText = bodyText + Convert.ToString(Math.Round(currentPrice, 2)) + "<br />Trigger Price: ";
            decimal triggerPrice = getTriggerValue(highOrLow, accountNumber, securityCode);
            if (triggerPrice == -1) return false;
            bodyText = bodyText + Convert.ToString(Math.Round(triggerPrice, 2)) + "<br />Remarks: ";
            if (highOrLow == "high") bodyText = bodyText + "Change in current security price triggered the high value alert settings.";
            else bodyText = bodyText + "Change in current security price triggered the low value alert settings.";

            bool result = emailHandler(email, subject, bodyText);

            return result;


            
        }
    }
}