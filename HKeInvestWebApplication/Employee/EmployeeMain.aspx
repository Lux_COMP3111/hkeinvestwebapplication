﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeMain.aspx.cs" Inherits="HKeInvestWebApplication.Employee.EmployeeMain" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Select Employee Activity</h2>
    <hr />
    <h4>Employee Service</h4>
    <asp:HyperLink runat="server" Text="Account Application Page" NavigateUrl="~/Employee/AccountApplicationPage.aspx"></asp:HyperLink>
    <br />    
    <asp:HyperLink runat="server" Text="Search Securities" NavigateUrl="~/SecuritySearch.aspx"></asp:HyperLink>
    <br />    
    <asp:HyperLink runat="server" Text="Report Generation" NavigateUrl="~/Employee/ReportGeneration.aspx"></asp:HyperLink>
</asp:Content>
