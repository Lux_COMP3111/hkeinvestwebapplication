﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportGeneration.aspx.cs" Inherits="HKeInvestWebApplication.Employee.ReportGeneration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-header"><h2>Report Generation</h2></div>
    <%--Security Type DropdownList with Bootstrap Select Applied--%>
    <div class="form-horizontal panel-group">
        <%--Currency DropdownList with Bootstrap Select Applied--%>
        <asp:Panel runat="server" ID="pnInputs" CssClass="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="txtAccountNumber" Text="Account number:" CssClass="control-label col-md-2"></asp:Label>
                    <div class="col-md-4">
                        <asp:TextBox runat="server" ID="txtAccountNumber" CssClass="form-control"></asp:TextBox>
                    </div>
                    <%--Security Type DropdownList with Bootstrap Select Applied--%>
                    <div class="col-md-3">
                        <asp:DropDownList runat="server" ID="ddlSecurityType" AutoPostBack="true" OnSelectedIndexChanged="ddlSecurityType_SelectedIndexChanged" EnableViewState="true" CssClass="form-control">
                            <asp:ListItem Value="0">-- Select --</asp:ListItem>
                            <asp:ListItem Value="all">All</asp:ListItem>
                            <asp:ListItem Value="active">Active Orders</asp:ListItem>
                            <asp:ListItem Value="history">Order History</asp:ListItem>
                            <asp:ListItem Value="bond">Bond</asp:ListItem>
                            <asp:ListItem Value="stock">Stock</asp:ListItem>
                            <asp:ListItem Value="unit trust">Unit Trust</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <%--Currency DropdownList with Bootstrap Select Applied--%>
                    <div class="col-md-3">
                    <asp:DropDownList runat="server" ID="ddlCurrency" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged" CssClass="form-control">
                        <asp:ListItem Value="0">Currency</asp:ListItem>
                    </asp:DropDownList>
                    </div>
                </div>
                <asp:Panel ID="pnRange" runat="server">
                    <div class="form-group">
                        <asp:Label runat="server" Text="from" CssClass="control-label col-md-2" AssociatedControlID="DateStart"></asp:Label>
                        <div class="col-md-2">
                            <asp:TextBox ID="DateStart" runat="server" CssClass="form-control" MaxLength="10" AutoPostBack="True" OnTextChanged="Date_TextChanged"></asp:TextBox>
                            <asp:RegularExpressionValidator runat="server" ControlToValidate="DateStart" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="Start date is not valid. Please use format dd/mm/yyyy" ValidationExpression="^(?:(?:31(\/)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/)(?:0?2)\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">Start date is not valid. Please use format dd/mm/yyyy</asp:RegularExpressionValidator>
                        </div>
                        <asp:Label runat="server" Text="until" CssClass="control-label col-md-2" AssociatedControlID="DateEnd"></asp:Label>
                        <div class="col-md-2">
                            <asp:TextBox ID="DateEnd" runat="server" CssClass="form-control" MaxLength="10" AutoPostBack="True" OnTextChanged="Date_TextChanged"></asp:TextBox>
                            <asp:RegularExpressionValidator runat="server" ControlToValidate="DateEnd" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="End date is not valid. Please use format dd/mm/yyyy" ValidationExpression="^(?:(?:31(\/)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/)(?:0?2)\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">End date is not valid. Please use format dd/mm/yyyy</asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" Text="Buy or Sell" AssociatedControlID="ddlBuyOrSell" CssClass="control-label col-md-2"></asp:Label>
                        <div class="col-md-2">
                            <asp:DropDownList ID="ddlBuyOrSell" runat="server" AutoPostBack="true" EnableViewState="true" CssClass="form-control" OnSelectedIndexChanged="filter_SelectedIndexChanged">
                                <asp:ListItem Value="0">None</asp:ListItem>
                                <asp:ListItem Value="buy">Buy</asp:ListItem>
                                <asp:ListItem Value="sell">Sell</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" Text="Order Type" AssociatedControlID="ddlType" CssClass="control-label col-md-2"></asp:Label>
                        <div class="col-md-2">
                            <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true" EnableViewState="true" CssClass="form-control" OnSelectedIndexChanged="filter_SelectedIndexChanged">
                                <asp:ListItem Value="0">None</asp:ListItem>
                                <asp:ListItem Value="bond">Bond</asp:ListItem>
                                <asp:ListItem Value="stock">Stock</asp:ListItem>
                                <asp:ListItem Value="trust">Unit Trust</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:Label runat="server" Text="Security Code" CssClass="control-label col-md-2" AssociatedControlID="Code"></asp:Label>
                        <div class="col-md-2">
                            <asp:TextBox ID="Code" runat="server" AutoPostBack="true" CssClass="form-control" Enabled="False" OnTextChanged="filter_SelectedIndexChanged"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" Text="Status" AssociatedControlID="ddlStatus" CssClass="control-label col-md-2"></asp:Label>
                        <div class="col-md-2">
                            <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="true" EnableViewState="true" CssClass="form-control" OnSelectedIndexChanged="filter_SelectedIndexChanged">
                                <asp:ListItem Value="0">None</asp:ListItem>
                                <asp:ListItem Value="completed">Completed</asp:ListItem>
                                <asp:ListItem Value="partial">Partial</asp:ListItem>
                                <asp:ListItem Value="cancelled">Cancelled</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnAll" CssClass="panel panel-default">
            <div class="panel-heading">Account Details</div>
            <div class="panel-body">  
                <%--Client Name and Result Message Labels--%>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" ID="lblClientName2" Visible="false" CssClass="label label-primary"></asp:Label>
                    <asp:Label runat="server" ID="lblResultMessage2" Visible="false" CssClass="label label-danger"></asp:Label>
                </div>
                <div class="form-group col-md-12">
                    <asp:Panel ID="pnDetails" runat="server">
                        <div class="form-group">
                            <asp:Label ID="lblTotal" runat="server" Text="Total Monetary Value:" CssClass="control-label col-md-3"></asp:Label>
                            <div class="col-md-3">
                                <asp:TextBox ID="Total" runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                            </div>
                            <asp:Label ID="lblFreeBalance" runat="server" Text="Free Balance:" CssClass="control-label col-md-3"></asp:Label>
                            <div class="col-md-3">
                                <asp:TextBox ID="FreeBalance" runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                           </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="lblBond" runat="server" Text="Bond Value:" CssClass="control-label col-md-3"></asp:Label>
                            <div class="col-md-3">
                                <asp:TextBox ID="Bond" runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                           </div>
                            <asp:Label ID="lblStock" runat="server" Text="Stock Value:" CssClass="control-label col-md-3"></asp:Label>
                            <div class="col-md-3">
                                <asp:TextBox ID="Stock" runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                           </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="lblTrust" runat="server" Text="Unit Trust Value:" CssClass="control-label col-md-3"></asp:Label>
                            <div class="col-md-3">
                                <asp:TextBox ID="Trust" runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                           </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="lblLastDate" runat="server" Text="Last Executed Order Date:" CssClass="control-label col-md-3"></asp:Label>
                            <div class="col-md-3">
                                <asp:TextBox ID="LastDate" runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                           </div>
                            <asp:Label ID="lblLastValue" runat="server" Text="Last Executed Order Value:" CssClass="control-label col-md-3"></asp:Label>
                            <div class="col-md-3">
                                <asp:TextBox ID="LastValue" runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                           </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnActive" CssClass="panel panel-default">
            <div class="panel-heading">Order Details</div>
            <div class="panel-body">  
                <%--Client Name and Result Message Labels--%>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" ID="lblClientName3" Visible="false" CssClass="label label-primary"></asp:Label>
                    <asp:Label runat="server" ID="lblResultMessage3" Visible="false" CssClass="label label-danger"></asp:Label>
                </div>
                <div class="form-group col-md-12">
                    <%--Gridview to display query results--%>
                    <asp:GridView runat="server" ID="gvBondTrustActiveOrders" Visible="False" AutoGenerateColumns="False" CssClass="table table-bordered table-condensed table-hover" AllowSorting="True">
                        <Columns>
                            <asp:BoundField DataField="referenceNumber" HeaderText="Reference Number" ReadOnly="True" />
                            <asp:BoundField DataField="buyOrSell" HeaderText="Buy or Sell" ReadOnly="True" />
                            <asp:BoundField DataField="type" HeaderText="Type" ReadOnly="True" />
                            <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="True" />
                            <asp:BoundField DataField="name"  HeaderText="Name" ReadOnly="True" />
                            <asp:BoundField DataField="dateSubmitted" DataFormatString="{0:MM-dd-yyyy}" HeaderText="Date Submitted" ReadOnly="True" SortExpression="dateSubmitted" />
                            <asp:BoundField DataField="status" HeaderText="Status" ReadOnly="True" />
                            <asp:BoundField DataField="orderAmount" HeaderText="Amount" ReadOnly="True" /><%-- buy only--%>
                            <asp:BoundField DataField="shares" HeaderText="Shares" ReadOnly="True" /><%-- sell only--%>
                        </Columns>
                    </asp:GridView>
                    <asp:GridView runat="server" ID="gvStockActiveOrders" Visible="False" AutoGenerateColumns="False" CssClass="table table-bordered table-condensed table-hover" AllowSorting="True">
                        <Columns>
                            <asp:BoundField DataField="referenceNumber" HeaderText="Reference Number" ReadOnly="True" />
                            <asp:BoundField DataField="buyOrSell" HeaderText="Buy or Sell" ReadOnly="True" />
                            <asp:BoundField DataField="type" HeaderText="Type" ReadOnly="True" />
                            <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="True" />
                            <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True" />
                            <asp:BoundField DataField="dateSubmitted" DataFormatString="{0:MM-dd-yyyy}" HeaderText="Date Submitted" ReadOnly="True" SortExpression="dateSubmitted" />
                            <asp:BoundField DataField="status" HeaderText="Status" ReadOnly="True" />
                            <asp:BoundField DataField="orderAmount" HeaderText="Shares" ReadOnly="True" />
                            <asp:BoundField DataField="limitPrice" HeaderText="Limit Price" ReadOnly="True" /> <%-- limit order or stoplimit order only --%>
                            <asp:BoundField DataField="stopPrice" HeaderText="Stop Price" ReadOnly="True" /> <%-- stop order stoplimit order only --%>
                            <asp:BoundField DataField="expiryDay" HeaderText="Expiry Day" ReadOnly="True" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnHistory" CssClass="panel panel-default">
            <div class="panel-heading">Order History</div>
            <div class="panel-body">  
                <%--Client Name and Result Message Labels--%>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" ID="lblClientName4" Visible="false" CssClass="label label-primary"></asp:Label>
                    <asp:Label runat="server" ID="lblResultMessage4" Visible="false" CssClass="label label-danger"></asp:Label>
                </div>
                <div class="form-group col-md-12">
                    <%--Gridview to display query results--%>
                    
                    <asp:GridView runat="server" ID="gvOrderHistory" Visible="True" AutoGenerateColumns="False" OnSorting="gvOrderHistory_Sorting" CssClass="table table-bordered table-condensed table-hover" AllowSorting="True">
                        <Columns>
                            <asp:BoundField DataField="referenceNumber" HeaderText="Reference Number" ReadOnly="True" />
                            <asp:BoundField DataField="buyOrSell" HeaderText="Buy or Sell" ReadOnly="True" />
                            <asp:BoundField DataField="type" HeaderText="Type" ReadOnly="True"  SortExpression="type" />
                            <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="True" />
                            <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True"  SortExpression="name" />
                            <%--<asp:BoundField DataField="submit" HeaderText="Date Submitted" ReadOnly="True" SortExpression="dateSubmitted" />--%>
                            <asp:BoundField DataField="status" HeaderText="Status" ReadOnly="True"  SortExpression="status" />
                            <asp:BoundField DataField="totalShares" DataFormatString="{0:n2}" HeaderText="Total Shares" ReadOnly="True" />
                            <asp:BoundField DataField="executedAmount" DataFormatString="{0:n2}" HeaderText="Total Amount(HKD)" ReadOnly="True" />
                            <asp:BoundField DataField="serviceFee" DataFormatString="{0:n2}" HeaderText="Service Fee" ReadOnly="True" />
                            <asp:BoundField DataField="transactionNumber" HeaderText="Transaction Number" ReadOnly="True" />
                            <%--<asp:BoundField DataField="execute" HeaderText="Date Executed" ReadOnly="True" />--%>
                            <asp:BoundField DataField="executeShares" DataFormatString="{0:n2}" HeaderText="Shares" ReadOnly="True" />
                            <asp:BoundField DataField="executePrice" DataFormatString="{0:n2}" HeaderText="Price" ReadOnly="True" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnResult" CssClass="panel panel-default">
            <div class="panel-heading">Holding Details</div>
            <div class="panel-body">  
                <%--Client Name and Result Message Labels--%>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" ID="lblClientName" Visible="false" CssClass="label label-primary"></asp:Label>
                    <asp:Label runat="server" ID="lblResultMessage" Visible="false" CssClass="label label-danger"></asp:Label>
                </div>
                <div class="form-group col-md-12">
                    <%--Gridview to display query results--%>
                    <asp:GridView runat="server" ID="gvSecurityHolding" Visible="False" AutoGenerateColumns="False" OnSorting="gvSecurityHolding_Sorting" CssClass="table table-bordered table-condensed table-hover" AllowSorting="True">
                        <Columns>
                            <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="True" SortExpression="code" />
                            <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True" SortExpression="name" />
                            <asp:BoundField DataField="shares" DataFormatString="{0:n2}" HeaderText="Shares" ReadOnly="True" SortExpression="shares" />
                            <asp:BoundField DataField="base" HeaderText="Base" ReadOnly="True" />
                            <asp:BoundField DataField="price" DataFormatString="{0:n2}" HeaderText="Price" ReadOnly="True" />
                            <asp:BoundField DataField="value" DataFormatString="{0:n2}" HeaderText="Value" ReadOnly="True" SortExpression="value" />
                            <asp:BoundField DataField="convertedValue" DataFormatString="{0:n2}" HeaderText="Value in" ReadOnly="True" SortExpression="convertedValue" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
