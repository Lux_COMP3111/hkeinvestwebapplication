﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AccountApplicationPage.aspx.cs" Inherits="HKeInvestWebApplication.AccountApplicationPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Account Application</h2>
    <div class="form-horizontal">
        <asp:ValidationSummary runat="server" CssClass="text-danger" DisplayMode="BulletList" EnableClientScript="false" ShowSummary="true" />
        <h4>1. AccountType</h4>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="AccountType" Text="Account Type" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:DropDownList runat="server" ID="AccountType" CssClass="form-control" AutoPostBack="true" OnTextChanged="AccountType_TextChanged">
                    <asp:ListItem Value="0">-- Select Type --</asp:ListItem>
                    <asp:ListItem Value="individual">Individual</asp:ListItem>
                    <asp:ListItem Value="survivorship">Survivorship</asp:ListItem>
                    <asp:ListItem Value="common">Common</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="AccountType" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Account Type is required." InitialValue="0">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <h4>2. Client Information</h4>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="NameTitle" Text="Title" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:DropDownList runat="server" ID="NameTitle" CssClass="form-control" AutoPostBack="false">
                    <asp:ListItem Value="0">-- Select Title --</asp:ListItem>
                    <asp:ListItem Value="mr">Mr.</asp:ListItem>
                    <asp:ListItem Value="mrs">Mrs.</asp:ListItem>
                    <asp:ListItem Value="ms">Ms.</asp:ListItem>
                    <asp:ListItem Value="dr">Dr.</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="NameTitle" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Title is required." InitialValue="0">*</asp:RequiredFieldValidator>
            </div>
         </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="FirstName" Text="First Name" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" MaxLength="35"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName" CssClass="text-danger" ErrorMessage="First Name is required." EnableClientScript="false" Display="Dynamic">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ControlToValidate="FirstName" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="No digits allowed in First Name" ValidationExpression="^[a-zA-Z]{0,999}$">*</asp:RegularExpressionValidator>
            </div>
            <asp:Label runat="server" AssociatedControlID="LastName" Text="Last Name" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox runat="server" ID="LastName" CssClass="form-control" MaxLength="35"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName" CssClass="text-danger" ErrorMessage="Last Name is required." EnableClientScript="false" Display="Dynamic">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ControlToValidate="LastName" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="No digits allowed in Last Name" ValidationExpression="^[a-zA-Z]{0,999}$">*</asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="DateOfBirth" Text="Date of Birth" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox runat="server" ID="DateOfBirth" CssClass="form-control" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="DateOfBirth" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="Date of Birth is required.">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ControlToValidate="DateOfBirth" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="Date of birth is not valid. Please use format dd/mm/yyyy" ValidationExpression="^(?:(?:31(\/)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/)(?:0?2)\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">*</asp:RegularExpressionValidator>
            </div>
            <asp:Label runat="server" AssociatedControlID="Email" Text="Email" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox runat="server" ID="Email" CssClass="form-control" MaxLength="30"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="Email is required." >*</asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="HomePhone" Text="Home Phone" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox runat="server" ID="HomePhone" CssClass="form-control" MaxLength="8" OnTextChanged="HomePhone_TextChanged"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="HomePhone" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Home Phone is required." ID="rfvHomePhone">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ControlToValidate="HomePhone" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Only digits are allowed." ValidationExpression="^[0-9]{0,8}$">*</asp:RegularExpressionValidator>
            </div>
            <asp:Label runat="server" AssociatedControlID="BusinessPhone" Text="Business Phone" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox runat="server" ID="BusinessPhone" CssClass="form-control" MaxLength="8" OnTextChanged="BusinessPhone_TextChanged"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="BusinessPhone" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Business Phone is required." ID="rfvBusinessPhone">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ControlToValidate="BusinessPhone" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Only digits are allowed." ValidationExpression="^[0-9]{0,8}$">*</asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="MobilePhone" Text="Mobile Phone" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox runat="server" ID="MobilePhone" CssClass="form-control" MaxLength="8" OnTextChanged="MobilePhone_TextChanged"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="MobilePhone" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Mobile Phone is required." ID="rfvMobilePhone">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ControlToValidate="MobilePhone" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Only digits are allowed." ValidationExpression="^[0-9]{0,8}$">*</asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Building"  Text="Building" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox runat="server" ID="Building" CssClass="form-control" MaxLength="50"></asp:TextBox>
            </div>
            <asp:Label runat="server" AssociatedControlID="HomeFax" Text="Home Fax" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox runat="server" ID="HomeFax" CssClass="form-control" MaxLength="8"></asp:TextBox>
                <asp:RegularExpressionValidator runat="server" ControlToValidate="HomeFax" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Only digits are allowed." ValidationExpression="^[0-9]{0,8}$">*</asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Street" Text="Street" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox runat="server" ID="Street" CssClass="form-control" MaxLength="35"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Street" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Street is required.">*</asp:RequiredFieldValidator>
            </div>
            <asp:Label runat="server" AssociatedControlID="District" Text="District" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox runat="server" ID="District" CssClass="form-control" MaxLength="19"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="District" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="District is required.">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="CountryOfCitizenship" Text="Country of Citizenship" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox runat="server" ID="CountryOfCitizenship" CssClass="form-control" MaxLength="70"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="CountryOfCitizenship" CssClass="text-danger" EnableClientScript="false" ErrorMessage="Country of Citizenship is required." Display="Dynamic">*</asp:RequiredFieldValidator>
            </div>
            <asp:Label runat="server" AssociatedControlID="CountryOfLegalResidence" Text="Country of Legal Residence" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox runat="server" ID="CountryOfLegalResidence" CssClass="form-control" MaxLength="70"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="CountryOfLegalResidence" CssClass="text-danger" EnableClientScript="false" ErrorMessage="Country of Legal Residence is required." Display="Dynamic">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="HKID" Text="HKID/Passport Number" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox runat="server" ID="HKID" CssClass="form-control" MaxLength="8"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="HKID" CssClass="text-danger" EnableClientScript="false" ErrorMessage="HKID/Passport Number is required" Display="Dynamic">*</asp:RequiredFieldValidator>
            </div>
            <asp:Label runat="server" AssociatedControlID="CountryOfIssue" Text="Passport Country of Issue" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox runat="server" ID="CountryOfIssue" CssClass="form-control" MaxLength="70"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="CountryOfIssue" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Passport Country of Issue is required.">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <h4>3. Employment Information</h4>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="EmploymentStatus" Text="Employment Status" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:DropDownList runat="server" ID="EmploymentStatus" CssClass="form-control" AutoPostBack="True" OnTextChanged="EmploymentStatus_TextChanged" >
                    <asp:ListItem Value="0">-- Select Status --</asp:ListItem>
                    <asp:ListItem Value="employed">Employed</asp:ListItem>
                    <asp:ListItem Value="selfemployed">Self-Employed</asp:ListItem>
                    <asp:ListItem Value="retired">Retired</asp:ListItem>
                    <asp:ListItem Value="student">Student</asp:ListItem>
                    <asp:ListItem Value="notemployed">Not Employed</asp:ListItem>
                    <asp:ListItem Value="homemaker">Homemaker</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="EmploymentStatus" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Employment Status is required." InitialValue="0">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <asp:Panel ID="EmploymentInformation" runat="server">
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="SpecificOccupation" Text="Specific Occupation" CssClass="control-label col-md-3" ID="lblSpecificOccupation"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="SpecificOccupation" CssClass="form-control" MaxLength="20"></asp:TextBox>
                </div>
                <asp:Label runat="server" AssociatedControlID="YearsWithEmployer" Text="Years With Employer" CssClass="control-label col-md-3" ID="lblYearsWithEmployer"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="YearsWithEmployer" CssClass="form-control" MaxLength="2"></asp:TextBox>
                    <asp:RegularExpressionValidator runat="server" ErrorMessage="Must be a positive integer." CssClass="text-danger" Display="Dynamic" Text="*" ControlToValidate="YearsWithEmployer" EnableClientScript="False" ValidationExpression="^[1-9]\d*$"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="EmployerName" Text="Employer Name" CssClass="control-label col-md-3" ID="lblEmployerName"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="EmployerName" CssClass="form-control" MaxLength="25"></asp:TextBox>
                </div>
                <asp:Label runat="server" AssociatedControlID="EmployerPhone" Text="Employer Phone" CssClass="control-label col-md-3" ID="lblEmployerPhone"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="EmployerPhone" CssClass="form-control" MaxLength="8"></asp:TextBox>            
                    <asp:RegularExpressionValidator runat="server" ControlToValidate="YearsWithEmployer" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Only digits are allowed." ValidationExpression="^[0-9]{0,8}$">*</asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="NatureOfBusiness" Text="Nature of Business" CssClass="control-label col-md-3" ID="lblNatureOfBusiness"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="NatureOfBusiness" CssClass="form-control" MaxLength="20"></asp:TextBox>
                </div>
            </div>
        </asp:Panel>
        <h4>4. Disclosures and Regulatory Information</h4>
        <h5>Question 1: Are you employed by a registered securities broker/dealer, investment advisor, bank or other financial institution?</h5>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="financialInstitutionEmployment" Text="Answer:" CssClass="control-label col-md-2"></asp:Label>
            <div class="col-md-3">
                <asp:DropdownList runat="server" ID="financialInstitutionEmployment" CssClass="form-control">
                    <asp:ListItem Value="0">-- Select Answer --</asp:ListItem>
                    <asp:ListItem Value="no">No</asp:ListItem>
                    <asp:ListItem Value="yes">Yes</asp:ListItem>
                </asp:DropdownList>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="financialInstitutionEmployment" CssClass="text-danger" Display="Dynamic" ErrorMessage="Financial Employment is required." EnableClientScript="false" InitialValue="0">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <h5>Question 2: Are you a director, 10% shareholder or policy-making officer of publicly traded company?</h5>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="publiclyTradedCompanyOwnership" Text="Answer:" CssClass="control-label col-md-2"></asp:Label>
            <div class="col-md-3">
                <asp:DropdownList runat="server" ID="publiclyTradedCompanyOwnership" CssClass="form-control">
                    <asp:ListItem Value="0">-- Select Answer --</asp:ListItem>
                    <asp:ListItem Value="no">No</asp:ListItem>
                    <asp:ListItem Value="yes">Yes</asp:ListItem>
                </asp:DropdownList>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="publiclyTradedCompanyOwnership" CssClass="text-danger" Display="Dynamic" ErrorMessage="Company Officer is required." EnableClientScript="false" InitialValue="0">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <h5>Describe the primary source of funds deposited to this account:</h5>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="fundSource" Text="Source of Fund" CssClass="control-label col-md-2"></asp:Label>
            <div class="col-md-3">
                <asp:DropDownList runat="server" ID="fundSource" CssClass="form-control" AutoPostBack="true" OnTextChanged="fundSource_TextChanged">
                    <asp:ListItem Value="0">-- Select --</asp:ListItem>
                    <asp:ListItem Value="salarywagessavings">Salary/Wages/Savings</asp:ListItem>
                    <asp:ListItem Value="investmentcapitalgains">Investment/Capital Gains</asp:ListItem>
                    <asp:ListItem Value="familyrelativesinheritance">Family/Relatives/Inheritance</asp:ListItem>
                    <asp:ListItem Value="others">Others (describe)</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="fundSource" Display="Dynamic" CssClass="text-danger" ErrorMessage="Primary Income Souce is required." EnableClientScript="false" InitialValue="0">*</asp:RequiredFieldValidator>
            </div>
            <asp:Label runat="server" AssociatedControlID="fundSourceOthers" Text="Others:" CssClass="control-label col-md-2" ID="lblSourceOfFund"></asp:Label>
            <div class="col-md-4">
                <asp:TextBox runat="server" ID="fundSourceOthers" CssClass="form-control" MaxLength="30"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvFundSourceOthers" runat="server" ControlToValidate="fundSourceOthers" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Others is required.">*</asp:RequiredFieldValidator>
            </div>
        </div>
<!---->
        <asp:Panel ID="coholder" runat="server">
            <h4>2. Client Information, Co-Account Holder</h4>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="NameTitle2" Text="Title" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:DropDownList runat="server" ID="NameTitle2" CssClass="form-control" AutoPostBack="false">
                        <asp:ListItem Value="0">-- Select Title --</asp:ListItem>
                        <asp:ListItem Value="mr">Mr.</asp:ListItem>
                        <asp:ListItem Value="mrs">Mrs.</asp:ListItem>
                        <asp:ListItem Value="ms">Ms.</asp:ListItem>
                        <asp:ListItem Value="dr">Dr.</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="NameTitle2" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Title is required." InitialValue="0">*</asp:RequiredFieldValidator>
                </div>
             </div>
             <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="FirstName2" Text="First Name" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="FirstName2" CssClass="form-control" MaxLength="35"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName2" CssClass="text-danger" ErrorMessage="First Name is required." EnableClientScript="false" Display="Dynamic">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ControlToValidate="FirstName2" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="No digits allowed in First Name" ValidationExpression="^[a-zA-Z]{0,999}$">*</asp:RegularExpressionValidator>
                </div>
                    <asp:Label runat="server" AssociatedControlID="LastName2" Text="Last Name" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="LastName2" CssClass="form-control" MaxLength="35"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="LastNAme2" CssClass="text-danger" ErrorMessage="Last Name is required." EnableClientScript="false" Display="Dynamic">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ControlToValidate="LastName2" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="No digits allowed in Last Name" ValidationExpression="^[a-zA-Z]{0,999}$">*</asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="DateOfBirth2" Text="Date of Birth" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="DateOfBirth2" CssClass="form-control" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="DateOfBirth2" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="Date of Birth is required.">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ControlToValidate="DateOfBirth2" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="Date of birth is not valid. Please use format dd/mm/yyyy" ValidationExpression="^(?:(?:31(\/)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/)(?:0?2)\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">*</asp:RegularExpressionValidator>
                </div>
                <asp:Label runat="server" AssociatedControlID="Email2" Text="Email" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="Email2" CssClass="form-control" MaxLength="30"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="Email2" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="Email is required." >*</asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="HomePhone2" Text="Home Phone" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="HomePhone2" CssClass="form-control" MaxLength="8" OnTextChanged="HomePhone2_TextChanged" AutoPostBack="True"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="HomePhone2" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Home Phone is required." ID="rfvHomePhone2">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ControlToValidate="HomePhone2" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Only digits are allowed." ValidationExpression="^[0-9]{0,8}$">*</asp:RegularExpressionValidator>
                </div>
               <asp:Label runat="server" AssociatedControlID="BusinessPhone2" Text="Business Phone" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="BusinessPhone2" CssClass="form-control" MaxLength="8" OnTextChanged="BusinessPhone2_TextChanged" AutoPostBack="True"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="BusinessPhone2" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Business Phone is required." ID="rfvBusinessPhone2">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ControlToValidate="BusinessPhone2" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Only digits are allowed." ValidationExpression="^[0-9]{0,8}$">*</asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="MobilePhone2" Text="Mobile Phone" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="MobilePhone2" CssClass="form-control" MaxLength="8" OnTextChanged="MobilePhone2_TextChanged" AutoPostBack="True"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="MobilePhone2" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Mobile Phone is required." ID="rfvMobilePhone2">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ControlToValidate="MobilePhone2" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Only digits are allowed." ValidationExpression="^[0-9]{0,8}$">*</asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Building2"  Text="Building" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="Building2" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
                <asp:Label runat="server" AssociatedControlID="HomeFax2" Text="Home Fax" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="HomeFax2" CssClass="form-control" MaxLength="8"></asp:TextBox>
                    <asp:RegularExpressionValidator runat="server" ControlToValidate="HomeFax2" CssClass="texr-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Only digits are allowed." ValidationExpression="^[0-9]{0,8}$">*</asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Street2" Text="Street" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="Street2" CssClass="form-control" MaxLength="35"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="Street2" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Street is required.">*</asp:RequiredFieldValidator>
                </div>
                <asp:Label runat="server" AssociatedControlID="District2" Text="District" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="District2" CssClass="form-control" MaxLength="19"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="District2" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="District is required.">*</asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="CountryOfCitizenship2" Text="Country of Citizenship" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="CountryOfCitizenship2" CssClass="form-control" MaxLength="70"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="CountryOfCitizenship2" CssClass="text-danger" EnableClientScript="false" ErrorMessage="Country of Citizenship is required." Display="Dynamic">*</asp:RequiredFieldValidator>
                </div>
                <asp:Label runat="server" AssociatedControlID="CountryOfLegalResidence2" Text="Country of Legal Residence" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="CountryOfLegalResidence2" CssClass="form-control" MaxLength="70"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="CountryOfLegalResidence2" CssClass="text-danger" EnableClientScript="false" ErrorMessage="Country of Legal Residence is required." Display="Dynamic">*</asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="HKID2" Text="HKID/Passport Number" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="HKID2" CssClass="form-control" MaxLength="8"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="HKID2" CssClass="text-danger" EnableClientScript="false" ErrorMessage="HKID/Passport Number is required" Display="Dynamic">*</asp:RequiredFieldValidator>
                </div>
                <asp:Label runat="server" AssociatedControlID="CountryOfIssue2" Text="Passport Country of Issue" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox runat="server" ID="CountryOfIssue2" CssClass="form-control" MaxLength="70"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="CountryOfIssue2" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Passport Country of Issue is required.">*</asp:RequiredFieldValidator>
                </div>
            </div>
            <h4>3. Employment Information, Co-Account Holder</h4>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="EmploymentStatus2" Text="Employment Status" CssClass="control-label col-md-3"></asp:Label>
                <div class="col-md-3">
                    <asp:DropDownList runat="server" ID="EmploymentStatus2" CssClass="form-control" AutoPostBack="True" OnTextChanged="EmploymentStatus2_TextChanged" >
                        <asp:ListItem Value="0">-- Select Status --</asp:ListItem>
                        <asp:ListItem Value="employed">Employed</asp:ListItem>
                        <asp:ListItem Value="selfemployed">Self-Employed</asp:ListItem>
                        <asp:ListItem Value="retired">Retired</asp:ListItem>
                        <asp:ListItem Value="student">Student</asp:ListItem>
                        <asp:ListItem Value="notemployed">Not Employed</asp:ListItem>
                        <asp:ListItem Value="homemaker">Homemaker</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="EmploymentStatus2" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Employment Status is required." InitialValue="0">*</asp:RequiredFieldValidator>
                </div>
            </div>
            <asp:Panel ID="EmploymentInformation2" runat="server">
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="SpecificOccupation2" Text="Specific Occupation" CssClass="control-label col-md-3" ID="lblSpecificOccupation2"></asp:Label>
                    <div class="col-md-3">
                        <asp:TextBox runat="server" ID="SpecificOccupation2" CssClass="form-control" MaxLength="20"></asp:TextBox>
                    </div>
                    <asp:Label runat="server" AssociatedControlID="YearsWithEmployer2" Text="Years With Employer" CssClass="control-label col-md-3" ID="lblYearsWithEmployer2"></asp:Label>
                    <div class="col-md-3">
                        <asp:TextBox runat="server" ID="YearsWithEmployer2" CssClass="form-control" MaxLength="2"></asp:TextBox>
                        <asp:RegularExpressionValidator runat="server" ErrorMessage="Must be a positive integer." CssClass="text-danger" Display="Dynamic" Text="*" ControlToValidate="YearsWithEmployer2" EnableClientScript="False" ValidationExpression="^[1-9]\d*$"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="EmployerName2" Text="Employer Name" CssClass="control-label col-md-3" ID="lblEmployerName2"></asp:Label>
                    <div class="col-md-3">
                        <asp:TextBox runat="server" ID="EmployerName2" CssClass="form-control" MaxLength="25"></asp:TextBox>
                    </div>
                    <asp:Label runat="server" AssociatedControlID="EmployerPhone2" Text="Employer Phone" CssClass="control-label col-md-3" ID="lblEmployerPhone2"></asp:Label>
                    <div class="col-md-3">
                        <asp:TextBox runat="server" ID="EmployerPhone2" CssClass="form-control" MaxLength="8"></asp:TextBox>            
                        <asp:RegularExpressionValidator runat="server" ControlToValidate="EmployerPhone2" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Only digits are allowed." ValidationExpression="^[0-9]{0,8}$">*</asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="NatureOfBusiness2" Text="Nature of Business" CssClass="control-label col-md-3" ID="lblNatureOfBusiness2"></asp:Label>
                    <div class="col-md-3">
                        <asp:TextBox runat="server" ID="NatureOfBusiness2" CssClass="form-control" MaxLength="20"></asp:TextBox>
                    </div>
                </div>
            </asp:Panel>
            <h4>4. Disclosures and Regulatory Information, Co-Account Holder</h4>
            <h5>Question 1: Are you employed by a registered securities broker/dealer, investment advisor, bank or other financial institution?</h5>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="financialInstitutionEmployment2" Text="Answer:" CssClass="control-label col-md-2"></asp:Label>
                <div class="col-md-3">
                    <asp:DropdownList runat="server" ID="financialInstitutionEmployment2" CssClass="form-control">
                        <asp:ListItem Value="0">-- Select Answer --</asp:ListItem>
                        <asp:ListItem Value="no">No</asp:ListItem>
                        <asp:ListItem Value="yes">Yes</asp:ListItem>
                    </asp:DropdownList>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="financialInstitutionEmployment2" CssClass="text-danger" Display="Dynamic" ErrorMessage="Question 1 of Section 4 is required." EnableClientScript="false" InitialValue="0">*</asp:RequiredFieldValidator>
                </div>
            </div>
            <h5>Question 2: Are you a director, 10% shareholder or policy-making officer of publicly traded company?</h5>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="publiclyTradedCompanyOwnership2" Text="Answer:" CssClass="control-label col-md-2"></asp:Label>
                <div class="col-md-3">
                    <asp:DropdownList runat="server" ID="publiclyTradedCompanyOwnership2" CssClass="form-control">
                        <asp:ListItem Value="0">-- Select Answer --</asp:ListItem>
                        <asp:ListItem Value="no">No</asp:ListItem>
                        <asp:ListItem Value="yes">Yes</asp:ListItem>
                    </asp:DropdownList>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="publiclyTradedCompanyOwnership2" CssClass="text-danger" Display="Dynamic" ErrorMessage="Question 2 of Section 4 is required." EnableClientScript="false" InitialValue="0">*</asp:RequiredFieldValidator>
                </div>
            </div>
            <h5>Describe the primary source of funds deposited to this account:</h5>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="fundSource2" Text="Source of Fund" CssClass="control-label col-md-2"></asp:Label>
                <div class="col-md-3">
                    <asp:DropDownList runat="server" ID="fundSource2" CssClass="form-control" AutoPostBack="true" OnTextChanged="fundSource_TextChanged">
                        <asp:ListItem Value="0">-- Select --</asp:ListItem>
                        <asp:ListItem Value="salarywagessavings">Salary/Wages/Savings</asp:ListItem>
                        <asp:ListItem Value="investmentcapitalgains">Investment/Capital Gains</asp:ListItem>
                        <asp:ListItem Value="familyrelativesinheritance">Family/Relatives/Inheritance</asp:ListItem>
                        <asp:ListItem Value="others">Others (describe)</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="fundSource2" Display="Dynamic" CssClass="text-danger" ErrorMessage="Source of Fund is required." EnableClientScript="false" InitialValue="0">*</asp:RequiredFieldValidator>
                </div>
                <asp:Label runat="server" AssociatedControlID="fundSourceOthers2" Text="Others:" CssClass="control-label col-md-2" ID="lblSourceOfFund2"></asp:Label>
                <div class="col-md-4">
                    <asp:TextBox runat="server" ID="fundSourceOthers2" CssClass="form-control" MaxLength="30"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvFundSourceOthers2" runat="server" ControlToValidate="fundSourceOthers2" CssClass="text-danger" Display="Dynamic" EnableClientScript="False" ErrorMessage="Others is required.">*</asp:RequiredFieldValidator>
                </div>
            </div>
        </asp:Panel>
<!---->
        <h4>5. Investment Profile</h4>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="InvestmentObjective" Text="Investment objective for this account:" CssClass="control-label col-md-4"></asp:Label>
            <div class="col-md-4">
                <asp:DropDownList runat="server" ID="InvestmentObjective" CssClass="form-control">
                    <asp:ListItem Value="0">-- Select --</asp:ListItem>
                    <asp:ListItem Value="capitalpreservation">Capital Preservation</asp:ListItem>
                    <asp:ListItem Value="income">Income</asp:ListItem>
                    <asp:ListItem Value="growth">Growth</asp:ListItem>
                    <asp:ListItem Value="speculation">Speculation</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="InvestmentObjective" Display="Dynamic" CssClass="text-danger" ErrorMessage="Investment Objective is required." EnableClientScript="false" InitialValue="0">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="InvestmentKnowledge" Text="Investment Knowledge:" CssClass="control-label col-md-4"></asp:Label>
            <div class="col-md-4">
                <asp:DropDownList runat="server" ID="InvestmentKnowledge" CssClass="form-control">
                    <asp:ListItem Value="0">-- Select --</asp:ListItem>
                    <asp:ListItem Value="none">None</asp:ListItem>
                    <asp:ListItem Value="limited">Limited</asp:ListItem>
                    <asp:ListItem Value="good">Good</asp:ListItem>
                    <asp:ListItem Value="extensive">Extensive</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="InvestmentKnowledge" Display="Dynamic" CssClass="text-danger" ErrorMessage="Investment Knowledge is required." EnableClientScript="false" InitialValue="0">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="InvestmentExperience" Text="Investment Experiece:" CssClass="control-label col-md-4"></asp:Label>
            <div class="col-md-4">
                <asp:DropDownList runat="server" ID="InvestmentExperience" CssClass="form-control">
                    <asp:ListItem Value="0">-- Select --</asp:ListItem>
                    <asp:ListItem Value="none">None</asp:ListItem>
                    <asp:ListItem Value="limited">Limited</asp:ListItem>
                    <asp:ListItem Value="good">Good</asp:ListItem>
                    <asp:ListItem Value="extensive">Extensive</asp:ListItem>
                </asp:DropDownList>            
                <asp:RequiredFieldValidator runat="server" ControlToValidate="InvestmentKnowledge" Display="Dynamic" CssClass="text-danger" ErrorMessage="Investment Experience is required." EnableClientScript="false" InitialValue="0">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="AnnualIncome" Text="Annual Income:" CssClass="control-label col-md-4"></asp:Label> 
            <div class="col-md-4">
                <asp:DropDownList runat="server" ID="AnnualIncome" CssClass="form-control">
                    <asp:ListItem Value="0">-- Select --</asp:ListItem>
                    <asp:ListItem Value="under20000">under HK$20,000</asp:ListItem>
                    <asp:ListItem Value="20001to200000">HK$20,001 - HK$200,000</asp:ListItem>
                    <asp:ListItem Value="200001to2000000">HK$200,001 - HK$2,000,000</asp:ListItem>
                    <asp:ListItem Value="over2000000">more than HK$2,000,000</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="AnnualIncome" Display="Dynamic" CssClass="text-danger" ErrorMessage="Annual Income is required." EnableClientScript="false" InitialValue="0">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="LiquidNetWorth" Text="Approximate liquid net worth (cash and securities)" CssClass="control-label col-md-4"></asp:Label>
            <div class="col-md-4">
                <asp:DropDownList runat="server" ID="LiquidNetWorth" CssClass="form-control">
                    <asp:ListItem Value="0">-- Select --</asp:ListItem>
                    <asp:ListItem Value="under100000">under HK$100,000</asp:ListItem>
                    <asp:ListItem Value="100001to1000000">HK$100,001 - HK$1,000,000</asp:ListItem>
                    <asp:ListItem Value="1000001to10000000">HK$1,000,001 - HK$10,000,000</asp:ListItem>
                    <asp:ListItem Value="over10000000">more than HK$10,000,000</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="LiquidNetWorth" Display="Dynamic" CssClass="text-danger" ErrorMessage="Liquid net worth is required." EnableClientScript="false" InitialValue="0">*</asp:RequiredFieldValidator>
            </div>
        </div>
<!----> <h4>6. Account Feature</h4>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Sweep" Text="Sweep Account Feature:" CssClass="control-label col-md-4"></asp:Label>
            <div class="col-md-4">
                <asp:DropDownList runat="server" ID="Sweep" CssClass="form-control">
                    <asp:ListItem Value="0">-- Select --</asp:ListItem>
                    <asp:ListItem Value="yes">Yes</asp:ListItem>
                    <asp:ListItem Value="no">No</asp:ListItem>
                </asp:DropDownList>            
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Sweep" Display="Dynamic" CssClass="text-danger" ErrorMessage="Sweep Account Feature is required." EnableClientScript="false" InitialValue="0">*</asp:RequiredFieldValidator>
            </div>
        </div>
<!---->
        <h4>7. Initial Account Deposit</h4>
        <div class="form-group">
            <asp:Label runat="server" Text="Cheque" AssociatedControlID="Cheque" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox ID="Cheque" runat="server" CssClass="form-control" OnTextChanged="Cheque_TextChanged" AutoPostBack="True"></asp:TextBox>
            </div>
            <asp:Label runat="server" Text="Transfer" AssociatedControlID="Transfer" CssClass="control-label col-md-3"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox ID="Transfer" runat="server" CssClass="form-control" OnTextChanged="Transfer_TextChanged" AutoPostBack="True"></asp:TextBox>
            </div>
        </div>
        <asp:RequiredFieldValidator runat="server" ErrorMessage="Initial Deposit is required" EnableClientScript="False" ControlToValidate="Total" Text="*" CssClass="text-danger"></asp:RequiredFieldValidator>
        <asp:RangeValidator runat="server" ErrorMessage="The initial deposit must be at least HK$20000 and must not exceed HK$999999999." MinimumValue="20000" Text="*" EnableClientScript="False" ControlToValidate="Total" Type="Double" MaximumValue="999999999" CssClass="text-danger"></asp:RangeValidator>
        <asp:Label runat="server" Text="Total" AssociatedControlID="Total" CssClass="control-label col-md-3" Visible="False"></asp:Label>
        <div class="col-md-3">
                <asp:TextBox ID="Total" runat="server" CssClass="form-control" Visible="False"></asp:TextBox>
            </div>

        <div class="form-group">
            <div class="col-md-offset-1 col-md-11"><asp:Button runat="server" ID="Register" Text="Register" CssClass="btn button-default" OnClick="Register_Click" /></div>
        </div>
    </div>
</asp:Content>
