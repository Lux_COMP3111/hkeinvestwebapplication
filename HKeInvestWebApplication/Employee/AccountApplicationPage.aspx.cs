﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HKeInvestWebApplication.Code_File;

namespace HKeInvestWebApplication
{
    public partial class AccountApplicationPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (AccountType.SelectedIndex == 0)
            {
                coholder.Visible = false;
                coholder.Enabled = false;
            }
            if (fundSource.SelectedValue != "others")
            {
                fundSourceOthers.Enabled = false;
                rfvFundSourceOthers.Enabled = false;
            }
            if (fundSource2.SelectedValue != "others")
            {
                fundSourceOthers2.Enabled = false;
                rfvFundSourceOthers2.Enabled = false;
            }
        }

        protected void AccountType_TextChanged(object sender, EventArgs e)
        {
            if (AccountType.SelectedValue != "individual")
            {
                coholder.Enabled = true;
                coholder.Visible = true;
            }
            else
            {
                coholder.Enabled = false;
                coholder.Visible = false;
            }
        }
        protected void EmploymentStatus_TextChanged(object sender, EventArgs e)
        {
            if (EmploymentStatus.SelectedValue != "employed")
            {
                EmploymentInformation.Enabled = false;
                EmploymentInformation.Visible = false;
            }
            else
            {
                EmploymentInformation.Enabled = true;
                EmploymentInformation.Visible = true;
            }
        }
        protected void EmploymentStatus2_TextChanged(object sender, EventArgs e)
        {
            if (EmploymentStatus2.SelectedValue != "employed")
            {
                EmploymentInformation2.Enabled = false;
                EmploymentInformation2.Visible = false;
            }
            else
            {
                EmploymentInformation2.Enabled = true;
                EmploymentInformation2.Visible = true;
            }
        }

        protected void HomePhone_TextChanged(object sender, EventArgs e)
        {
            if (HomePhone.Text.Trim() != "")
            {
                rfvBusinessPhone.Enabled = false;
                rfvMobilePhone.Enabled = false;
            }
            else
            {
                rfvBusinessPhone.Enabled = true;
                rfvMobilePhone.Enabled = true;
            }
        }
        protected void BusinessPhone_TextChanged(object sender, EventArgs e)
        {
            if (BusinessPhone.Text.Trim() != "")
            {
                rfvHomePhone.Enabled = false;
                rfvMobilePhone.Enabled = false;
            }
            else
            {
                rfvHomePhone.Enabled = true;
                rfvMobilePhone.Enabled = true;
            }
        }
        protected void MobilePhone_TextChanged(object sender, EventArgs e)
        {
            if (MobilePhone.Text.Trim() != "")
            {
                rfvBusinessPhone.Enabled = false;
                rfvHomePhone.Enabled = false;
            }
            else
            {
                rfvBusinessPhone.Enabled = true;
                rfvHomePhone.Enabled = true;
            }
        }
        protected void HomePhone2_TextChanged(object sender, EventArgs e)
        {
            if (HomePhone2.Text.Trim() != "")
            {
                rfvBusinessPhone2.Enabled = false;
                rfvMobilePhone2.Enabled = false;
            }
            else
            {
                rfvBusinessPhone2.Enabled = true;
                rfvMobilePhone2.Enabled = true;
            }
        }
        protected void BusinessPhone2_TextChanged(object sender, EventArgs e)
        {
            if (BusinessPhone2.Text.Trim() != "")
            {
                rfvHomePhone2.Enabled = false;
                rfvMobilePhone2.Enabled = false;
            }
            else
            {
                rfvHomePhone2.Enabled = true;
                rfvMobilePhone2.Enabled = true;
            }
        }
        protected void MobilePhone2_TextChanged(object sender, EventArgs e)
        {
            if (MobilePhone2.Text.Trim() != "")
            {
                rfvBusinessPhone2.Enabled = false;
                rfvHomePhone2.Enabled = false;
            }
            else
            {
                rfvBusinessPhone2.Enabled = true;
                rfvHomePhone2.Enabled = true;
            }
        }
        protected void fundSource_TextChanged(object sender, EventArgs e)
        {
            if (fundSource.SelectedValue != "others")
            {
                fundSourceOthers.Enabled = false;
                rfvFundSourceOthers.Enabled = false;
            }
            else
            {
                fundSourceOthers.Enabled = true;
                rfvFundSourceOthers.Enabled = true;
            }
        }

        protected void Cheque_TextChanged(object sender, EventArgs e)
        {
            if (Cheque.Text != "")
            {
                Transfer.Enabled = false;
                Total.Text = Convert.ToString(Convert.ToDouble(Cheque.Text.Trim()));
            }
            else
            {
                Transfer.Enabled = true;
                Total.Text = "0";
            }
        }
        protected void Transfer_TextChanged(object sender, EventArgs e)
        {
            if (Transfer.Text.Trim() != "")
            {
                Cheque.Enabled = false;
                Total.Text = Convert.ToString(Convert.ToDouble(Transfer.Text.Trim()));
            }
            else
            {
                Cheque.Enabled = true;
                Total.Text = "0";
            }
        }

        protected void Register_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                HKeInvestData myClientData = new HKeInvestData();
                string lastName = LastName.Text.Trim().Replace(" ", string.Empty).Substring(0, 2).ToUpper(); ;
                if (lastName.Length == 1)
                {
                    lastName = lastName + lastName;
                }
                string number = "";
                string sql = "select count(*) from Account where accountNumber Like '" + lastName + "%';";
                decimal count = myClientData.getAggregateValue(sql) + 1;
                sql = "select count(*) from Client where HKIDPassportNumber = '" + HKID.Text.Trim() + "';";
                decimal checkexist = myClientData.getAggregateValue(sql);
                sql = "select count(*) from Client where HKIDPassportNumber = '" + HKID2.Text.Trim() + "';";
                decimal checkexist2 = myClientData.getAggregateValue(sql);
                if (checkexist == 1 || checkexist2 == 1)
                {
                    Response.Write("<script language=javascript>alert('A client with the same HKID/Passport Number already exists.')</script>");
                    return;
                }
                if (HKID.Text.Trim() == HKID2.Text.Trim())
                {
                    Response.Write("<script language=javascript>alert('Primary account holder and coholder cannot have the same HKID/Passport Number.')</script>");
                    return;
                }
                number = lastName + count.ToString("00000000");
                SqlTransaction trans = myClientData.beginTransaction();
                myClientData.commitTransaction(trans);

                if (AccountType.SelectedValue == "individual" && fundSource.SelectedValue != "others")
                {
                    //Individual fundsource
                    trans = myClientData.beginTransaction();
                    myClientData.setData("INSERT [Account] VALUES ('" + number + "', '" + AccountType.SelectedValue.Trim() + "', '" + InvestmentObjective.Text.Trim() + "', '" + InvestmentKnowledge.Text.Trim() + "', '" + InvestmentExperience.Text.Trim() + "', '" + AnnualIncome.SelectedValue.Trim() + "', '" + LiquidNetWorth.SelectedValue.Trim() + "', '" + Total.Text.Trim() + "', '" + Sweep.SelectedValue.Trim() + "', ' ');", trans);
                    myClientData.commitTransaction(trans);
                    trans = myClientData.beginTransaction();
                    myClientData.setData("INSERT [Client] VALUES ('" + NameTitle.SelectedValue.Trim() + "', '" + FirstName.Text.Trim() + "', '" + LastName.Text.Trim() + "', CONVERT(date, '" + DateOfBirth.Text.Trim() + "', 103), '" + Email.Text.Trim() + "', '" + Building.Text.Trim() + "', '" + Street.Text.Trim() + "', '" + District.Text.Trim() + "', '" + HomePhone.Text.Trim() + "', '" + BusinessPhone.Text.Trim() + "', '" + MobilePhone.Text.Trim() + "', '" + HomeFax.Text.Trim() + "', '" + CountryOfCitizenship.Text.Trim() + "', '" + CountryOfLegalResidence.Text.Trim() + "', '" + HKID.Text.Trim() + "', '" + CountryOfIssue.Text.Trim() + "', '" + EmploymentStatus.SelectedValue.Trim() + "', '" + SpecificOccupation.Text.Trim() + "', '" + YearsWithEmployer.Text.Trim() + "', '" + EmployerName.Text.Trim() + "', '" + EmployerPhone.Text.Trim() + "', '" + NatureOfBusiness.Text.Trim() + "', '" + financialInstitutionEmployment.SelectedValue.Trim() + "', '" + publiclyTradedCompanyOwnership.SelectedValue.Trim() + "', '" + fundSource.SelectedValue.Trim() + "', '" + number.Trim() + "', 'primary');", trans);
                    myClientData.commitTransaction(trans);
                }
                else if (AccountType.SelectedValue == "individual" && fundSource.SelectedValue == "others")
                {
                    //Individual fundsource others
                    trans = myClientData.beginTransaction();
                    myClientData.setData("INSERT [Account] VALUES ('" + number + "', '" + AccountType.SelectedValue.Trim() + "', '" + InvestmentObjective.Text.Trim() + "', '" + InvestmentKnowledge.Text.Trim() + "', '" + InvestmentExperience.Text.Trim() + "', '" + AnnualIncome.SelectedValue.Trim() + "', '" + LiquidNetWorth.SelectedValue.Trim() + "', '" + Total.Text.Trim() + "', '" + Sweep.SelectedValue.Trim() + "', ' ');", trans);
                    myClientData.commitTransaction(trans);
                    trans = myClientData.beginTransaction();
                    myClientData.setData("INSERT [Client] VALUES ('" + NameTitle.SelectedValue.Trim() + "', '" + FirstName.Text.Trim() + "', '" + LastName.Text.Trim() + "', CONVERT(date, '" + DateOfBirth.Text.Trim() + "', 103), '" + Email.Text.Trim() + "', '" + Building.Text.Trim() + "', '" + Street.Text.Trim() + "', '" + District.Text.Trim() + "', '" + HomePhone.Text.Trim() + "', '" + BusinessPhone.Text.Trim() + "', '" + MobilePhone.Text.Trim() + "', '" + HomeFax.Text.Trim() + "', '" + CountryOfCitizenship.Text.Trim() + "', '" + CountryOfLegalResidence.Text.Trim() + "', '" + HKID.Text.Trim() + "', '" + CountryOfIssue.Text.Trim() + "', '" + EmploymentStatus.SelectedValue.Trim() + "', '" + SpecificOccupation.Text.Trim() + "', '" + YearsWithEmployer.Text.Trim() + "', '" + EmployerName.Text.Trim() + "', '" + EmployerPhone.Text.Trim() + "', '" + NatureOfBusiness.Text.Trim() + "', '" + financialInstitutionEmployment.SelectedValue.Trim() + "', '" + publiclyTradedCompanyOwnership.SelectedValue.Trim() + "', '" + fundSourceOthers.Text.Trim() + "', '" + number.Trim() + "', 'primary');", trans);
                    myClientData.commitTransaction(trans);
                }
                else if (AccountType.SelectedValue != "individual" && fundSource.SelectedValue == "others" && fundSource2.SelectedValue != "others")
                {
                    //Joint fundsource
                    trans = myClientData.beginTransaction();
                    myClientData.setData("INSERT [Account] VALUES ('" + number + "', '" + AccountType.SelectedValue.Trim() + "', '" + InvestmentObjective.Text.Trim() + "', '" + InvestmentKnowledge.Text.Trim() + "', '" + InvestmentExperience.Text.Trim() + "', '" + AnnualIncome.SelectedValue.Trim() + "', '" + LiquidNetWorth.SelectedValue.Trim() + "', '" + Total.Text.Trim() + "', '" + Sweep.SelectedValue.Trim() + "', ' ');", trans);
                    myClientData.commitTransaction(trans);
                    trans = myClientData.beginTransaction();
                    myClientData.setData("INSERT [Client] VALUES ('" + NameTitle.SelectedValue.Trim() + "', '" + FirstName.Text.Trim() + "', '" + LastName.Text.Trim() + "', CONVERT(date, '" + DateOfBirth.Text.Trim() + "', 103), '" + Email.Text.Trim() + "', '" + Building.Text.Trim() + "', '" + Street.Text.Trim() + "', '" + District.Text.Trim() + "', '" + HomePhone.Text.Trim() + "', '" + BusinessPhone.Text.Trim() + "', '" + MobilePhone.Text.Trim() + "', '" + HomeFax.Text.Trim() + "', '" + CountryOfCitizenship.Text.Trim() + "', '" + CountryOfLegalResidence.Text.Trim() + "', '" + HKID.Text.Trim() + "', '" + CountryOfIssue.Text.Trim() + "', '" + EmploymentStatus.SelectedValue.Trim() + "', '" + SpecificOccupation.Text.Trim() + "', '" + YearsWithEmployer.Text.Trim() + "', '" + EmployerName.Text.Trim() + "', '" + EmployerPhone.Text.Trim() + "', '" + NatureOfBusiness.Text.Trim() + "', '" + financialInstitutionEmployment.SelectedValue.Trim() + "', '" + publiclyTradedCompanyOwnership.SelectedValue.Trim() + "', '" + fundSourceOthers.Text.Trim() + "', '" + number.Trim() + "', 'primary');", trans);
                    myClientData.commitTransaction(trans);
                    trans = myClientData.beginTransaction();
                    myClientData.setData("INSERT [Client] VALUES ('" + NameTitle2.SelectedValue.Trim() + "', '" + FirstName2.Text.Trim() + "', '" + LastName2.Text.Trim() + "', CONVERT(date, '" + DateOfBirth2.Text.Trim() + "', 103), '" + Email2.Text.Trim() + "', '" + Building2.Text.Trim() + "', '" + Street2.Text.Trim() + "', '" + District2.Text.Trim() + "', '" + HomePhone2.Text.Trim() + "', '" + BusinessPhone2.Text.Trim() + "', '" + MobilePhone2.Text.Trim() + "', '" + HomeFax2.Text.Trim() + "', '" + CountryOfCitizenship2.Text.Trim() + "', '" + CountryOfLegalResidence2.Text.Trim() + "', '" + HKID2.Text.Trim() + "', '" + CountryOfIssue2.Text.Trim() + "', '" + EmploymentStatus2.SelectedValue.Trim() + "', '" + SpecificOccupation2.Text.Trim() + "', '" + YearsWithEmployer2.Text.Trim() + "', '" + EmployerName2.Text.Trim() + "', '" + EmployerPhone2.Text.Trim() + "', '" + NatureOfBusiness2.Text.Trim() + "', '" + financialInstitutionEmployment2.SelectedValue.Trim() + "', '" + publiclyTradedCompanyOwnership2.SelectedValue.Trim() + "', '" + fundSource2.SelectedValue.Trim() + "', '" + number.Trim() + "', 'secondary');", trans);
                    myClientData.commitTransaction(trans);
                }
                else if (AccountType.SelectedValue != "individual" && fundSource.SelectedValue != "others" && fundSource2.SelectedValue == "others")
                {
                    //Joint fundsource 1other
                    trans = myClientData.beginTransaction();
                    myClientData.setData("INSERT [Account] VALUES ('" + number + "', '" + AccountType.SelectedValue.Trim() + "', '" + InvestmentObjective.Text.Trim() + "', '" + InvestmentKnowledge.Text.Trim() + "', '" + InvestmentExperience.Text.Trim() + "', '" + AnnualIncome.SelectedValue.Trim() + "', '" + LiquidNetWorth.SelectedValue.Trim() + "', '" + Total.Text.Trim() + "', '" + Sweep.SelectedValue.Trim() + "', ' ');", trans);
                    myClientData.commitTransaction(trans);
                    trans = myClientData.beginTransaction();
                    myClientData.setData("INSERT [Client] VALUES ('" + NameTitle.SelectedValue.Trim() + "', '" + FirstName.Text.Trim() + "', '" + LastName.Text.Trim() + "', CONVERT(date, '" + DateOfBirth.Text.Trim() + "', 103), '" + Email.Text.Trim() + "', '" + Building.Text.Trim() + "', '" + Street.Text.Trim() + "', '" + District.Text.Trim() + "', '" + HomePhone.Text.Trim() + "', '" + BusinessPhone.Text.Trim() + "', '" + MobilePhone.Text.Trim() + "', '" + HomeFax.Text.Trim() + "', '" + CountryOfCitizenship.Text.Trim() + "', '" + CountryOfLegalResidence.Text.Trim() + "', '" + HKID.Text.Trim() + "', '" + CountryOfIssue.Text.Trim() + "', '" + EmploymentStatus.SelectedValue.Trim() + "', '" + SpecificOccupation.Text.Trim() + "', '" + YearsWithEmployer.Text.Trim() + "', '" + EmployerName.Text.Trim() + "', '" + EmployerPhone.Text.Trim() + "', '" + NatureOfBusiness.Text.Trim() + "', '" + financialInstitutionEmployment.SelectedValue.Trim() + "', '" + publiclyTradedCompanyOwnership.SelectedValue.Trim() + "', '" + fundSource.SelectedValue.Trim() + "', '" + number.Trim() + "', 'primary');", trans);
                    myClientData.commitTransaction(trans);
                    trans = myClientData.beginTransaction();
                    myClientData.setData("INSERT [Client] VALUES ('" + NameTitle2.SelectedValue.Trim() + "', '" + FirstName2.Text.Trim() + "', '" + LastName2.Text.Trim() + "', CONVERT(date, '" + DateOfBirth2.Text.Trim() + "', 103), '" + Email2.Text.Trim() + "', '" + Building2.Text.Trim() + "', '" + Street2.Text.Trim() + "', '" + District2.Text.Trim() + "', '" + HomePhone2.Text.Trim() + "', '" + BusinessPhone2.Text.Trim() + "', '" + MobilePhone2.Text.Trim() + "', '" + HomeFax2.Text.Trim() + "', '" + CountryOfCitizenship2.Text.Trim() + "', '" + CountryOfLegalResidence2.Text.Trim() + "', '" + HKID2.Text.Trim() + "', '" + CountryOfIssue2.Text.Trim() + "', '" + EmploymentStatus2.SelectedValue.Trim() + "', '" + SpecificOccupation2.Text.Trim() + "', '" + YearsWithEmployer2.Text.Trim() + "', '" + EmployerName2.Text.Trim() + "', '" + EmployerPhone2.Text.Trim() + "', '" + NatureOfBusiness2.Text.Trim() + "', '" + financialInstitutionEmployment2.SelectedValue.Trim() + "', '" + publiclyTradedCompanyOwnership2.SelectedValue.Trim() + "', '" + fundSourceOthers2.Text.Trim() + "', '" + number.Trim() + "', 'secondary');", trans);
                    myClientData.commitTransaction(trans);
                }
                else if (AccountType.SelectedValue != "individual" && fundSource.SelectedValue == "others" && fundSource2.SelectedValue == "others")
                {
                    //Joint fundsource 1other
                    trans = myClientData.beginTransaction();
                    myClientData.setData("INSERT [Account] VALUES ('" + number + "', '" + AccountType.SelectedValue.Trim() + "', '" + InvestmentObjective.Text.Trim() + "', '" + InvestmentKnowledge.Text.Trim() + "', '" + InvestmentExperience.Text.Trim() + "', '" + AnnualIncome.SelectedValue.Trim() + "', '" + LiquidNetWorth.SelectedValue.Trim() + "', '" + Total.Text.Trim() + "', '" + Sweep.SelectedValue.Trim() + "', ' ');", trans);
                    myClientData.commitTransaction(trans);
                    trans = myClientData.beginTransaction();
                    myClientData.setData("INSERT [Client] VALUES ('" + NameTitle.SelectedValue.Trim() + "', '" + FirstName.Text.Trim() + "', '" + LastName.Text.Trim() + "', CONVERT(date, '" + DateOfBirth.Text.Trim() + "', 103), '" + Email.Text.Trim() + "', '" + Building.Text.Trim() + "', '" + Street.Text.Trim() + "', '" + District.Text.Trim() + "', '" + HomePhone.Text.Trim() + "', '" + BusinessPhone.Text.Trim() + "', '" + MobilePhone.Text.Trim() + "', '" + HomeFax.Text.Trim() + "', '" + CountryOfCitizenship.Text.Trim() + "', '" + CountryOfLegalResidence.Text.Trim() + "', '" + HKID.Text.Trim() + "', '" + CountryOfIssue.Text.Trim() + "', '" + EmploymentStatus.SelectedValue.Trim() + "', '" + SpecificOccupation.Text.Trim() + "', '" + YearsWithEmployer.Text.Trim() + "', '" + EmployerName.Text.Trim() + "', '" + EmployerPhone.Text.Trim() + "', '" + NatureOfBusiness.Text.Trim() + "', '" + financialInstitutionEmployment.SelectedValue.Trim() + "', '" + publiclyTradedCompanyOwnership.SelectedValue.Trim() + "', '" + fundSourceOthers.Text.Trim() + "', '" + number.Trim() + "', 'primary');", trans);
                    myClientData.commitTransaction(trans);
                    trans = myClientData.beginTransaction();
                    myClientData.setData("INSERT [Client] VALUES ('" + NameTitle2.SelectedValue.Trim() + "', '" + FirstName2.Text.Trim() + "', '" + LastName2.Text.Trim() + "', CONVERT(date, '" + DateOfBirth2.Text.Trim() + "', 103), '" + Email2.Text.Trim() + "', '" + Building2.Text.Trim() + "', '" + Street2.Text.Trim() + "', '" + District2.Text.Trim() + "', '" + HomePhone2.Text.Trim() + "', '" + BusinessPhone2.Text.Trim() + "', '" + MobilePhone2.Text.Trim() + "', '" + HomeFax2.Text.Trim() + "', '" + CountryOfCitizenship2.Text.Trim() + "', '" + CountryOfLegalResidence2.Text.Trim() + "', '" + HKID2.Text.Trim() + "', '" + CountryOfIssue2.Text.Trim() + "', '" + EmploymentStatus2.SelectedValue.Trim() + "', '" + SpecificOccupation2.Text.Trim() + "', '" + YearsWithEmployer2.Text.Trim() + "', '" + EmployerName2.Text.Trim() + "', '" + EmployerPhone2.Text.Trim() + "', '" + NatureOfBusiness2.Text.Trim() + "', '" + financialInstitutionEmployment2.SelectedValue.Trim() + "', '" + publiclyTradedCompanyOwnership2.SelectedValue.Trim() + "', '" + fundSourceOthers2.Text.Trim() + "', '" + number.Trim() + "', 'secondary');", trans);
                    myClientData.commitTransaction(trans);
                }
                Response.Write("<script language=javascript>alert('Account Application completed(" + number + ")')</script>");
                Server.Transfer("/default.aspx");
            }
        }
    }
}