﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="HKeInvestWebApplication.Account.Register" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2><%: Title %>.</h2>
    <%--Validation Summary for Validators--%>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <asp:ValidationSummary runat="server" CssClass="text-danger" EnableClientScript="false" />
    
    <%--Largest Div Element--%>
    <div class="form-horizontal">
        <h4>Create a new client account</h4>
        <hr />
        <%--Controls and Labels Arrangement Derived from Lab 3 Notes--%>
        
        <%--Account Number and First Name Div Element--%>
        <div class="form-group">
            <%--Account Number--%>
            <asp:Label runat="server" AssociatedControlID="AccountNumber" Text="Account#" CssClass="control-label col-md-2"></asp:Label>
            <div class="col-md-4">
                <asp:TextBox runat="server" ID="AccountNumber" CssClass="form-control" MaxLength="10"></asp:TextBox>
                <%--Account Number Validators--%>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="AccountNumber" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="Account Number is required." Text="*"></asp:RequiredFieldValidator>
                <%--Account Number Custom Validator--%>
                <asp:CustomValidator runat="server" ID="cvAccountNumber" ControlToValidate="AccountNumber" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="The account number does not match the client's last name." Text="*" ValidateEmptyText="true" OnServerValidate="cvAccountNumber_ServerValidate"></asp:CustomValidator>
            </div>
            <%--First Name--%>
            <asp:Label runat="server" AssociatedControlID="FirstName" Text="First Name" CssClass="control-label col-md-2"></asp:Label> 
            <div class="col-md-4">
                <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" MaxLength="35"></asp:TextBox>
                <%--First Name Validators--%>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="First Name is required." Text="*"></asp:RequiredFieldValidator>             
            </div>
        </div>
        <%--HKID/Passport and Last Name Div Element--%>
        <div class="form-group">
            <%--HKID/Passport Number--%>
            <asp:Label runat="server" AssociatedControlID="HKID" Text="HKID/Passport#" CssClass="control-label col-md-2"></asp:Label>
            <div class="col-md-4">
                <asp:TextBox runat="server" ID="HKID" CssClass="form-control" MaxLength="8"></asp:TextBox>
                <%--HKID/Passport# Validators--%>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="HKID" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="HKID/Passport# is required." Text="*"></asp:RequiredFieldValidator>
            </div>
            <%--Last Name--%>
            <asp:Label runat="server" AssociatedControlID="LastName" Text="Last Name" CssClass="control-label col-md-2"></asp:Label>
            <div class="col-md-4">
                <asp:TextBox runat="server" ID="LastName" CssClass="form-control" MaxLength="35"></asp:TextBox>
                <%--Last Name Validators--%>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="Last Name is required." Text="*"></asp:RequiredFieldValidator> 
            </div>
        </div>
        <%--DOB and Email Div Element--%>
        <div class="form-group">
            <%--Date of Birth--%>
            <asp:Label runat="server" AssociatedControlID="DateOfBirth" Text="Date of Birth" CssClass="control-label col-md-2"></asp:Label>
            <div class="col-md-4">
                <asp:TextBox runat="server" ID="DateOfBirth" CssClass="form-control" MaxLength="10"></asp:TextBox>
                <%--Date of Birth Validators--%>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="DateOfBirth" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="Date of Birth is required." Text="*"></asp:RequiredFieldValidator>
                <%--Regex Validator for Date of Birth to have dd/mm/yyyy format--%>
                <asp:RegularExpressionValidator runat="server" ControlToValidate="DateOfBirth" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="Date of birth is not valid. Please use format dd/mm/yyyy" Text="*"  ValidationExpression="^(?:(?:31(\/)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/)(?:0?2)\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$"></asp:RegularExpressionValidator>
            </div>
            <%--Email--%>
            <asp:Label runat="server" AssociatedControlID="Email" Text="Email" CssClass="control-label col-md-2"></asp:Label>
            <div class="col-md-4">
                <asp:TextBox runat="server" ID="Email" TextMode="Email" CssClass="form-control" MaxLength="30"></asp:TextBox>
                <%--Email Validators--%>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="Email is Required." Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>
        <hr />
        <%--Username Div Element--%>
        <div class="form-group">
            <%--User Name--%>
            <asp:Label runat="server" AssociatedControlID="UserName" Text="User Name" CssClass="control-label col-md-2"></asp:Label>
            <div class="col-md-4">
                <asp:TextBox runat="server" ID="UserName" CssClass="form-control"></asp:TextBox>
                <%--User Name Validators--%>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="UserName" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="User Name is Required." Text="*"></asp:RequiredFieldValidator>
                <%--Regex Validator for User Name to be between 6 to 10 characters, containing only letters and digits (case-insensitive)--%>
                <asp:RegularExpressionValidator runat="server" ControlToValidate ="UserName" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="User Name must be between 6 and 10 charaters and contain only digits and letters (case-insensitive)." Text="*" ValidationExpression="^(?=.{6,10}$)[A-Za-z0-9]*$"></asp:RegularExpressionValidator>
            </div>
        </div>
        <%--Password and Confirm Password Div Element--%>
        <div class="form-group">
            <%--Password--%>
            <asp:Label runat="server" AssociatedControlID="Password" Text="Password" CssClass="control-label col-md-2"></asp:Label>
            <div class="col-md-4">
                <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control"></asp:TextBox>
                <%--Password Validators--%>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="Password is Required." Text="*"></asp:RequiredFieldValidator>
                <%--Regex Validator for Password to be between 8 to 15 characters, containing at least two non-alphanumeric chars (case-sensitive)--%>
                <asp:RegularExpressionValidator runat="server" ControlToValidate ="Password" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="Password must be between 8 and 15 charaters and contain at least two non-alphanumeric characters (case-insensitive)." Text="*" ValidationExpression="^(?=.*[^a-zA-Z\d\s:].*[^a-zA-Z\d\s:]).{8,15}$"></asp:RegularExpressionValidator>
            </div>
            <%--Confirm Password--%>
            <asp:Label runat="server" AssociatedControlID="ConfirmPassword" Text="Confirm Password" CssClass="control-label col-md-2"></asp:Label>
            <div class="col-md-4">
                <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control"></asp:TextBox>
                <%--Confirm Password Validators--%>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="Confirm Password is required." Text="*"></asp:RequiredFieldValidator>
                <%--Compare Validator for Confirm Password--%>
                <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" CssClass="text-danger" Display="Dynamic" EnableClientScript="false" ErrorMessage="Password and Confirm Password do not match." Text="*"></asp:CompareValidator>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" OnClick="CreateUser_Click" Text="Register" CssClass="btn btn-default" />
            </div>
        </div>
    </div>
</asp:Content>
