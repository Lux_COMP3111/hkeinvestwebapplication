﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using HKeInvestWebApplication.Models;
using HKeInvestWebApplication.Code_File;
using System.Data.SqlClient;

namespace HKeInvestWebApplication.Account
{
    public partial class Register : Page
    {
        protected void CreateUser_Click(object sender, EventArgs e)
        {
                if (check_Client_serverSide(FirstName.Text, LastName.Text, DateOfBirth.Text, Email.Text, HKID.Text, AccountNumber.Text))
                {
                    var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
                    var user = new ApplicationUser() { UserName = UserName.Text, Email = Email.Text };
                    IdentityResult result = manager.Create(user, Password.Text);
                    if (result.Succeeded)
                    {
                        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                        //string code = manager.GenerateEmailConfirmationToken(user.Id);
                        //string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
                        //manager.SendEmail(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>.");

                        //Assign the user to Client role
                        IdentityResult roleResult = manager.AddToRole(user.Id, "Client");
                        
                        if (!roleResult.Succeeded)
                        {
                            ErrorMessage.Text = roleResult.Errors.FirstOrDefault();
                        }

                        Add_Username_to_Account(AccountNumber.Text, UserName.Text);
                        signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);

                        //Apparently, the role assignment is only valid after the second log-in.
                        //Therefore, I've added these code to force the client to log-in twice before
                        //they are allowed to access the website after registration.
                        //This still is a security hazard.
                       
                        //signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);

                        IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);

                    }
                    else
                    {
                        ErrorMessage.Text = result.Errors.FirstOrDefault();
                    }
                }
                else
                {
                    ErrorMessage.Text = "Client information does not exist. Please contact HKeInvest service centre for more information.";
                }
        }

        //Private function to record Username into the Account Table (functionality check complete)
        private void Add_Username_to_Account(string accountNumber, string userName)
        {
            HKeInvestData myClientData = new HKeInvestData();
            SqlTransaction trans = myClientData.beginTransaction();
            myClientData.setData("update [Account] set [userName] = '" + userName + "' where [accountNumber] ='" + accountNumber + "';", trans);
            myClientData.commitTransaction(trans);
        }

        //Private function to compare data in Register.aspx form and Database Client information.
        private bool check_Client_serverSide(string firstName, string lastName, string dateOfBirth, string email, string HKID, string accountNumber)
        {
            HKeInvestData myClientData = new HKeInvestData();
            bool check = false;

            DataTable dtClient = new DataTable();
            dtClient = myClientData.getData("select * from [Client] where [accountNumber] ='" + accountNumber.Trim() + "' And [HKIDPassportNumber] ='"+ HKID.Trim() +"' And [firstName] = '"+ firstName.Trim() +"' And [lastName] ='"+ lastName.Trim() + "' And [email] = '"+ email.Trim() + "' And [dateOfBirth] ='" + dateOfBirth + "';");

            if (dtClient != null) check = true;

            return check;
        }

        protected void cvAccountNumber_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
        {
            //Retrieve AccountNumber Textbox value
            string accountNumber = AccountNumber.Text.Trim();
            char[] arr_accountNumber = new char[] { };
            //Retrieve LastName Textbox value
            string lastName = LastName.Text.Trim();
            char[] arr_lastName = new char[] { };
            //Decalre local boolean variable to initiate validation when string lenghts are legal.
            bool cvAccountNumber_Validator = false;
            //Check AccountNumber/LastName (validated by requiredfieldvalidator)
            if (accountNumber.Length < 10 || lastName.Length == 0)
            {
                args.IsValid = false;
                cvAccountNumber.ErrorMessage = "Please check the inputs in Account Number field and Last Name field.";
            }
            else
            {
                arr_accountNumber = accountNumber.ToUpper().ToCharArray(0, 10);
                int i = lastName.Length;
                arr_lastName = lastName.ToUpper().ToCharArray(0, i);
                cvAccountNumber_Validator = true;
            }

            //String comparison activated only when two strings have legal length
            if (cvAccountNumber_Validator)
            {
                int i = lastName.Length;
                char[] cvAccountNumber_Comparator = new char[2] { 'A', 'A' };


                if (i == 1)
                {
                    for (int h = 0; h < 2; h++)
                    {
                        cvAccountNumber_Comparator[h] = arr_lastName[0];
                    }
                }
                else
                {
                    for (int h = 0; h < 2; h++)
                    {
                        cvAccountNumber_Comparator[h] = arr_lastName[h];
                    }
                }

                if (cvAccountNumber_Comparator[0] == arr_accountNumber[0])
                {
                    if (cvAccountNumber_Comparator[1] == arr_accountNumber[1]) args.IsValid = true;
                    else
                    {
                        args.IsValid = false;
                        cvAccountNumber.ErrorMessage = "Account Number is Invalid. Please check the account number or the client's last name.";
                    }
                }
                else
                {
                    args.IsValid = false;
                    cvAccountNumber.ErrorMessage = "Account Number is invalid. Please check the account number or the client's last name.";
                }
            }
        }
    }
}