﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using HKeInvestWebApplication.Code_File;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using Microsoft.AspNet.Identity;

namespace HKeInvestWebApplication.Client
{
    public partial class CurrencyConverter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HKeInvestData myHKeInvestData = new HKeInvestData();
            HKeInvestCode myHKeInvestCode = new HKeInvestCode();
            ExternalFunctions myExternalFunctions = new ExternalFunctions();
            if (!Page.IsPostBack)
            {
                string userName = Context.User.Identity.GetUserName();
                string sql = "select [accountNumber] from [Account] where [userName] ='" + userName + "';";
                string accountNumber = "";

                DataTable dtTemp = myHKeInvestData.getData(sql);
                if (!(dtTemp == null))
                {
                    accountNumber = dtTemp.Rows[0]["accountNumber"].ToString().Trim();
                }

                lblClientAccountNumber.Text = "Account Number: " + accountNumber;
                lblClientAccountNumber.Visible = true;

                sql = "select [lastName], [firstName] from [Client] where ([accountNumber]='" + accountNumber + "')";

                DataTable dtClient = myHKeInvestData.getData(sql);
                string clientName = "Client(s): ";
                int i = 1;
                foreach (DataRow row in dtClient.Rows)
                {
                    clientName = clientName + row["lastName"] + ", " + row["firstName"];
                    if (dtClient.Rows.Count != i)
                    {
                        clientName = clientName + "and ";
                    }
                    i = i + 1;
                }
                lblClientName.Text = clientName;
                lblClientName.Visible = true;

                DataTable dtCurrency = myExternalFunctions.getCurrencyData();

                dlCurrency.DataSource = dtCurrency;
                dlCurrency.DataBind();
                foreach (DataRow row in dtCurrency.Rows)
                {
                    string currencyItem = row["currency"].ToString().Trim();

                    if (!(currencyItem == "HKD"))
                    {
                        ddlBaseCurrency.Items.Add(row["currency"].ToString().Trim());
                        ddlTargetCurrency.Items.Add(row["currency"].ToString().Trim());
                    }
                    
                }

                ViewState["dtCurrencyData"] = dtCurrency;
            }

        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {

            Page.Validate();
            if (Page.IsValid == true)
            {
                string baseCurrency = ddlBaseCurrency.SelectedValue;
                string targetCurrency = ddlTargetCurrency.SelectedValue;
                decimal amount = Math.Round(decimal.Parse(txtAmount.Text.Trim()),2);

                decimal convertedAmount;
                if (baseCurrency == targetCurrency)
                {
                    convertedAmount = amount;
                    lblError.Text = "Base and Target Currency Identical";
                    lblError.Visible = true;
                    lblStatus.Visible = false;
                }
                else
                {
                    convertedAmount = CurrencyConvert(baseCurrency, amount, targetCurrency);
                    lblStatus.Text = "Converting " + baseCurrency + " to " + targetCurrency + ".";
                    lblStatus.Visible = true;
                    lblError.Visible = false;

                }
                lblResultBase.Text = amount.ToString() + " " + baseCurrency;
                lblResultTarget.Text = convertedAmount.ToString() + " " + targetCurrency;
                pnResult.Visible = true;
                tblResult.Visible = true;
            }
            else
            {
                pnResult.Visible = false;
                tblResult.Visible = false;
            }
        }

        private decimal CurrencyConvert (string baseCurrency, decimal amount, string targetCurrency)
        {
            DataTable dtCurrencyData = (DataTable)ViewState["dtCurrencyData"];
            decimal baseCurrencyRate = 0;
            decimal targetCurrencyRate = 0;
            foreach (DataRow baseRow in dtCurrencyData.Select("currency = '" + baseCurrency + "'"))
            {
                baseCurrencyRate = baseCurrencyRate + (decimal)(baseRow["rate"]);
            }
            foreach (DataRow targetRow in dtCurrencyData.Select("currency = '" + targetCurrency + "'"))
            {
                targetCurrencyRate = targetCurrencyRate + (decimal)(targetRow["rate"]);
            }
            decimal convertedValueTemp = (decimal)(amount * baseCurrencyRate / targetCurrencyRate);
            decimal convertedValue = Math.Round(convertedValueTemp, 2);
            return convertedValue;
        }
    }
}