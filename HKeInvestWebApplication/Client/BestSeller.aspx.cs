﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using HKeInvestWebApplication.Code_File;
using HKeInvestWebApplication.ExternalSystems.Code_File;


namespace HKeInvestWebApplication.Client
{
    public partial class BestSeller : System.Web.UI.Page
    {
        HKeInvestData myHKeInvestData = new HKeInvestData();
        HKeInvestCode myHKeInvestCode = new HKeInvestCode();
        HKeInvestService myHKeInvestService = new HKeInvestService();
        ExternalFunctions myExternalFunctions = new ExternalFunctions();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string currentYear = DateTime.Now.Year.ToString("0000").Trim();
                string currentMonth = DateTime.Now.Month.ToString("00").Trim();
                string currentDay = DateTime.Now.Day.ToString("00").Trim();

                pnResultHeading.Text = "Best Sellers of " + currentYear + " / " + currentMonth;
                lblDate.Text = "Today's Date: " + currentYear + " / " + currentMonth + " / " + currentDay;
                DataTable dtBestSeller = myHKeInvestService.queryBestSeller(currentMonth, currentYear);

                if (dtBestSeller == null)
                {
                    lblError.Text = "Error Occurred while processing the data. Please contact system administrator.";
                    lblError.Visible = true;
                    lblStatus.Visible = false;
                    gvBestSeller.Visible = false;

                }
                else
                {
                    lblStatus.Text = "Showing Best Sellers for " + currentYear + " / " + currentMonth; 
                    foreach (DataRow row in dtBestSeller.Rows)
                    {
                        calculateHKD(row);
                    }
                    dtBestSeller.AcceptChanges();
                    gvBestSeller.DataSource = dtBestSeller;
                    gvBestSeller.DataBind();
                    gvBestSeller.Visible = true;
                    lblStatus.Visible = true;
                }
            }
        }
        private void calculateHKD (DataRow drSecurity)
        {
            if (drSecurity == null) return;
            else
            {
                string type = drSecurity["type"].ToString().Trim();
                if (type == "stock") return;
                else
                {
                    string code = drSecurity["code"].ToString().Trim();
                    decimal highPrice = Decimal.Parse(drSecurity["highPrice"].ToString().Trim());
                    decimal lowPrice = Decimal.Parse(drSecurity["lowPrice"].ToString().Trim());
                    decimal volume = Decimal.Parse(drSecurity["volume"].ToString().Trim());

                    decimal rate = myHKeInvestService.getCurrency(type, code);
                    drSecurity["highPrice"] = Math.Round(highPrice * rate, 2);
                    drSecurity["lowPrice"] = Math.Round(lowPrice * rate, 2);
                    drSecurity["volume"] = Math.Round(volume * rate, 2);
                }
            }
        }

        

        protected void gvBestSeller_Sorting(object sender, GridViewSortEventArgs e)
        {
            // Since a GridView cannot be sorted directly, it is first loaded into a DataTable using the helper method 'unloadGridView'.
            // Create a DataTable from the GridView.
            DataTable dtSecurityHolding = myHKeInvestCode.unloadGridView(gvBestSeller);

            // Set the sort expression in ViewState for correct toggling of sort direction,
            // Sort the DataTable and bind it to the GridView.
            string sortExpression = e.SortExpression.ToLower();
            ViewState["SortExpression"] = sortExpression;
            dtSecurityHolding.DefaultView.Sort = sortExpression + " " + myHKeInvestCode.getSortDirection(ViewState, e.SortExpression);
            dtSecurityHolding.AcceptChanges();

            // Bind the DataTable to the GridView.
            gvBestSeller.DataSource = dtSecurityHolding.DefaultView;
            gvBestSeller.DataBind();
        }
    }
}