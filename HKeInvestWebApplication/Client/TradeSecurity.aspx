﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TradeSecurity.aspx.cs" Inherits="HKeInvestWebApplication.Client.TradeSecurity" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-header">
        <h2>HKeInvest: Securities Buy and Sell</h2>
        <asp:Label runat="server" ID="lblClientAccountNumber" CssClass="control-label label label-success" Font-Size="Small"></asp:Label>
        <asp:Label runat="server" ID="lblClientName" Visible="false" CssClass="label label-info" Font-Size="Small"></asp:Label>
    </div>
    <%--Largest Div Element--%>
    <div class ="form-horizontal panel-group">
        <asp:Panel runat="server" ID="pnResult" CssClass="panel panel-default" Visible="false">
            <asp:Label runat="server" ID="lblOrderNumber" CssClass="label label-success" Visible="false" Font-Size="Small"></asp:Label>
            <asp:Label runat="server" ID="lblOrderError" CssClass="label label-danger" Visible="false" Font-Size="Small"></asp:Label>
        </asp:Panel>
        <%--SecurityType ddlist and BuyOrSell ddlist div--%>
        <asp:Panel runat="server" ID="pnDdlists" CssClass="panel panel-default">
            <div class="form-group panel-body">
                <asp:Label runat="server" AssociatedControlID="ddlSecurityType" Text="Security Type" CssClass="control-label col-md-2"></asp:Label>
                <%--Security Type ddlist--%>
                <div class="col-md-3">
                    <asp:DropDownList runat="server" ID="ddlSecurityType" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlSecurityType_SelectedIndexChanged">
                        <asp:ListItem Value="0">-- Select Security Type --</asp:ListItem>
                        <asp:ListItem Value="bond">Bond</asp:ListItem>
                        <asp:ListItem Value="unit trust">Unit Trust</asp:ListItem>
                        <asp:ListItem Value="stock">Stock</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <asp:Label runat="server" AssociatedControlID="ddlBuyOrSell" Text="Buy or Sell" CssClass="control-label col-md-2"></asp:Label>
                <%--Buy or Sell ddlist--%>
                <div class="col-md-3">
                    <asp:DropDownList runat="server" ID="ddlBuyOrSell" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlBuyOrSell_SelectedIndexChanged">
                        <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                        <asp:ListItem Value="buy">Buy</asp:ListItem>
                        <asp:ListItem Value="sell">Sell</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2"><asp:Button runat="server" ID="btnSubmit" CssClass="btn btn-primary" Visible="false" OnClick="btnSubmit_Click"/></div>
            </div>
        </asp:Panel>
        <%--Bond/Unit Trust Buy Panel--%>
        <asp:Panel runat="server" ID="pnBondUnitTrust" Enabled="false" Visible="false" CssClass="panel panel-default">
            <div class="panel-heading"><asp:Label runat="server" ID="lblBondUnitTrustHeading"></asp:Label></div>
            <div class="form-horizontal panel-body">
                <div class="form-group col-md-12">
                    <asp:Label runat="server" ID="lblBondUnitTrustCode" AssociatedControlID="txtBondUnitTrustCode" CssClass="control-label col-md-4"></asp:Label>
                    <div class="col-md-4">
                        <asp:TextBox runat="server" ID="txtBondUnitTrustCode" CssClass="form-control"></asp:TextBox>
                    </div>
                    <asp:RequiredFieldValidator runat="server" ID="rfvBondUnitCode" ControlToValidate="txtBondUnitTrustCode" CssClass="label label-danger col-md-2" ErrorMessage="Security Code is Required." Enabled="false"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="revBondUnitCode" ControlToValidate="txtBondUnitTrustCode" CssClass="label label-danger col-md-2" ErrorMessage="Numeric Value Only" Enabled="false" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                </div>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" ID="lblBondUnitAmount" AssociatedControlID="txtBondUnitAmount" CssClass="control-label col-md-4"></asp:Label>
                    <div class="col-md-4">
                        <asp:TextBox runat="server" ID="txtBondUnitAmount" CssClass="form-control"></asp:TextBox>
                    </div>
                    <asp:RequiredFieldValidator runat="server" ID="rfvBondUnitAmount" ControlToValidate="txtBondUnitAmount" CssClass="label label-danger col-md-2" ErrorMessage="Please Specify Amount" Enabled="false"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="revBondUnitAmount" ControlToValidate="txtBondUnitAmount" CssClass="label label-danger col-md-2" ErrorMessage="Decimal Value Only" Enabled="false" ValidationExpression="(?:\d*\.)?\d+"></asp:RegularExpressionValidator>
                </div>
            </div>
        </asp:Panel>
        <%--Stock Trade Panel--%>
        <asp:Panel runat="server" ID="pnStock" Enabled="false" Visible="false" CssClass="panel panel-default">
            <div class="panel-heading"><asp:Label runat="server" ID="lblStockHeading"></asp:Label></div>
            <div class="form-horizontal panel-body">
                <div class="form-group col-md-12">
                    <asp:Label runat="server" AssociatedControlID="txtStockCode" Text="Stock Code: " CssClass="control-label col-md-4"></asp:Label>
                    <div class="col-md-4">
                        <asp:TextBox runat="server" ID="txtStockCode" CssClass="form-control"></asp:TextBox>
                    </div>
                    <asp:RequiredFieldValidator runat="server" ID="rfvStockCode" ControlToValidate="txtStockCode" CssClass="label label-danger col-md-2" ErrorMessage="Security Code is Required." Enabled="false"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="revStockCode" ControlToValidate="txtStockCode" CssClass="label label-danger col-md-2" ErrorMessage="Numeric Value Only" Enabled="false" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                </div>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" AssociatedControlID="ddlStockOrderType" Text="Order Type: " CssClass="control-label col-md-4"></asp:Label>
                    <div class="col-md-4">
                        <asp:DropDownList runat="server" ID="ddlStockOrderType" CssClass="form-control">
                            <asp:ListItem Value="0">-- Select Order Type --</asp:ListItem>
                            <asp:ListItem Value="market">Market Order</asp:ListItem>
                            <asp:ListItem Value="limit">Limit Order</asp:ListItem>
                            <asp:ListItem Value="stop">Stop Order</asp:ListItem>
                            <asp:ListItem Value="stop limit">Stop Limit Order</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator runat="server" ID="rfvStockOrderType" InitialValue="0" ControlToValidate="ddlStockOrderType" CssClass="label label-danger col-md-2" ErrorMessage="Please Specify Order Type." Enabled="false"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" ID="lblStockShare" AssociatedControlID="txtStockShare" CssClass="control-label col-md-4"></asp:Label>
                    <div class="col-md-4">
                        <asp:TextBox runat="server" ID="txtStockShare" CssClass="form-control"></asp:TextBox>
                    </div>
                    <asp:RequiredFieldValidator runat="server" ID="rfvStockShare" ControlToValidate="txtStockShare" CssClass="label label-danger col-md-2" ErrorMessage="Please Specify Amount." Enabled="false"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="revStockShare" ControlToValidate="txtStockShare" CssClass="label label-danger col-md-2" ErrorMessage="Numeric Value Only" Enabled="false" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                </div>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" AssociatedControlID="ddlStockExpiryDate" Text="Expiry Date (Up to 7 days): " CssClass="control-label col-md-4"></asp:Label>
                    <div class="col-md-4">
                        <asp:DropDownList runat="server" ID="ddlStockExpiryDate" CssClass="form-control" AutoPostBack="false">
                            <asp:ListItem Value="0">-- Select Expiry Date --</asp:ListItem>
                            <asp:ListItem Value="1">1 day</asp:ListItem>
                            <asp:ListItem Value="2">2 days</asp:ListItem>
                            <asp:ListItem Value="3">3 days</asp:ListItem>
                            <asp:ListItem Value="4">4 days</asp:ListItem>
                            <asp:ListItem Value="5">5 days</asp:ListItem>
                            <asp:ListItem Value="6">6 days</asp:ListItem>
                            <asp:ListItem Value="7">1 week</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator runat="server" ID="rfvStockExpiryDate" InitialValue="0" ControlToValidate="ddlStockExpiryDate" CssClass="label label-danger col-md-2" ErrorMessage="Please Specify Expiry Date." Enabled="false"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" AssociatedControlID="ddlStockAllOrNone" Text="All or None: " CssClass="control-label col-md-4"></asp:Label>
                    <div class="col-md-4">
                        <asp:DropDownList runat="server" ID="ddlStockAllOrNone" AutoPostBack="true" CssClass="form-control">
                            <asp:ListItem Value="0">-- Select Order Type --</asp:ListItem>
                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                            <asp:ListItem Value="N">No</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator runat="server" ID="rfvStockAllOrNone" InitialValue="0" ControlToValidate="ddlStockAllOrNone" CssClass="label label-danger col-md-2" ErrorMessage="Please Specify All or None." Enabled="false"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" AssociatedControlID="txtStockHigh" Text="Set High Price: " CssClass="control-label col-md-4"></asp:Label>
                    <div class="col-md-4">
                        <asp:TextBox runat="server" ID="txtStockHigh" CssClass="form-control"></asp:TextBox>
                    </div>
                    <asp:RegularExpressionValidator runat="server" ID="revStockHigh" ControlToValidate="txtStockHigh" CssClass="label label-danger col-md-2" ErrorMessage="Decimal Value Only" Enabled="false" ValidationExpression="(?:\d*\.)?\d+"></asp:RegularExpressionValidator>
                </div>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" AssociatedControlID="txtStockStop" Text="Set Stop Price: " CssClass="control-label col-md-4"></asp:Label>
                    <div class="col-md-4">
                        <asp:TextBox runat="server" ID="txtStockStop" CssClass="form-control"></asp:TextBox>
                    </div>
                    <asp:RegularExpressionValidator runat="server" ID="revStockStop" ControlToValidate="txtStockStop" CssClass="label label-danger col-md-2" ErrorMessage="Decimal Value Only" Enabled="false" ValidationExpression="(?:\d*\.)?\d+"></asp:RegularExpressionValidator>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>