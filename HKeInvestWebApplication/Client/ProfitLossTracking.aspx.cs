﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using HKeInvestWebApplication.Code_File;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using Microsoft.AspNet.Identity;

namespace HKeInvestWebApplication.Client
{
    public partial class ProfitLossTracking : System.Web.UI.Page
    {
        HKeInvestData myHKeInvestData = new HKeInvestData();
        HKeInvestCode myHKeInvestCode = new HKeInvestCode();
        HKeInvestService myHKeInvestService = new HKeInvestService();
        ExternalFunctions myExternalFunctions = new ExternalFunctions();

        protected void Page_Load(object sender, EventArgs e)
        {
            /* Set Global Variables for functions to use */
            string userNameTemp = Context.User.Identity.GetUserName();
            // DataTable accountNumberTemp = myHKeInvestData.getData("select [accountNumber] from [Account] where [userName] ='" + userNameTemp + "';");
            DataTable accountNumberTemp = myHKeInvestData.getData("select [accountNumber] from [Account]");
            string accountNumber = accountNumberTemp.Rows[0]["accountNumber"].ToString().Trim();
            
            if (Convert.ToBoolean(myHKeInvestData.getAggregateValue("select count(*) from [Invoice] where [accountNumber] = '" + accountNumber + "' AND [status] = 'completed';")))
            {
                getIndividualSecurity(accountNumber);
                getTypeSecurityResult(accountNumber);
            }
        }

        protected void getIndividualSecurity(string accountNumber)
        {
            // Get Data from Invoice DataTable
            DataTable dtIndividualSecurity = myHKeInvestData.getData("select [type], [code], [name], [shares], 0.00 AS [totalbuyamount], 0.00 AS [totalsellamount], 0.00 AS [totalfeespaid], 0.00 AS [totalpl] from [SecurityHolding] where [accountNumber] ='" + accountNumber + "';");
            DataTable dtIndividualSecurityFromInvoice = myHKeInvestData.getData("select [type], [code], [name], [executeDate], [executeShares], [executePrice], [referenceNumber] from [Invoice] where [accountNumber] ='" + accountNumber + "' and [status] = 'completed';");

            // Define DataTable and Assign Columns
            DataTable dtIndividualResult = new DataTable();
            dtIndividualResult.Clear();
            dtIndividualResult.Columns.Add("type");
            dtIndividualResult.Columns.Add("code");
            dtIndividualResult.Columns.Add("name");
            dtIndividualResult.Columns.Add("shares");
            dtIndividualResult.Columns.Add("totalbuyamount");
            dtIndividualResult.Columns.Add("totalsellamount");
            dtIndividualResult.Columns.Add("totalfeespaid");
            dtIndividualResult.Columns.Add("totalpl");

            // Define Variables Needed
            foreach (DataRow row in dtIndividualSecurityFromInvoice.Rows)
            {
                string securityType = row["type"].ToString().Trim();
                string securityCode = row["code"].ToString();

                decimal currencyMultiplier = myHKeInvestService.getCurrency(securityType, securityCode);

                decimal shares = myHKeInvestData.getAggregateValue("select sum(shares) from [SecurityHolding] where [accountNumber] = '" + accountNumber + "' AND [type] = '" + row["type"].ToString().Trim() + "' AND [code] = '" + row["code"].ToString() + "'");
                decimal individualBuyTotalValue = currencyMultiplier * myHKeInvestData.getAggregateValue("select sum(executeShares*executePrice) from [Invoice] where [accountNumber] ='" + accountNumber + "' AND [buyOrSell] = 'buy' AND [type] = '" + row["type"].ToString().Trim() + "' AND [code] = '" + row["code"].ToString().Trim() + "'");
                decimal individualSellTotalValue = currencyMultiplier * myHKeInvestData.getAggregateValue("select sum(executeShares*executePrice) from [Invoice] where [accountNumber] ='" + accountNumber + "' AND [buyOrSell] = 'sell' AND [type] = '" + row["type"].ToString().Trim() + "'AND [code] = '" + row["code"].ToString() + "'");
                decimal individualTotalFeesPaid = myHKeInvestData.getAggregateValue("select sum(serviceFee) from [Invoice] where [accountNumber] = '" + accountNumber + "' AND [type] = '" + row["type"].ToString().Trim() + "'AND [code] = '" + row["code"].ToString() + "'");

                // Subtract Duplicate Service Fee Charges
                decimal duplicateServiceFeeCount = myHKeInvestData.getAggregateValue("select count(*) from [Invoice] where [referenceNumber] = '" + row["referenceNumber"].ToString() + "';");
                if (duplicateServiceFeeCount != 0)
                {
                    individualTotalFeesPaid = individualTotalFeesPaid / duplicateServiceFeeCount;
                }

                // Get the Current Total Security Value of each Individual Security

                decimal price = currencyMultiplier * myExternalFunctions.getSecuritiesPrice(securityType, securityCode);
                decimal currentShareValue = shares * price;
                decimal totalplperc = 0;

                decimal totalpl = (currentShareValue + individualSellTotalValue) - (individualBuyTotalValue + individualTotalFeesPaid);
                if (individualBuyTotalValue != 0) {
                    totalplperc = (totalpl / individualBuyTotalValue) * 100;
                }


                shares = Math.Round(shares, 2, MidpointRounding.AwayFromZero);
                price = Math.Round(price, 2, MidpointRounding.AwayFromZero);
                totalpl = Math.Round(totalpl, 2, MidpointRounding.AwayFromZero);
                totalplperc = Math.Round(totalplperc, 2, MidpointRounding.AwayFromZero);
                individualTotalFeesPaid = Math.Round(individualTotalFeesPaid, 2, MidpointRounding.AwayFromZero);
                individualBuyTotalValue = Math.Round(individualBuyTotalValue, 2, MidpointRounding.AwayFromZero);
                individualSellTotalValue = Math.Round(individualSellTotalValue, 2, MidpointRounding.AwayFromZero);


                addRowInDT(dtIndividualResult, row["type"].ToString().Trim(), row["code"].ToString(), row["name"].ToString(), shares, individualBuyTotalValue, individualSellTotalValue, individualTotalFeesPaid, totalpl, totalplperc);
            }

            // Remove Duplicate Results
            DataTable distinctTable = dtIndividualResult.DefaultView.ToTable( /*distinct*/ true);

            // Bind DataTable to GridView
            gvIndividualPL.DataSource = distinctTable;
            gvIndividualPL.DataBind();
            gvIndividualPL.Visible = true;
        }

        protected void getTypeSecurityResult(string accountNumber)
        {
            DataTable dtTypeSecurity = myHKeInvestData.getData("select [type], 0.00 AS [typetotalbuyamount], 0.00 AS [typetotalsellamount], 0.00 AS [typetotalfeespaid], 0.00 AS [typetotalpl] from [SecurityHolding] where [accountNumber] ='" + accountNumber + "';");
            DataTable dtTypeSecurityFromInvoice = myHKeInvestData.getData("select * from [Invoice] where [accountNumber] = '" + accountNumber + "' and [status]='completed';");


            // Declare Variables needed
            decimal stockBuyTotal = 0;
            decimal bondBuyTotal = 0;
            decimal unitTrustBuyTotal = 0;
            decimal stockSellTotal = 0;
            decimal bondSellTotal = 0;
            decimal unitTrustSellTotal = 0;
            decimal stockTotalFee = 0;
            decimal bondTotalFee = 0;
            decimal unitTrustTotalFee = 0;
            decimal individualBuyFeesPaid = 0;
            decimal individualSellFeesPaid = 0;
            decimal individualTotalFeesPaid = 0;

            decimal stockPL = 0;
            decimal bondPL = 0;
            decimal unitTrustPL = 0;

            decimal stockPLperc = 0;
            decimal bondPLperc = 0;
            decimal unitTrustPLperc = 0;

            decimal currentStockValue = 0;
            decimal currentBondValue = 0;
            decimal currentUnitTrustValue = 0;

            // Define Variables from Invoice DataTable
            foreach (DataRow row in dtTypeSecurityFromInvoice.Rows)
            {
                if (row["status"].ToString().Trim() == "pending") continue;
                string securityType = row["type"].ToString().Trim();
                string securityCode = row["code"].ToString();
                decimal currencyMultiplier = myHKeInvestService.getCurrency(securityType, securityCode);
                decimal shares = Convert.ToDecimal(row["executeshares"]);
                decimal price = currencyMultiplier * Convert.ToDecimal(row["executePrice"]);

                /*
                individualBuyFeesPaid = myHKeInvestData.getAggregateValue("select sum(serviceFee) from [Invoice] where [accountNumber] = '" + accountNumber + "' AND [type] = '" + row["type"].ToString().Trim() + "'AND [code] = '" + row["code"].ToString() + "' AND [buyOrSell] = 'buy';");
                individualSellFeesPaid = myHKeInvestData.getAggregateValue("select sum(serviceFee) from [Invoice] where [accountNumber] = '" + accountNumber + "' AND [type] = '" + row["type"].ToString().Trim() + "'AND [code] = '" + row["code"].ToString() + "' AND [buyOrSell] = 'sell';");
    */

                // Subtract Duplicate Service Fee Charges
                individualTotalFeesPaid = myHKeInvestData.getAggregateValue("select sum(serviceFee) from [Invoice] where [referenceNumber] = '" + row["referenceNumber"].ToString() + "';");

                decimal duplicateServiceFeeCount = myHKeInvestData.getAggregateValue("select count(*) from [Invoice] where [referenceNumber] = '" + row["referenceNumber"].ToString() + "';");

                if (duplicateServiceFeeCount != 0)
                {
                    individualTotalFeesPaid = (individualTotalFeesPaid / duplicateServiceFeeCount) / duplicateServiceFeeCount;
                }

                if (row["type"].ToString().Trim() == "stock" && row["buyOrSell"].ToString().Trim() == "buy")
                {
                    stockBuyTotal += price * shares;
                    stockTotalFee += individualTotalFeesPaid;
                }
                else if (row["type"].ToString().Trim() == "stock" && row["buyOrSell"].ToString().Trim() == "sell")
                {
                    stockSellTotal += price * shares;
                    stockTotalFee += individualTotalFeesPaid;
                }

                else if (row["type"].ToString().Trim() == "bond" && row["buyOrSell"].ToString().Trim() == "buy")
                {
                    bondBuyTotal += price * shares;
                    bondTotalFee += individualTotalFeesPaid;
                }
                else if (row["type"].ToString().Trim() == "bond" && row["buyOrSell"].ToString().Trim() == "sell")
                {
                    bondSellTotal += price * shares;
                    bondTotalFee += individualTotalFeesPaid;
                }
                else if (row["type"].ToString().Trim() == "unit trust" && row["buyOrSell"].ToString().Trim() == "buy")
                {
                    unitTrustBuyTotal += price * shares;
                    unitTrustTotalFee += individualTotalFeesPaid;
                }
                else if (row["type"].ToString().Trim() == "unit trust" && row["buyOrSell"].ToString().Trim() == "sell")
                {
                    unitTrustSellTotal += price * shares;
                    unitTrustTotalFee += individualTotalFeesPaid;
                }
                else
                {

                }
            }

            // Get the Current Total Security Value of each Data Type
            DataTable dtCurrentSecurities = myHKeInvestData.getData("select [type], [code], [name], [shares], [base] from [SecurityHolding] where [accountNumber] ='" + accountNumber + "';");

            foreach (DataRow row in dtCurrentSecurities.Rows)
            {
                string securityType = row["type"].ToString().Trim();
                string securityCode = row["code"].ToString();
                decimal currencyMultiplier = myHKeInvestService.getCurrency(securityType, securityCode);

                decimal shares = Convert.ToDecimal(row["shares"]);
                decimal price = currencyMultiplier * myExternalFunctions.getSecuritiesPrice(securityType, securityCode);
                decimal value = Math.Round(shares * price - (decimal).005, 2);

                if (securityType == "stock")
                {
                    currentStockValue += value;
                }
                else if (securityType == "bond")
                {
                    currentBondValue += value;
                }
                else if (securityType == "unit trust")
                {
                    currentUnitTrustValue += value;
                }
                else
                {

                }
            }

            // Define Profit/Loss and Percentage Profit/Loss of each Security Type
            stockPL = (currentStockValue + stockSellTotal) - (stockBuyTotal + stockTotalFee);
            bondPL = (currentBondValue + bondSellTotal) - (bondBuyTotal + bondTotalFee);
            unitTrustPL = (currentUnitTrustValue + unitTrustSellTotal) - (unitTrustBuyTotal + unitTrustTotalFee);

            if (stockBuyTotal != 0)
            {
                stockPLperc = (stockPL / stockBuyTotal) * 100;
                stockPLperc = Math.Round(stockPLperc, 2, MidpointRounding.AwayFromZero);
            }
            if (bondBuyTotal != 0)
            {
                bondPLperc = (bondPL / bondBuyTotal) * 100;
                bondPLperc = Math.Round(bondPLperc, 2, MidpointRounding.AwayFromZero);
            }
            if (unitTrustBuyTotal != 0)
            {
                unitTrustPLperc = (unitTrustPL / unitTrustBuyTotal) * 100;
                unitTrustPLperc = Math.Round(unitTrustPLperc, 2, MidpointRounding.AwayFromZero);
            }


            // Create DataTable and Define Columns
            DataTable dtTypeResult = new DataTable();
            dtTypeResult.Clear();
            dtTypeResult.Columns.Add("type");
            dtTypeResult.Columns.Add("typetotalbuyamount");
            dtTypeResult.Columns.Add("typetotalsellamount");
            dtTypeResult.Columns.Add("typetotalfeespaid");
            dtTypeResult.Columns.Add("typetotalpl");


            // Round All Variables to 2 decimal points
            stockBuyTotal = Math.Round(stockBuyTotal, 2, MidpointRounding.AwayFromZero);
            stockSellTotal = Math.Round(stockSellTotal, 2, MidpointRounding.AwayFromZero);
            stockTotalFee = Math.Round(stockTotalFee, 2, MidpointRounding.AwayFromZero);
            bondBuyTotal = Math.Round(bondBuyTotal, 2, MidpointRounding.AwayFromZero);
            bondSellTotal = Math.Round(bondSellTotal, 2, MidpointRounding.AwayFromZero);
            bondTotalFee = Math.Round(bondTotalFee, 2, MidpointRounding.AwayFromZero);
            unitTrustBuyTotal = Math.Round(unitTrustBuyTotal, 2, MidpointRounding.AwayFromZero);
            unitTrustSellTotal = Math.Round(unitTrustSellTotal, 2, MidpointRounding.AwayFromZero);
            unitTrustTotalFee = Math.Round(unitTrustTotalFee, 2, MidpointRounding.AwayFromZero);
            stockPL = Math.Round(stockPL, 2, MidpointRounding.AwayFromZero);
            bondPL = Math.Round(bondPL, 2, MidpointRounding.AwayFromZero);
            unitTrustPL = Math.Round(unitTrustPL, 2, MidpointRounding.AwayFromZero);
            stockPLperc = Math.Round(stockPLperc, 2, MidpointRounding.AwayFromZero);
            bondPLperc = Math.Round(bondPLperc, 2, MidpointRounding.AwayFromZero);
            unitTrustPLperc = Math.Round(unitTrustPLperc, 2, MidpointRounding.AwayFromZero);

            // Create DataRow for each Type and assign values
            DataRow stockRow = dtTypeResult.NewRow();
            stockRow["type"] = "Stock";
            stockRow["typetotalbuyamount"] = stockBuyTotal;
            stockRow["typetotalsellamount"] = stockSellTotal;
            stockRow["typetotalfeespaid"] = stockTotalFee;
            stockRow["typetotalpl"] = stockPL + " (" + stockPLperc + "%)";
            dtTypeResult.Rows.Add(stockRow);

            DataRow bondRow = dtTypeResult.NewRow();
            bondRow["type"] = "Bond";
            bondRow["typetotalbuyamount"] = bondBuyTotal;
            bondRow["typetotalsellamount"] = bondSellTotal;
            bondRow["typetotalfeespaid"] = bondTotalFee;
            bondRow["typetotalpl"] = bondPL + " (" + bondPLperc + "%)";
            dtTypeResult.Rows.Add(bondRow);

            DataRow unitTrustRow = dtTypeResult.NewRow();
            unitTrustRow["type"] = "Unit Trust";
            unitTrustRow["typetotalbuyamount"] = unitTrustBuyTotal;
            unitTrustRow["typetotalsellamount"] = unitTrustSellTotal;
            unitTrustRow["typetotalfeespaid"] = unitTrustTotalFee;
            unitTrustRow["typetotalpl"] = unitTrustPL + " (" + unitTrustPLperc + "%)";
            dtTypeResult.Rows.Add(unitTrustRow);
            

            // Bind the Data to the GridView
            gvTypePL.DataSource = dtTypeResult;
            gvTypePL.DataBind();
            gvTypePL.Visible = true;

            getAllSecurityResult(accountNumber, stockBuyTotal, bondBuyTotal, unitTrustBuyTotal, stockSellTotal, bondSellTotal, unitTrustSellTotal, stockTotalFee, bondTotalFee, unitTrustTotalFee, stockPL, bondPL, unitTrustPL, stockPLperc, bondPLperc, unitTrustPLperc);
            }

        protected void getAllSecurityResult(string accountNumber, decimal stockBuyTotal, decimal bondBuyTotal, decimal unitTrustBuyTotal, decimal stockSellTotal, decimal bondSellTotal, decimal unitTrustSellTotal, decimal stockTotalFee, decimal bondTotalFee, decimal unitTrustTotalFee, decimal stockPL, decimal bondPL, decimal unitTrustPL, decimal stockPLperc, decimal bondPLperc, decimal unitTrustPLperc)
        {

            decimal totalPL = stockPL + bondPL + unitTrustPL;
            decimal totalPLperc = 0;
            if (stockBuyTotal + bondBuyTotal + unitTrustBuyTotal != 0)
            {
                totalPLperc = (totalPL / (stockBuyTotal + bondBuyTotal + unitTrustBuyTotal)) * 100;
            }

            DataTable dtTypeResult = new DataTable();
            dtTypeResult.Clear();
            dtTypeResult.Columns.Add("type");
            dtTypeResult.Columns.Add("typetotalbuyamount");
            dtTypeResult.Columns.Add("typetotalsellamount");
            dtTypeResult.Columns.Add("typetotalfeespaid");
            dtTypeResult.Columns.Add("typetotalpl");

            addRowInTypeDT(dtTypeResult, "Stock", stockBuyTotal, stockSellTotal, stockTotalFee, stockPL, stockPLperc);
            addRowInTypeDT(dtTypeResult, "Bond", bondBuyTotal, bondSellTotal, bondTotalFee, bondPL, bondPLperc);
            addRowInTypeDT(dtTypeResult, "Unit Trust", unitTrustBuyTotal, unitTrustSellTotal, unitTrustTotalFee, unitTrustPL, unitTrustPLperc);
            
            gvTypePL.DataSource = dtTypeResult;
            gvTypePL.DataBind();
            gvTypePL.Visible = true;

            DataTable dtTotalResult = new DataTable();
            dtTotalResult.Clear();
            dtTotalResult.Columns.Add("totalbuyamount");
            dtTotalResult.Columns.Add("totalsellamount");
            dtTotalResult.Columns.Add("totalfeespaid");
            dtTotalResult.Columns.Add("totalpl");
            dtTotalResult.Columns.Add("currentbalance");

            totalPLperc = Math.Round(totalPLperc, 2, MidpointRounding.AwayFromZero);

            decimal currentBalance = myHKeInvestData.getAggregateValue("select sum(balance) From [Account] where [accountNumber] ='" + accountNumber + "';");

            DataRow totalRow = dtTotalResult.NewRow();
            totalRow["totalbuyamount"] = stockBuyTotal + bondBuyTotal + unitTrustBuyTotal;
            totalRow["totalsellamount"] = stockSellTotal + bondSellTotal + unitTrustSellTotal;
            totalRow["totalfeespaid"] = stockTotalFee + bondTotalFee + unitTrustTotalFee;
            totalRow["totalpl"] = totalPL + " (" + totalPLperc + "%)";
            totalRow["currentbalance"] = currentBalance;

            dtTotalResult.Rows.Add(totalRow);

            // DataTable dtAllSecurity = myHKeInvestData.getData("select 0.00 AS [totalbuyamount], 0.00 AS [totalsellamount], 0.00 AS [totalfeespaid], 0.00 AS [totalpl] from [SecurityHolding] where [accountNumber] ='" + accountNumber + "';");
            gvTotalPL.DataSource = dtTotalResult;
            gvTotalPL.DataBind();
            gvTotalPL.Visible = true;
        }

        protected void addRowInDT(DataTable dt, string type, string code, string name, decimal shares, decimal totalbuyamount, decimal totalsellamount, decimal totalfeespaid, decimal totalpl, decimal totalplperc)
        {
            DataRow individualRow = dt.NewRow();
            individualRow["type"] = type;
            individualRow["code"] = code;
            individualRow["name"] = name;
            individualRow["shares"] = shares;
            individualRow["totalbuyamount"] = totalbuyamount;
            individualRow["totalsellamount"] = totalsellamount;
            individualRow["totalfeespaid"] = totalfeespaid;
            individualRow["totalpl"] = totalpl + " (" + totalplperc + " %)";

            dt.Rows.Add(individualRow);
        }

        protected void addRowInTypeDT(DataTable dt, string type, decimal totalbuyamount, decimal totalsellamount, decimal totalfeespaid, decimal totalpl, decimal totalplperc)
        {
            DataRow individualRow = dt.NewRow();
            individualRow["type"] = type;
            individualRow["typetotalbuyamount"] = totalbuyamount;
            individualRow["typetotalsellamount"] = totalsellamount;
            individualRow["typetotalfeespaid"] = totalfeespaid;
            individualRow["typetotalpl"] = totalpl + " (" + totalplperc + " %)";

            dt.Rows.Add(individualRow);
        }


        protected void gvTotalPL_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void gvIndividualPL_Sorting(object sender, GridViewSortEventArgs e)
        {

        }
    }
}

/*
UNUSED

    

    Orignally in dtTypeResult Sector
            DataRow stockRow = dtTypeResult.NewRow();
            stockRow["type"] = "Stock";
            stockRow["typetotalbuyamount"] = stockBuyTotal;
            stockRow["typetotalsellamount"] = stockSellTotal;
            stockRow["typetotalfeespaid"] = stockTotalFee;
            stockRow["typetotalpl"] = stockPL + " (" + stockPLperc + "%)";
            dtTypeResult.Rows.Add(stockRow);

            DataRow bondRow = dtTypeResult.NewRow();
            bondRow["type"] = "Bond";
            bondRow["typetotalbuyamount"] = bondBuyTotal;
            bondRow["typetotalsellamount"] = bondSellTotal;
            bondRow["typetotalfeespaid"] = bondTotalFee;
            bondRow["typetotalpl"] = bondPL + " (" + bondPLperc + "%)";
            dtTypeResult.Rows.Add(bondRow);

            DataRow unitTrustRow = dtTypeResult.NewRow();
            unitTrustRow["type"] = "Unit Trust";
            unitTrustRow["typetotalbuyamount"] = unitTrustBuyTotal;
            unitTrustRow["typetotalsellamount"] = unitTrustSellTotal;
            unitTrustRow["typetotalfeespaid"] = unitTrustTotalFee;
            unitTrustRow["typetotalpl"] = unitTrustPL + " (" + unitTrustPLperc + "%)";
            dtTypeResult.Rows.Add(unitTrustRow);

*/
