﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using HKeInvestWebApplication.Code_File;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using Microsoft.AspNet.Identity;

namespace HKeInvestWebApplication.Client
{
    public partial class TradeSecurity : System.Web.UI.Page
    {
        HKeInvestData myHKeInvestData = new HKeInvestData();
        HKeInvestCode myHKeInvestCode = new HKeInvestCode();
        ExternalFunctions myExternalFunctions = new ExternalFunctions();
        HKeInvestService myHKeInvestService = new HKeInvestService();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string userName = Context.User.Identity.GetUserName();
                string sql = "select [accountNumber] from [Account] where [userName] ='" + userName + "';";
                string accountNumber = "";

                DataTable dtTemp = myHKeInvestData.getData(sql);
                if (!(dtTemp == null))
                {
                    accountNumber = dtTemp.Rows[0]["accountNumber"].ToString().Trim();
                }

                lblClientAccountNumber.Text = "Account Number: " + accountNumber;
                lblClientAccountNumber.Visible = true;

                sql = "select [lastName], [firstName] from [Client] where ([accountNumber]='" + accountNumber + "')";

                DataTable dtClient = myHKeInvestData.getData(sql);
                string clientName = "Client(s): ";
                int i = 1;
                foreach (DataRow row in dtClient.Rows)
                {
                    clientName = clientName + row["lastName"] + ", " + row["firstName"];
                    if (dtClient.Rows.Count != i)
                    {
                        clientName = clientName + "and ";
                    }
                    i = i + 1;
                }
                lblClientName.Text = clientName;
                lblClientName.Visible = true;

                ViewState["accountNumber"] = accountNumber;
            }
        }

        protected void ddlSecurityType_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnBondUnitTrust.Visible = false;
            pnStock.Visible = false;
            btnSubmit.Visible = false;
            rfvBondUnitAmount.Enabled = false;
            rfvBondUnitCode.Enabled = false;
            rfvStockAllOrNone.Enabled = false;
            rfvStockCode.Enabled = false;
            rfvStockExpiryDate.Enabled = false;
            rfvStockOrderType.Enabled = false;
            rfvStockShare.Enabled = false;
            revBondUnitAmount.Enabled = false;
            revBondUnitCode.Enabled = false;
            revStockCode.Enabled = false;
            revStockHigh.Enabled = false;
            revStockShare.Enabled = false;
            revStockStop.Enabled = false;
            ddlBuyOrSell.ClearSelection();
        }

        protected void ddlBuyOrSell_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnBondUnitTrust.Visible = false;
            pnStock.Visible = false;

            if (!((ddlSecurityType.SelectedValue == "0")||(ddlBuyOrSell.SelectedValue=="0")))
            {
                btnSubmit.Visible = true;
                btnSubmit.Enabled = true;
                btnSubmit.Text = "Submit " + ddlBuyOrSell.SelectedValue + " Order";
            }
            else
            {
                btnSubmit.Visible = false;
                btnSubmit.Enabled = false;
                rfvBondUnitAmount.Enabled = false;
                rfvBondUnitCode.Enabled = false;
                rfvStockAllOrNone.Enabled = false;
                rfvStockCode.Enabled = false;
                rfvStockExpiryDate.Enabled = false;
                rfvStockOrderType.Enabled = false;
                rfvStockShare.Enabled = false;
                return;
            }

            if (ddlBuyOrSell.SelectedValue == "buy")
            {
                if (ddlSecurityType.Text == "bond")
                {
                    lblBondUnitTrustHeading.Text = "Buy Bond";
                    lblBondUnitTrustCode.Text = "Bond Security Code:";
                    lblBondUnitAmount.Text = "Amount to be Bought (in HKD): ";
                    rfvBondUnitCode.Enabled = true;
                    rfvBondUnitAmount.Enabled = true;
                    revBondUnitAmount.Enabled = true;
                    revBondUnitCode.Enabled = true;
                    pnBondUnitTrust.Enabled = true;
                    pnBondUnitTrust.Visible = true;
                }
                if (ddlSecurityType.Text == "unit trust")
                {
                    lblBondUnitTrustHeading.Text = "Buy Unit Trust";
                    lblBondUnitTrustCode.Text = "Unit Trust Security Code:";
                    lblBondUnitAmount.Text = "Amount to be Bought (in HKD): ";
                    rfvBondUnitCode.Enabled = true;
                    rfvBondUnitAmount.Enabled = true;
                    revBondUnitAmount.Enabled = true;
                    revBondUnitCode.Enabled = true;
                    pnBondUnitTrust.Enabled = true;
                    pnBondUnitTrust.Visible = true;
                }
                if (ddlSecurityType.Text == "stock")
                {
                    lblStockHeading.Text = "Buy Stock";
                    lblStockShare.Text = "Shares to Buy (in 100): ";
                    rfvStockAllOrNone.Enabled = true;
                    rfvStockCode.Enabled = true;
                    rfvStockExpiryDate.Enabled = true;
                    rfvStockOrderType.Enabled = true;
                    rfvStockShare.Enabled = true;
                    revStockCode.Enabled = true;
                    revStockHigh.Enabled = true;
                    revStockShare.Enabled = true;
                    revStockStop.Enabled = true;
                    pnStock.Enabled = true;
                    pnStock.Visible = true;
                }
            }
            else if (ddlBuyOrSell.SelectedValue == "sell")
            {
                if (ddlSecurityType.Text == "bond")
                {
                    lblBondUnitTrustHeading.Text = "Sell Bond";
                    lblBondUnitTrustCode.Text = "Bond Security Code:";
                    lblBondUnitAmount.Text = "Amount to be Sold (in Shares): ";
                    rfvBondUnitCode.Enabled = true;
                    rfvBondUnitAmount.Enabled = true;
                    revBondUnitAmount.Enabled = true;
                    revBondUnitCode.Enabled = true;
                    pnBondUnitTrust.Enabled = true;
                    pnBondUnitTrust.Visible = true;
                }
                if (ddlSecurityType.Text == "unit trust")
                {
                    lblBondUnitTrustHeading.Text = "Sell Unit Trust";
                    lblBondUnitTrustCode.Text = "Unit Trust Security Code:";
                    lblBondUnitAmount.Text = "Amount to be Sold (in Shares): ";
                    rfvBondUnitCode.Enabled = true;
                    rfvBondUnitAmount.Enabled = true;
                    revBondUnitAmount.Enabled = true;
                    revBondUnitCode.Enabled = true;
                    pnBondUnitTrust.Enabled = true;
                    pnBondUnitTrust.Visible = true;
                }
                if (ddlSecurityType.Text == "stock")
                {
                    lblStockHeading.Text = "Sell Stock";
                    lblStockShare.Text = "Shares to Sell (in 100): ";
                    rfvStockAllOrNone.Enabled = true;
                    rfvStockCode.Enabled = true;
                    rfvStockExpiryDate.Enabled = true;
                    rfvStockOrderType.Enabled = true;
                    rfvStockShare.Enabled = true;
                    revStockCode.Enabled = true;
                    revStockHigh.Enabled = true;
                    revStockShare.Enabled = true;
                    revStockStop.Enabled = true;
                    pnStock.Enabled = true;
                    pnStock.Visible = true;
                }
            }
            else
            {
                pnBondUnitTrust.Enabled = false;
                pnBondUnitTrust.Visible = false;
                pnStock.Enabled = false;
                pnStock.Visible = false;
                rfvBondUnitAmount.Enabled = false;
                rfvBondUnitCode.Enabled = false;
                rfvStockAllOrNone.Enabled = false;
                rfvStockCode.Enabled = false;
                rfvStockExpiryDate.Enabled = false;
                rfvStockOrderType.Enabled = false;
                rfvStockShare.Enabled = false;
                revBondUnitAmount.Enabled = false;
                revBondUnitCode.Enabled = false;
                revStockCode.Enabled = false;
                revStockHigh.Enabled = false;
                revStockShare.Enabled = false;
                revStockStop.Enabled = false;

                string securityType = ddlSecurityType.SelectedValue;
                if (securityType == "0")
                {
                    lblOrderError.Text = "Please Select the Security Type";
                    lblOrderError.Visible = true;
                }
            }
        }

        /**Below methods are implemented to enable Buy/Sell(Trade) security use case. **/

        //checkBuyOrder will only check if the account balance is sufficient to submit the order.
        private Boolean checkBuyOrder(string type, string code, string amount)
        {
            string sql = "";
            decimal amountDecimal = Decimal.Parse(amount);
            if (!(amountDecimal == 0))
            {
                string accountNumber = (string)(ViewState["accountNumber"]);
                decimal price = myExternalFunctions.getSecuritiesPrice(type, code);
                sql = "Select [balance] from [Account] where [accountNumber] = '" + accountNumber + "';";
                DataTable dtAccount = myHKeInvestData.getData(sql);
                decimal balance = 0;
                if (!(dtAccount == null))
                {
                    string balanceString = dtAccount.Rows[0]["balance"].ToString().Trim();
                    balance = balance + Decimal.Parse(balanceString);
                }

                if (type == "bond" || type == "unit trust")
                {
                    if (amountDecimal > balance) return false;
                    else return true;
                }
                //needs more implementation
                else if (type == "stock")
                {
                    if ((amountDecimal * price) > balance) return false;
                    else return true;
                }
                else return false;
            }
            else return false;
        }

        //checkSellOrder will only check if the holding shares is sufficient to submit the order.
        private Boolean checkSellOrder(string type, string code, string shares)
        {
            string sql = "";
            decimal sharesInt = Decimal.Parse(shares);
            if (!(sharesInt == 0))
            {
                string accountNumber = (string)(ViewState["accountNumber"]);
                sql = "Select [shares] from [SecurityHolding] where [accountNumber] = '" + accountNumber + "' and [code] ='" + code +"';";
                DataTable dtAccount = myHKeInvestData.getData(sql);
                decimal holdingShare = 0;
                if (dtAccount == null || dtAccount.Rows.Count == 0) return false;
                else holdingShare = holdingShare + Decimal.Parse(dtAccount.Rows[0]["shares"].ToString().Trim());

                if (type == "bond" || type == "unit trust")
                {
                    if (sharesInt > holdingShare) return false;
                    else return true;
                }
                //needs more implementation
                else if (type == "stock")
                {
                    if (sharesInt > holdingShare) return false;
                    else return true;
                }
                else return false;
            }
            else return false;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (!Page.IsValid) return;
            lblOrderError.Visible = false;
            lblOrderNumber.Visible = false;
            pnResult.Visible = false;
            string result;

            if (ddlBuyOrSell.SelectedValue == "buy")
            {
                if (ddlSecurityType.SelectedValue == "bond")
                {
                    string code = txtBondUnitTrustCode.Text.Trim();
                    string amount = txtBondUnitAmount.Text.Trim();
                    if (checkBuyOrder("bond", code, amount))
                    {
                        result = myExternalFunctions.submitBondBuyOrder(code, amount);
                        if (result == null)
                        {
                            lblOrderError.Text = "Order was unsuccessful, Please check security information.";
                            lblOrderError.Visible = true;
                            pnResult.Visible = true;
                        }
                        else
                        {
                            lblOrderNumber.Text = "Order Successful, Reference Number: " + result;
                            myHKeInvestService.updateOrderInitial((string)(ViewState["accountNumber"]), result, ddlBuyOrSell.SelectedValue.Trim(), code, ddlSecurityType.SelectedValue.Trim(), amount);
                            lblOrderNumber.Visible = true;
                            pnResult.Visible = true;
                        }
                    }
                    else
                    {
                        lblOrderError.Text = "Order was unsuccessful. Insufficient balance.";
                        lblOrderError.Visible = true;
                        pnResult.Visible = true;
                    }
                }
                else if (ddlSecurityType.SelectedValue == "unit trust")
                {
                    string code = txtBondUnitTrustCode.Text.Trim();
                    string amount = txtBondUnitAmount.Text.Trim();
                    if (checkBuyOrder("unit trust", code, amount))
                    {
                        result = myExternalFunctions.submitUnitTrustBuyOrder(code, amount);
                        if (result == null)
                        {
                            lblOrderError.Text = "Order was unsuccessful, Please check security information.";
                            lblOrderError.Visible = true;
                            pnResult.Visible = true;
                        }
                        else
                        {
                            lblOrderNumber.Text = "Order Successful, Reference Number: " + result;
                            myHKeInvestService.updateOrderInitial((string)(ViewState["accountNumber"]), result, ddlBuyOrSell.SelectedValue.Trim(), code, ddlSecurityType.SelectedValue.Trim(), amount);
                            lblOrderNumber.Visible = true;
                            pnResult.Visible = true;
                        }
                    }
                    else
                    {
                        lblOrderError.Text = "Order was unsuccessful. Insufficient balance.";
                        lblOrderError.Visible = true;
                        pnResult.Visible = true;
                    }
                }
                else if (ddlSecurityType.SelectedValue == "stock")
                {
                    string code = txtStockCode.Text.Trim();
                    string amount = txtStockShare.Text.Trim();
                    if (checkBuyOrder("stock", code, amount))
                    {
                        string orderType = ddlStockOrderType.SelectedValue.Trim();
                        string expiryDay = ddlStockExpiryDate.SelectedValue.Trim();
                        string allOrNone = ddlStockAllOrNone.SelectedValue.Trim();
                        string highPrice = txtStockHigh.Text.Trim();
                        string stopPrice = txtStockStop.Text.Trim();
                        if (string.IsNullOrEmpty(highPrice)) highPrice = "";
                        if (string.IsNullOrEmpty(stopPrice)) stopPrice = "";

                        result = myExternalFunctions.submitStockBuyOrder(code, amount, orderType, expiryDay, allOrNone, highPrice, stopPrice);
                        if (result == null)
                        {
                            lblOrderError.Text = "Order was unsuccessful, Please check security information.";
                            lblOrderError.Visible = true;
                            pnResult.Visible = true;
                        }
                        else
                        {
                            lblOrderNumber.Text = "Order Successful, Reference Number: " + result;
                            myHKeInvestService.updateOrderInitial((string)(ViewState["accountNumber"]), result, ddlBuyOrSell.SelectedValue.Trim(), code, ddlSecurityType.SelectedValue.Trim(), amount,  ddlStockOrderType.SelectedValue.Trim(), expiryDay, allOrNone, highPrice, stopPrice);
                            lblOrderNumber.Visible = true;
                            pnResult.Visible = true;
                        }
                    }
                    else
                    {
                        lblOrderError.Text = "Order was unsuccessful. Insufficient balance.";
                        lblOrderError.Visible = true;
                        pnResult.Visible = true;
                    }
                }
                else return;
            }
            else if (ddlBuyOrSell.SelectedValue == "sell")
            {
                if (ddlSecurityType.SelectedValue == "bond")
                {
                    string code = txtBondUnitTrustCode.Text.Trim();
                    string shares = txtBondUnitAmount.Text.Trim();
                    if (checkSellOrder("bond", code, shares))
                    {
                        result = myExternalFunctions.submitBondSellOrder(code, shares);
                        if (result == null)
                        {
                            lblOrderError.Text = "Order was unsuccessful, Please check security information.";
                            lblOrderError.Visible = true;
                            pnResult.Visible = true;
                        }
                        else
                        {
                            lblOrderNumber.Text = "Order Successful, Reference Number: " + result;
                            myHKeInvestService.updateOrderInitial((string)(ViewState["accountNumber"]), result, ddlBuyOrSell.SelectedValue.Trim(), code, ddlSecurityType.SelectedValue.Trim(), shares);
                            lblOrderNumber.Visible = true;
                            pnResult.Visible = true;
                        }
                    }
                    else
                    {
                        lblOrderError.Text = "Order was unsuccessful. Insufficient Shares.";
                        lblOrderError.Visible = true;
                        pnResult.Visible = true;
                    }
                }
                else if (ddlSecurityType.SelectedValue == "unit trust")
                {
                    string code = txtBondUnitTrustCode.Text.Trim();
                    string shares = txtBondUnitAmount.Text.Trim();
                    if (checkSellOrder("unit trust", code, shares))
                    {
                        result = myExternalFunctions.submitUnitTrustSellOrder(code, shares);
                        if (result == null)
                        {
                            lblOrderError.Text = "Order was unsuccessful, Please check security information.";
                            lblOrderError.Visible = true;
                            pnResult.Visible = true;
                        }
                        else
                        {
                            lblOrderNumber.Text = "Order Successful, Reference Number: " + result;
                            myHKeInvestService.updateOrderInitial((string)(ViewState["accountNumber"]), result, ddlBuyOrSell.SelectedValue.Trim(), code, ddlSecurityType.SelectedValue.Trim(), shares);
                            lblOrderNumber.Visible = true;
                            pnResult.Visible = true;
                        }
                    }
                    else
                    {
                        lblOrderError.Text = "Order was unsuccessful. Insufficient Shares.";
                        lblOrderError.Visible = true;
                        pnResult.Visible = true;
                    }
                }
                else if (ddlSecurityType.SelectedValue == "stock")
                {
                    string code = txtStockCode.Text.Trim();
                    string amount = txtStockShare.Text.Trim();
                    if (checkSellOrder("stock", code, amount))
                    {
                        string orderType = ddlStockOrderType.SelectedValue.Trim();
                        string expiryDay = ddlStockExpiryDate.SelectedValue.Trim();
                        string allOrNone = ddlStockAllOrNone.SelectedValue.Trim();
                        string highPrice = txtStockHigh.Text.Trim();
                        string stopPrice = txtStockStop.Text.Trim();
                        if (string.IsNullOrEmpty(highPrice)) highPrice = "";
                        if (string.IsNullOrEmpty(stopPrice)) stopPrice = "";

                        result = myExternalFunctions.submitStockSellOrder(code, amount, orderType, expiryDay, allOrNone, highPrice, stopPrice);
                        if (result == null)
                        {
                            lblOrderError.Text = "Order was unsuccessful, Please check security information.";
                            lblOrderError.Visible = true;
                            pnResult.Visible = true;
                        }
                        else
                        {
                            lblOrderNumber.Text = "Order Successful, Reference Number: " + result;
                            myHKeInvestService.updateOrderInitial((string)(ViewState["accountNumber"]), result, ddlBuyOrSell.SelectedValue.Trim(), code, ddlSecurityType.SelectedValue.Trim(), amount,  ddlStockOrderType.SelectedValue.Trim(), expiryDay, allOrNone, highPrice, stopPrice);
                            lblOrderNumber.Visible = true;
                            pnResult.Visible = true;
                        }
                    }
                    else
                    {
                        lblOrderError.Text = "Order was unsuccessful. Insufficient Shares.";
                        lblOrderError.Visible = true;
                        pnResult.Visible = true;
                    }
                }
                else return;
            }
            else return;
        }
    }
}