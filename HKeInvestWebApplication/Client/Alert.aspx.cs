﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using HKeInvestWebApplication.Code_File;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using Microsoft.AspNet.Identity;
using System.Data.SqlClient;
using System.Net.Mail;


namespace HKeInvestWebApplication.Client
{
    public partial class Alert : System.Web.UI.Page
    {
        HKeInvestData myHKeInvestData = new HKeInvestData();
        HKeInvestCode myHKeInvestCode = new HKeInvestCode();
        ExternalFunctions myExternalFunctions = new ExternalFunctions();
        DataTable dtAlerts;

        protected void Page_Load(object sender, EventArgs e)
        {
            string userName = Context.User.Identity.GetUserName();
            string sql = "select [accountNumber] from [Account] where [userName] ='" + userName + "';";
            string accountNumber = "";

            DataTable dtTemp = myHKeInvestData.getData(sql);
            if (!(dtTemp == null || dtTemp.Rows.Count == 0))
            {
                accountNumber = dtTemp.Rows[0]["accountNumber"].ToString().Trim();
            }

            lblClientAccountNumber.Text = "Account Number: " + accountNumber;
            lblClientAccountNumber.Visible = true;

            sql = "select [lastName], [firstName], [email] from [Client] where ([accountNumber]='" + accountNumber + "')";

            DataTable dtClient = myHKeInvestData.getData(sql);
            string clientName = "Client(s): ";
            int i = 1;
            foreach (DataRow row in dtClient.Rows)
            {
                clientName = clientName + row["lastName"] + ", " + row["firstName"];
                if (dtClient.Rows.Count != i)
                {
                    clientName = clientName + "and ";
                }
                i = i + 1;
            }
            lblClientName.Text = clientName;
            lblClientName.Visible = true;

            string email = dtClient.Rows[0]["email"].ToString().Trim();
            ViewState["email"] = email;
            ViewState["accountNumber"] = accountNumber;

            //Get Previous Alerts
            sql = "select [type],[code],[name],[highAlarm],[lowAlarm] from [SecurityHolding] where [accountNumber] = '" + accountNumber + "';";
            dtAlerts = myHKeInvestData.getData(sql);
            if(!(dtAlerts == null || dtAlerts.Rows.Count == 0))
            {
                foreach (DataRow row in dtAlerts.Rows)
                {
                    if (row.IsNull("highAlarm") && row.IsNull("lowAlarm"))
                    {
                        row.Delete();
                    }
                }
                dtAlerts.AcceptChanges();
            }
            if(dtAlerts == null || dtAlerts.Rows.Count == 0)
            {
                lblError.Text = "You have no alerts configured currently.";
                lblError.Visible = true;
                gvAlert.Visible = false;
            }
            else
            {
                lblError.Visible = false;
                gvAlert.Visible = true;

                gvAlert.DataSource = dtAlerts;
                gvAlert.DataBind();
            }

            lblAlertError.Visible = false;
            lblPrice.Visible = false;
            txtAlertValue.Enabled = false;
            txtCode.Enabled = true;
            ddlAlertType.Enabled = false;
            ddlSecurityType.Enabled = true;
            rfvAlertType.Enabled = false;
            rfvAlertValue.Enabled = false;
            revAlertValue.Enabled = false;
        }

        protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string accountNumber = (string)(ViewState["accountNumber"]);
            string sql = "update [SecurityHolding] set highAlarm = NULL, lowAlarm = NULL where [accountNumber] = '" + accountNumber + "' and [code] = '" + gvAlert.Rows[e.RowIndex].Cells[2].Text + "'";
            SqlTransaction trans = myHKeInvestData.beginTransaction();
            myHKeInvestData.setData(sql, trans);
            myHKeInvestData.commitTransaction(trans);
            Response.Redirect(Request.RawUrl);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string securityType = ddlSecurityType.SelectedValue;
            string securityCode = txtCode.Text.Trim();
            decimal currentPrice = myExternalFunctions.getSecuritiesPrice(securityType, securityCode);
            ViewState["currentPrice"] = currentPrice;
            if(currentPrice == -1)
            {
                lblAlertError.Text = "Please check if security information is valid.";
                lblAlertError.Visible = true;
                lblPrice.Visible = false;

                ddlAlertType.Enabled = false;
                txtAlertValue.Enabled = false;
                btnSetAlert.Enabled = false;
                rfvAlertType.Enabled = false;
                rfvAlertValue.Enabled = false;
                revAlertValue.Enabled = false;
                ddlSecurityType.Enabled = true;
                txtCode.Enabled = true;
            } 
            else
            {
                string accountNumber = (string)(ViewState["accountNumber"]);
                string sql = "select count(*) from [SecurityHolding] where [accountNumber] = '" + accountNumber + "' and [code] = '" + securityCode + "'";
                decimal count = myHKeInvestData.getAggregateValue(sql);
                if (count == -1 || count == 0)
                {
                    lblAlertError.Text = "You do not hold the specified security.";
                    lblAlertError.Visible = true;
                    lblPrice.Visible = false;

                    ddlAlertType.Enabled = false;
                    txtAlertValue.Enabled = false;
                    btnSetAlert.Enabled = false;
                    rfvAlertType.Enabled = false;
                    rfvAlertValue.Enabled = false;
                    revAlertValue.Enabled = false;
                    ddlSecurityType.Enabled = true;
                    txtCode.Enabled = true;

                }
                else
                {
                    lblPrice.Text = "Current Price: " + Convert.ToString(currentPrice) + ".";
                    lblPrice.Visible = true;
                    lblAlertError.Visible = false;

                    ddlAlertType.Enabled = true;
                    txtAlertValue.Enabled = true;
                    btnSetAlert.Enabled = true;
                    rfvAlertType.Enabled = true;
                    rfvAlertValue.Enabled = true;
                    revAlertValue.Enabled = true;
                    ddlSecurityType.Enabled = false;
                    txtCode.Enabled = false;
                }
            }
        }

        protected void btnSetAlert_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (!Page.IsValid) return;
            string accountNumber = (string)(ViewState["accountNumber"]);
            string sql = "";
            if (ddlAlertType.SelectedValue == "low") {
                sql = "select max([highAlarm]) from [SecurityHolding] where [accountNumber] = '" + accountNumber + "' and [code] = '" + txtCode.Text.Trim() + "'";
                decimal highPrice = myHKeInvestData.getAggregateValue(sql);
                decimal lowPrice = Decimal.Parse(txtAlertValue.Text);
                if (lowPrice >= highPrice && highPrice != -1)
                {
                    lblAlertError.Text = "Please set valid Low Alert Value. Cannot be higher than High Alert.";
                    lblAlertError.Visible = true;
                    return;
                }
                else
                {
                    decimal currentPrice = (decimal)(ViewState["currentPrice"]);
                    sql = "update [SecurityHolding] set [lowAlarm] = '" + txtAlertValue.Text.Trim() + "', ";
                    if (lowPrice > currentPrice) sql = sql + "[lowComparator] = 'H'";
                    else sql = sql + "[lowComparator] = 'L'";
                    sql = sql + " where [accountNumber] = '" + accountNumber + "' and [code] = '" + txtCode.Text.Trim() + "'";
                }
                
            }
            else if (ddlAlertType.SelectedValue == "high") {
                sql = "select min([lowAlarm]) from [SecurityHolding] where [accountNumber] = '" + accountNumber + "' and [code] = '" + txtCode.Text.Trim() + "'";
                decimal lowPrice = myHKeInvestData.getAggregateValue(sql);
                decimal highPrice = Decimal.Parse(txtAlertValue.Text);
                if (highPrice <= lowPrice && lowPrice != -1)
                {
                    lblAlertError.Text = "Please set valid High Alert Value. Cannot be lower than Low Alert.";
                    lblAlertError.Visible = true;
                    return;
                }
                else
                {
                    decimal currentPrice = (decimal)(ViewState["currentPrice"]);
                    sql = "update [SecurityHolding] set [highAlarm] = '" + txtAlertValue.Text.Trim() + "', ";
                    if (highPrice < currentPrice) sql = sql + "[highComparator] = 'L'";
                    else sql = sql + "[highComparator] = 'H'";
                    sql = sql + " where [accountNumber] = '" + accountNumber + "' and [code] = '" + txtCode.Text.Trim() + "'";
                }
            }
            SqlTransaction trans = myHKeInvestData.beginTransaction();
            myHKeInvestData.setData(sql, trans);
            myHKeInvestData.commitTransaction(trans);
            Response.Redirect(Request.RawUrl);
        }
    }
}