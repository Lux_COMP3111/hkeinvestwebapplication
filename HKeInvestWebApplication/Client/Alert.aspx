﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Alert.aspx.cs" Inherits="HKeInvestWebApplication.Client.Alert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">  
    <div class="page-header">
        <h2>Security Price Alert Settings</h2>
        <asp:Label runat="server" ID="lblClientAccountNumber" CssClass="control-label label label-success" Font-Size="Small"></asp:Label>
        <asp:Label runat="server" ID="lblClientName" Visible="false" CssClass="label label-info" Font-Size="Small"></asp:Label>
    </div>

    <%--Validation Summary for Validators--%>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <asp:ValidationSummary runat="server" CssClass="text-danger" EnableClientScript="false" />
    
    <%--Largest Div Element--%>
    <div class="form-horizontal panel-group">
        <asp:Panel runat="server" ID="pnCurrent" CssClass="panel panel-default">
            <div class="panel-heading">Your current alerts</div>
            <div class="panel-body">
                <div class="form-group col-md-12">
                    <asp:Label runat="server" ID="lblStatus" CssClass="label label-primary control-label" Visible="false" Font-Size="Small"></asp:Label>
                    <asp:Label runat="server" ID="lblError" CssClass="label label-danger control-label" Visible="false" Font-Size="Small"></asp:Label>
                </div>
                <div class="form-group col-md-12">
                    <div class="table-responsive">
                        <asp:GridView runat="server" ID="gvAlert" Visible="False" OnRowDeleting="OnRowDeleting" AutoGenerateColumns="False" CssClass="table table-bordered table-condensed table-hover">
                            <Columns>
                                <asp:CommandField ShowDeleteButton="True" />
                                <asp:BoundField DataField="type" HeaderText="Security Type" ReadOnly="True" SortExpression="email" />
                                <asp:BoundField DataField="code" HeaderText="Security Code" ReadOnly="True" SortExpression="code" />
                                <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True" SortExpression="type" />
                                <asp:BoundField DataField="highAlarm" DataFormatString="{0:n2}" HeaderText="High Alert Price" ReadOnly="True" />
                                <asp:BoundField DataField="lowAlarm" DataFormatString="{0:n2}" HeaderText="Low Alert Price" ReadOnly="True" />
                            </Columns>
                        </asp:GridView>
                     </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnAlertSetting" CssClass="panel panel-default">
            <div class="panel-heading">Create or Change Alert for your Securities</div>
            <div class="panel-body">
                <div class="form-group col-md-12">
                    <asp:Label runat="server" ID="lblResult" Visible="false" CssClass="control-label label label-primary" Font-Size ="Small"></asp:Label>
                    <asp:Label runat="server" ID="lblAlertError" Visible="false" CssClass="control-label label label-danger" Font-Size="Small"></asp:Label>
                </div>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" AssociatedControlID="ddlSecurityType" CssClass="control-label col-md-3" Text="Security Type:"></asp:Label>
                    <div class="col-md-4">
                        <asp:DropDownList runat="server" ID="ddlSecurityType" CssClass="form-control" AutoPostBack="false">
                            <asp:ListItem Value="0">-- Select Security --</asp:ListItem>
                            <asp:ListItem Value="bond">Bond</asp:ListItem>
                            <asp:ListItem Value="stock">Stock</asp:ListItem>
                            <asp:ListItem Value="unit trust">Unit Trust</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator runat="server" ID="rfvSecurityType" ControlToValidate="ddlSecurityType" InitialValue="0" CssClass="label label-danger col-md-2" ErrorMessage="Please Select Security Type."></asp:RequiredFieldValidator>
                </div>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" AssociatedControlID="txtCode" CssClass="control-label col-md-3" Text="Security Code:"></asp:Label>
                    <div class="col-md-4">
                        <asp:TextBox runat="server" ID="txtCode" CssClass="form-control"></asp:TextBox>
                    </div>
                    <asp:RequiredFieldValidator runat="server" ID="rfvCode" ControlToValidate="txtCode" CssClass="label label-danger col-md-2" ErrorMessage="Please Enter Security Code."></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="revCode" ControlToValidate="txtCode" CssClass="label label-danger col-md-2" ErrorMessage="Numeric Value Only." ValidationExpression="\d+"></asp:RegularExpressionValidator>
                </div>
                <div class="form-group col-md-12">
                    <div class="col-md-offset-3 col-md-2"><asp:Button runat="server" ID="btnSearch" CssClass="btn btn-primary" Text="Look Up Security" OnClick="btnSearch_Click" /> </div>
                </div>
                <hr />
                <div class="form-group col-md-12">
                    <asp:Label runat="server" ID="lblPrice" CssClass="label label-info" Visible="false"></asp:Label>
                </div>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" AssociatedControlID="ddlAlertType" CssClass="control-label col-md-3" Text="Alert Type:"></asp:Label>
                    <div class="col-md-4">
                        <asp:DropDownList runat="server" ID="ddlAlertType" CssClass="form-control" AutoPostBack="false" Enabled="false">
                            <asp:ListItem Value="0">-- Select Type --</asp:ListItem>
                            <asp:ListItem Value="high">High Value Alert</asp:ListItem>
                            <asp:ListItem Value="low">Low Value Alert</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:RequiredFieldValidator runat="server" ID="rfvAlertType" ControlToValidate="ddlAlertType" InitialValue="0" CssClass="label label-danger col-md-2" ErrorMessage="Please Specify Alert Type." Enabled="false"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" AssociatedControlID="txtAlertValue" CssClass="control-label col-md-3" Text="Alert Value:"></asp:Label>
                    <div class="col-md-4">
                        <asp:TextBox runat="server" ID="txtAlertValue" CssClass="form-control" Enabled="false"></asp:TextBox>
                    </div>
                    <asp:RequiredFieldValidator runat="server" ID="rfvAlertValue" ControlToValidate="txtAlertValue" CssClass="label label-danger col-md-2" ErrorMessage="Please Specify Alert Value" Enabled="false"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="revAlertValue" ControlToValidate="txtAlertValue" CssClass="label label-danger col-md-2" ErrorMessage="Decimal Value Only." Enabled="false" ValidationExpression="(?:\d*\.)?\d+"></asp:RegularExpressionValidator>
                </div>
                <div class="form-group col-md-12">
                    <div class="col-md-2 col-md-offset-3"><asp:Button runat="server" ID="btnSetAlert" CssClass="btn btn-primary" Text="Set Alert" Enabled="false" OnClick="btnSetAlert_Click" /></div>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>

