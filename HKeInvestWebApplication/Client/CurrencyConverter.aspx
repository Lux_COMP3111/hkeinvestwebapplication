﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CurrencyConverter.aspx.cs" Inherits="HKeInvestWebApplication.Client.CurrencyConverter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-header">
        <h2>Currency Converter</h2>
        <asp:Label runat="server" ID="lblClientAccountNumber" CssClass="control-label label label-success" Font-Size="Small"></asp:Label>
        <asp:Label runat="server" ID="lblClientName" Visible="false" CssClass="label label-info" Font-Size="Small"></asp:Label>
    </div>
    <div class="form-horizontal panel-group">
        <%--Currency Information Panel--%>
        <asp:Panel runat="server" ID="pnCurrency" CssClass="panel panel-default">
            <div class="panel-heading">Today's Currency</div>
            <div class="form-group panel-body">
                <div class="col-md-6">
                    <asp:DataList runat="server" ID="dlCurrency" RepeatDirection="Horizontal">
                        <ItemTemplate>
                            <asp:Table runat="server" ID="tblCurrency" CssClass="table table-condensed table-hover">
                                <asp:TableRow runat="server"><asp:TableCell HorizontalAlign="Center"><asp:Label ID="dllblbase" runat="server" Text='<%# Eval("currency")+ " " %>' CssClass="label label-info" Font-Size="Small"></asp:Label></asp:TableCell></asp:TableRow>
                                <asp:TableRow runat="server"><asp:TableCell HorizontalAlign="Center"><asp:Label ID="dllblrate" runat="server" Text='<%# Eval("rate")+ " " %>' CssClass="label label-default" Font-Size="Small"></asp:Label></asp:TableCell></asp:TableRow>
                            </asp:Table>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
            </div>
        </asp:Panel>
        <%--Base currency, Target Currency, Amount Selector--%>
        <asp:Panel runat="server" ID="pnInput" CssClass="panel panel-default" HorizontalAlign="Center">
            <div class="form-group panel-body">
                <div class="form-horizontal">
                    <div class="col-md-3">
                        <asp:DropDownList runat="server" ID="ddlBaseCurrency" AutoPostBack="false" CssClass="form-control">
                            <asp:ListItem Selected="True">HKD</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox runat="server" ID="txtAmount" CssClass="form-control" MaxLength="20"></asp:TextBox>
                    </div>
                    <asp:Label runat="server" ID="lblGlyph" CssClass="control-label col-md-1"><span class="glyphicon glyphicon-sort"></span></asp:Label>
                    <div class="col-md-3">
                        <asp:DropDownList runat="server" ID="ddlTargetCurrency" AutoPostBack="false" CssClass="form-control">
                            <asp:ListItem Selected="True">HKD</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:Button runat="server" ID="btnCalculate" CssClass="btn btn-primary" Text="Convert" OnClick="btnCalculate_Click" EnableViewState="true"></asp:Button>
                    </div>
                </div>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtAmount" CssClass="text-danger" EnableClientScript="false" ErrorMessage="Please Specify Amount."></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ControlToValidate="txtAmount" CssClass="text-danger" EnableClientScript="false" ErrorMessage="Please use numerical input for specifying amount" ValidationExpression="(?:\d*\.)?\d+"></asp:RegularExpressionValidator>
            </div>
        </asp:Panel>
        <%--Result Panel--%>
        <asp:Panel runat="server" ID="pnResult" CssClass="panel panel-default" Visible="false">
            <div class="panel-heading">Conversion Result</div>
            <div class="form-group panel-body">
                <%--Result Msg Labels--%>
                <div class="form-horizontal col-md-12">
                    <asp:Label runat="server" ID="lblStatus" CssClass="label label-primary" Visible="false" Font-Size="Small"></asp:Label>
                    <asp:Label runat="server" ID="lblError" CssClass="label label-danger" Visible="false" Font-Size="Small"></asp:Label>
                </div>
                <div class="form-horizontal col-md-12">
                    <asp:Table runat="server" ID="tblResult" CssClass="table table-condensed" Visible="false">
                        <asp:TableRow>
                            <asp:TableCell runat="server" HorizontalAlign="Right"><asp:Label runat="server" ID="lblResultBase" CssClass="label label-default" Font-Size="Medium"></asp:Label></asp:TableCell> 
                            <asp:TableCell runat="server" Wrap="true" HorizontalAlign="Center"><span class="glyphicon glyphicon-sort"></span></asp:TableCell>
                            <asp:TableCell runat="server" HorizontalAlign="Left"><asp:Label runat="server" ID="lblResultTarget" CssClass="label label-success" Font-Size="Medium"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                 </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
