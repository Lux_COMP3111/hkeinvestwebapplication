﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BestSeller.aspx.cs" Inherits="HKeInvestWebApplication.Client.BestSeller" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-header">
        <h2>Monthly Best Sellers</h2>
    </div>
    <div class="form-horizontal panel-group">
        <asp:Panel runat="server" ID="pnResult" CssClass="panel panel-default">
            <div class="panel-heading"><asp:Label runat="server" ID="pnResultHeading" Font-Bold ="true"></asp:Label></div>
            <div class="panel-body">
                <div class="form-group col-md-12">
                    <asp:Label runat="server" ID="lblDate" CssClass="label label-primary" Font-Size="Small"></asp:Label>
                    <asp:Label runat="server" ID="lblStatus" CssClass="label label-success" Visible="false" Font-Size="Small"></asp:Label>
                    <asp:Label runat="server" ID="lblError" CssClass="label label-danger" Visible="false" Font-Size="Small"></asp:Label>
                </div>
                <div class="form-group col-md-12">
                    <asp:GridView runat="server" ID="gvBestSeller" CssClass="table table-bordered table-condensed table-hover" AutoGenerateColumns="False" Visible="False" AllowSorting="True" HorizontalAlign="Center" OnSorting="gvBestSeller_Sorting">
                        <Columns>
                            <asp:BoundField DataField="type" HeaderText="Type" ReadOnly="True" />
                            <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="True" />
                            <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True" />
                            <asp:BoundField DataField="orders" HeaderText="Total Orders" ReadOnly="True" SortExpression="orders" />
                            <asp:BoundField DataField="highPrice" DataFormatString="{0:0.00}" HeaderText="High Price (HKD)" ReadOnly="True" />
                            <asp:BoundField DataField="lowPrice" DataFormatString="{0:0.00}" HeaderText="Low Price (HKD)" ReadOnly="True" />
                            <asp:BoundField DataField="volume" DataFormatString="{0:0.00}" HeaderText="Volume (HKD)" ReadOnly="True" SortExpression="volume" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </asp:Panel>
    </div>

</asp:Content>
