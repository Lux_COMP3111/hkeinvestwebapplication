﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using HKeInvestWebApplication.Code_File;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using Microsoft.AspNet.Identity;
using System.Globalization;

namespace HKeInvestWebApplication.Client
{
    public partial class ReportGeneration : System.Web.UI.Page
    {
        HKeInvestData myHKeInvestData = new HKeInvestData();
        HKeInvestCode myHKeInvestCode = new HKeInvestCode();
        HKeInvestService myHKeInvestService = new HKeInvestService();
        ExternalFunctions myExternalFunctions = new ExternalFunctions();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ddlSecurityType.SelectedValue == "0")
            {
                pnAll.Visible = false;
                pnResult.Visible = false;
                pnActive.Visible = false;
                pnHistory.Visible = false;
                pnRange.Visible = false;
            }

            else if (ddlSecurityType.SelectedValue == "all")
            {
                pnAll.Visible = true;
                pnActive.Visible = false;
                pnHistory.Visible = false;
                pnResult.Visible = false;
                pnRange.Visible = false;
            }
            else if (ddlSecurityType.SelectedValue == "active")
            {
                pnAll.Visible = false;
                pnActive.Visible = true;
                pnHistory.Visible = false;
                pnResult.Visible = false;
                pnRange.Visible = false;
            }
            else if (ddlSecurityType.SelectedValue == "history")
            {
                pnAll.Visible = false;
                pnActive.Visible = false;
                pnHistory.Visible = true;
                pnResult.Visible = false;
                pnRange.Visible = true;
            }
            else
            {
                pnAll.Visible = false;
                pnActive.Visible = false;
                pnHistory.Visible = false;
                pnResult.Visible = true;
                pnRange.Visible = false;
            }

            // Get the available currencies to populate the DropDownList.
            if (!Page.IsPostBack)
            {
                DataTable dtCurrency = myExternalFunctions.getCurrencyData();
                foreach (DataRow row in dtCurrency.Rows)
                {
                    ddlCurrency.Items.Add(row["currency"].ToString().Trim());
                }
                ViewState["dtCurrencyData"] = dtCurrency;

                //Store the User Name and Account Number of the currently logged-in client in viewstate.
                //And display them in lblAccountNumber and lblClientName.
                string userNameTemp = Context.User.Identity.GetUserName();
                string sql = "select [accountNumber] from [Account] where [userName] ='" + userNameTemp + "';";
                string accountNumberTemp = "";

                DataTable dtTemp = myHKeInvestData.getData(sql);
                if (!(dtTemp == null))
                {
                    accountNumberTemp = dtTemp.Rows[0]["accountNumber"].ToString().Trim();
                }

                txtAccountNumber.Text = accountNumberTemp;
                txtAccountNumber.Enabled = false;
                ViewState["userName"] = userNameTemp;
                ViewState["accountNumber"] = accountNumberTemp;
            }
        }
       
        private Boolean searchAccount(string accountNumber)
        {
            string sql = "";
            sql = "select [lastName], [firstName] from [Client] where ([accountNumber]='" + accountNumber + "')"; // Complete the SQL statement.

            DataTable dtClient = myHKeInvestData.getData(sql);
            if (dtClient == null) { return false; } // If the DataSet is null, a SQL error occurred.

            // If no result is returned by the SQL statement, then display a message.
            if (dtClient.Rows.Count == 0)
            {
                lblResultMessage.Text = "No such account number.";
                lblResultMessage.Visible = true;
                lblClientName.Visible = false;
                lblResultMessage2.Text = "No such account number.";
                lblResultMessage2.Visible = true;
                lblClientName2.Visible = false;
                lblResultMessage3.Text = "No such account number.";
                lblResultMessage3.Visible = true;
                lblClientName3.Visible = false;
                lblResultMessage4.Text = "No such account number.";
                lblResultMessage4.Visible = true;
                lblClientName4.Visible = false;
                return false;
            }
            // Show the client name(s) on the web page.
            string clientName = "Client(s): ";
            int i = 1;
            foreach (DataRow row in dtClient.Rows)
            {
                clientName = clientName + row["lastName"] + ", " + row["firstName"];
                if (dtClient.Rows.Count != i)
                {
                    clientName = clientName + "and ";
                }
                i = i + 1;
            }
            lblClientName.Text = clientName;
            lblClientName.Visible = true;
            lblClientName2.Text = clientName;
            lblClientName2.Visible = true;
            lblClientName3.Text = clientName;
            lblClientName3.Visible = true;
            lblClientName4.Text = clientName;
            lblClientName4.Visible = true;
            return true;
        }
        protected void ddlSecurityType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Reset visbility of controls and initialize values.
            lblResultMessage.Visible = false;
            lblResultMessage2.Visible = false;
            lblResultMessage3.Visible = false;
            lblResultMessage4.Visible = false;
            ddlCurrency.Visible = false;
            pnAll.Visible = false;
            pnActive.Visible = false;
            pnHistory.Visible = false;
            pnResult.Visible = false;
            gvSecurityHolding.Visible = false;
            ddlCurrency.SelectedIndex = 0;
            pnRange.Visible = false;
            string sql = "";

            // *******************************************************************
            // TODO: Set the account number and security type from the web page. *
            // *******************************************************************
            string accountNumber = txtAccountNumber.Text.Trim(); // Set the account number from a web form control!
            string securityType = ddlSecurityType.SelectedValue.Trim(); // Set the securityType from a web form control!

            // Check if an account number has been specified.
            if (accountNumber == "")
            {
                lblResultMessage.Text = "Please specify an account number.";
                lblResultMessage.Visible = true;
                lblResultMessage2.Text = "Please specify an account number.";
                lblResultMessage2.Visible = true;
                lblResultMessage3.Text = "Please specify an account number.";
                lblResultMessage3.Visible = true;
                lblResultMessage4.Text = "Please specify an account number.";
                lblResultMessage4.Visible = true;
                ddlSecurityType.SelectedIndex = 0;
                pnAll.Visible = true;
                pnDetails.Visible = false;

                return;
            }

            // No action when the first item in the DropDownList is selected.
            if (securityType == "0")
            {
                return;
            }

            // Display account information
            else if (securityType == "all")
            {
                pnAll.Visible = true;
                if (searchAccount(accountNumber) == false) { return; }
                // get balance of account
                sql = "select [balance] from [Account]  where ([accountNumber]='" + accountNumber + "')";
                DataTable dtAccount = myHKeInvestData.getData(sql);
                FreeBalance.Text = (dtAccount.Rows[0]["balance"].ToString());
                // get the last order if there is any
                sql = "select [code], [type], [dateSubmitted], [executeDate], [executeShares], [executePrice] from [Invoice]  where ([accountNumber]='" + accountNumber + "') and ([status]='completed') ORDER BY [executeDate] DESC";
                DataTable dtOrder = myHKeInvestData.getData(sql);
                // Display none if there is no order
                if (dtOrder.Rows.Count == 0)
                {
                    LastDate.Text = "none";
                    LastValue.Text = "none";
                }
                // Display last order
                else
                {
                    decimal lastshare = Convert.ToDecimal(dtOrder.Rows[0]["executeShares"]);
                    decimal lastprice = Convert.ToDecimal(dtOrder.Rows[0]["executePrice"]);
                    LastDate.Text = dtOrder.Rows[0]["dateSubmitted"].ToString();
                    LastValue.Text =Math.Round(lastshare*lastprice*myHKeInvestService.getCurrency(dtOrder.Rows[0]["type"].ToString().Trim(), dtOrder.Rows[0]["code"].ToString().Trim()),3).ToString();
                }
                // Queury security holdings
                decimal bond = 0;
                decimal stock = 0;
                decimal trust = 0;
                sql = "select * from [SecurityHolding] where ([accountNumber] = '" + accountNumber + "')";
                DataTable dtSecurityHolding = myHKeInvestData.getData(sql);
                if (dtSecurityHolding == null||dtSecurityHolding.Rows.Count == 0)
                {
                    lblResultMessage2.Text = "No " + "securities" + " held in this account.";
                    lblResultMessage2.Visible = true;
                    Bond.Text = bond.ToString();
                    Stock.Text = stock.ToString();
                    Trust.Text = trust.ToString();
                }
                // Calculate total asset
                decimal total = myHKeInvestService.calculateAsset(accountNumber);
                // Display 0 if there is an error
                if (total == -1) { Total.Text = "0 Asset non-existant"; }
                // Display total
                else { Total.Text = total.ToString(); }

                // For each security in the result, get its current price from an external system, calculate the total value
                // of the security and change the current price and total value columns of the security in the result.
                string type = "";
                decimal shares = 0;
                decimal price = 0;
                foreach (DataRow row in dtSecurityHolding.Rows)
                {
                    type = row["type"].ToString().Trim();
                    if (type == "bond")
                    {
                        shares = Decimal.Parse(row["shares"].ToString().Trim());
                        price = myHKeInvestService.convertPrice(type, row["code"].ToString());
                        bond = bond + Convert.ToDecimal(shares * price);

                    }
                    else if (type == "stock")
                    {
                        shares = Decimal.Parse(row["shares"].ToString().Trim());
                        price = myHKeInvestService.convertPrice(type, row["code"].ToString().Trim());
                        stock = stock + Convert.ToDecimal(shares * price);
                    }
                    else
                    {
                        shares = Decimal.Parse(row["shares"].ToString().Trim());
                        price = myHKeInvestService.convertPrice(type, row["code"].ToString().Trim());
                        trust = trust + Convert.ToDecimal(shares * price);
                    }
                }
                Bond.Text = Math.Round(bond,3).ToString().Trim();
                Stock.Text = Math.Round(stock,3).ToString().Trim();
                Trust.Text = Math.Round(trust,3).ToString().Trim();

                pnDetails.Visible = true;
            }

            // Display active orders
            else if (securityType == "active")
            {
                pnActive.Visible = true;
                if (searchAccount(accountNumber) == false) { return; }
                sql = "select [referenceNumber], [buyOrSell], [type], [code], [name], [dateSubmitted], [status] from [Invoice] where ([status] = 'pending') or ([status] = 'partial')";
                DataTable dtActiveOrders = myHKeInvestData.getData(sql);
                if (dtActiveOrders == null) { return; } // If the DataSet is null, a SQL error occurred.
                // If no result is returned, then display a message that the account does not hold this type of security.
                if (dtActiveOrders.Rows.Count == 0)
                {
                    lblResultMessage3.Text = "No active orders in this account.";
                    lblResultMessage3.Visible = true;
                    gvBondTrustActiveOrders.Visible = false;
                    gvStockActiveOrders.Visible = false;
                    return;
                }

                sql = "select [referenceNumber], [buyOrSell], [type], [code], [name], [dateSubmitted], [status], [orderAmount], '' AS [shares] from [Invoice] where (([type] = 'bond' and [status] = 'pending') or ([type] = 'bond' and [status] = 'partial') or ([type] = 'unit trust' and [status] = 'pending') or ([type] = 'unit trust' and [status] = 'partial'))";
                DataTable dtBondTrustActiveOrders = myHKeInvestData.getData(sql);
                foreach (DataRow row in dtBondTrustActiveOrders.Rows)
                {
                    if (row["buyOrSell"].ToString().Trim() == "sell")
                    {
                        row["shares"] = row["orderAmount"];
                        row["orderAmount"] = DBNull.Value ;
                    }
                }

                sql = "select [referenceNumber], [buyOrSell], [type], [code], [name], [dateSubmitted], [status], [orderAmount], [limitPrice], [stopPrice], [expiryDay] from [Invoice] where (([type] = 'stock' and [status] = 'pending') or ([type] = 'stock' and [status] = 'partial'))";
                DataTable dtStockActiveOrders = myHKeInvestData.getData(sql);

                // Set the initial sort expression and sort direction for sorting the GridView in ViewState.
                ViewState["SortExpression"] = "dateSubmitted";
                ViewState["SortDirection"] = "DESC";

                // Bind the GridView to the DataTable.
                gvBondTrustActiveOrders.DataSource = dtBondTrustActiveOrders;
                gvBondTrustActiveOrders.DataBind();

                gvStockActiveOrders.DataSource = dtStockActiveOrders;
                gvStockActiveOrders.DataBind();

                // Set the visibility of controls and GridView data.
                gvBondTrustActiveOrders.Visible = true;
                gvStockActiveOrders.Visible = true;
            }
            // Display order history
            else if (securityType == "history")
            {
                DateTime thisDay = DateTime.Today;
                // Display the date only.
                DateStart.Text = thisDay.ToString("dd/MM/yyyy");
                DateEnd.Text = thisDay.ToString("dd/MM/yyyy");

                pnHistory.Visible = true;
                pnRange.Visible = true;
                if (searchAccount(accountNumber) == false) { return; }
                string start = DateTime.Parse(DateStart.Text.Trim(), CultureInfo.GetCultureInfo("en-GB")).ToString(CultureInfo.GetCultureInfo("en-GB").DateTimeFormat.ShortDatePattern).Trim();
                string end = DateTime.Parse(DateEnd.Text.Trim(), CultureInfo.GetCultureInfo("en-GB")).AddDays(1).ToString(CultureInfo.GetCultureInfo("en-GB").DateTimeFormat.ShortDatePattern).Trim();

                sql = "select [referenceNumber], [buyOrSell], [type], [code], [name], [dateSubmitted], [status], [orderAmount] AS [totalShares], [orderAmount] AS [executedAmount], [serviceFee], [transactionNumber], [executeShares], [executePrice] from [Invoice] where ([status] = 'completed' or [status] = 'cancelled' or [status] = 'partial') and [dateSubmitted] >= CONVERT(date, '" + start + "', 103) and [dateSubmitted] <= CONVERT(date, '" + end + "', 103);";

                DataTable dtOrderHistory = myHKeInvestData.getData(sql);
                if (dtOrderHistory == null) { return; } // If the DataSet is null, a SQL error occurred.
                
                // If no result is returned, then display a message that the account does not hold this type of security.
                if (dtOrderHistory.Rows.Count == 0)
                {
                    lblResultMessage4.Text = "No order history for this period.";
                    lblResultMessage4.Visible = true;
                    gvOrderHistory.Visible = false;
                    return;
                }

                sql = "SELECT [code], SUM([executeShares]) AS [totalShares], SUM([executeShares]*[executePrice]) AS [totalAmount] FROM Invoice WHERE (([status] = 'completed' OR [status] = 'cancelled' or [status] = 'partial') AND [dateSubmitted] >= CONVERT(date, '" + start + "', 103) AND [dateSubmitted] <= CONVERT(date, '" + end + "', 103)) GROUP BY [code];";

                DataTable dtTransaction = myHKeInvestData.getData(sql);

                foreach (DataRow row in dtOrderHistory.Rows)
                {
                    foreach (DataRow sumrow in dtTransaction.Rows)
                    {
                        if (row["code"].ToString().Trim() == sumrow["code"].ToString().Trim())
                        {
                            row["totalShares"] = sumrow["totalShares"];
                        }
                        if (row["code"].ToString().Trim() == sumrow["code"].ToString().Trim() && row["type"].ToString().Trim() == "stock")
                        {
                            row["executedAmount"] = Convert.ToDecimal(sumrow["totalAmount"]) * myHKeInvestService.getCurrency(row["type"].ToString().Trim(), row["code"].ToString().Trim());
                        }
                    }
                }

                // Set the initial sort expression and sort direction for sorting the GridView in ViewState.
                ViewState["SortExpression"] = "dateSubmitted";
                ViewState["SortDirection"] = "DESC";

                // Bind the GridView to the DataTable.
                gvOrderHistory.DataSource = dtOrderHistory;
                gvOrderHistory.DataBind();

                // Set the visibility of controls and GridView data.
                gvOrderHistory.Visible = true;
                ddlBuyOrSell.SelectedIndex = 0;
                ddlType.SelectedIndex = 0;
                Code.Text = "";
                ddlStatus.SelectedIndex = 0;
            }
            else
            {
                pnResult.Visible = true;
                if (searchAccount(accountNumber) == false) { return; }

                // *****************************************************************************************************************************
                // TODO: Construct the SQL select statement to get the code, name, shares and base of the security holdings of a specific type *
                //       in an account. The select statement should also return three additonal columns -- price, value and convertedValue --  *
                //       whose values are not actually in the database, but are set to the constant 0.00 by the select statement. (HINT: see   *
                //       http://stackoverflow.com/questions/2504163/include-in-select-a-column-that-isnt-actually-in-the-database.)            *   
                // *****************************************************************************************************************************
                sql = "select [code], [name], [shares], [base], 0.00 AS [price], 0.00 AS [value], 0.00 AS [convertedValue] from [SecurityHolding] where ([accountNumber] = '" + accountNumber + "') and ([type]='" + securityType + "')"; // Complete the SQL statement.

                DataTable dtSecurityHolding = myHKeInvestData.getData(sql);
                if (dtSecurityHolding == null) { return; } // If the DataSet is null, a SQL error occurred.

                // If no result is returned, then display a message that the account does not hold this type of security.
                if (dtSecurityHolding.Rows.Count == 0)
                {
                    lblResultMessage.Text = "No " + securityType + "s held in this account.";
                    lblResultMessage.Visible = true;
                    gvSecurityHolding.Visible = false;
                    return;
                }
                // For each security in the result, get its current price from an external system, calculate the total value
                // of the security and change the current price and total value columns of the security in the result.
                int dtRow = 0;
                foreach (DataRow row in dtSecurityHolding.Rows)
                {
                    string securityCode = row["code"].ToString();
                    decimal shares = Convert.ToDecimal(row["shares"]);
                    decimal price = myExternalFunctions.getSecuritiesPrice(securityType, securityCode);
                    decimal value = Math.Round(shares * price - (decimal).005, 3);
                    dtSecurityHolding.Rows[dtRow]["price"] = price;
                    dtSecurityHolding.Rows[dtRow]["value"] = value;
                    dtRow = dtRow + 1;
                }

                // Set the initial sort expression and sort direction for sorting the GridView in ViewState.
                ViewState["SortExpression"] = "name";
                ViewState["SortDirection"] = "ASC";

                // Bind the GridView to the DataTable.
                gvSecurityHolding.DataSource = dtSecurityHolding;
                gvSecurityHolding.DataBind();

                // Set the visibility of controls and GridView data.
                gvSecurityHolding.Visible = true;
                ddlCurrency.Visible = true;
                gvSecurityHolding.Columns[myHKeInvestCode.getColumnIndexByName(gvSecurityHolding, "convertedValue")].Visible = false;
            }
        }

        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnRange.Visible = false;
            // Get the index value of the convertedValue column in the GridView using the helper method "getColumnIndexByName".
            int convertedValueIndex = myHKeInvestCode.getColumnIndexByName(gvSecurityHolding, "convertedValue");

            // Get the currency to convert to from the ddlCurrency dropdownlist.
            // Hide the converted currency column if no currency is selected.
            string toCurrency = ddlCurrency.SelectedValue;
            if (toCurrency == "0")
            {
                gvSecurityHolding.Columns[convertedValueIndex].Visible = false;
                return;
            }

            // Make the convertedValue column visible and create a DataTable from the GridView.
            // Since a GridView cannot be updated directly, it is first loaded into a DataTable using the helper method 'unloadGridView'.
            gvSecurityHolding.Columns[convertedValueIndex].Visible = true;
            DataTable dtSecurityHolding = myHKeInvestCode.unloadGridView(gvSecurityHolding);

            // ***********************************************************************************************************
            // TODO: For each row in the DataTable, get the base currency of the security, convert the current value to  *
            //       the selected currency and assign the converted value to the convertedValue column in the DataTable. *
            // ***********************************************************************************************************
            int dtRow = 0;
            foreach (DataRow row in dtSecurityHolding.Rows)
            {

                string baseCurrency = row["base"].ToString();
                string targetCurrency = ddlCurrency.SelectedValue.Trim();
                DataTable dtCurrencyData = (DataTable)ViewState["dtCurrencyData"];
                decimal baseCurrencyRate = 0;
                decimal targetCurrencyRate = 0;
                foreach (DataRow baseRow in dtCurrencyData.Select("currency = '" + baseCurrency + "'"))
                {
                    baseCurrencyRate = baseCurrencyRate + (decimal)(baseRow["rate"]);
                }
                foreach (DataRow targetRow in dtCurrencyData.Select("currency = '" + targetCurrency + "'"))
                {
                    targetCurrencyRate = targetCurrencyRate + (decimal)(targetRow["rate"]);
                }

                decimal value = Convert.ToDecimal(row["value"]);
                decimal convertedValue = (decimal)(value * baseCurrencyRate / targetCurrencyRate);

                dtSecurityHolding.Rows[dtRow]["convertedValue"] = convertedValue;
                dtRow = dtRow + 1;
            }

            // Change the header text of the convertedValue column to indicate the currency. 
            gvSecurityHolding.Columns[convertedValueIndex].HeaderText = "Value in " + toCurrency;

            // Bind the DataTable to the GridView.
            gvSecurityHolding.DataSource = dtSecurityHolding;
            gvSecurityHolding.DataBind();
        }

        protected void gvSecurityHolding_Sorting(object sender, GridViewSortEventArgs e)
        {
            // Since a GridView cannot be sorted directly, it is first loaded into a DataTable using the helper method 'unloadGridView'.
            // Create a DataTable from the GridView.
            DataTable dtSecurityHolding = myHKeInvestCode.unloadGridView(gvSecurityHolding);

            // Set the sort expression in ViewState for correct toggling of sort direction,
            // Sort the DataTable and bind it to the GridView.
            string sortExpression = e.SortExpression.ToLower();
            ViewState["SortExpression"] = sortExpression;
            dtSecurityHolding.DefaultView.Sort = sortExpression + " " + myHKeInvestCode.getSortDirection(ViewState, e.SortExpression);
            dtSecurityHolding.AcceptChanges();

            // Bind the DataTable to the GridView.
            gvSecurityHolding.DataSource = dtSecurityHolding.DefaultView;
            gvSecurityHolding.DataBind();
        }

        private decimal calculateSecurityVolume(string securityType, string accountNumber)
        {
            decimal volume = -1;
            DataTable dtSecurity = myHKeInvestData.getData("select * from [SecurityHolding] where [accountNumber] ='" + accountNumber + "' and [type] = '" + securityType + "'");

            if (dtSecurity == null || dtSecurity.Rows.Count == 0) return volume;
            else
            {
                volume = 0;
                foreach (DataRow row in dtSecurity.Rows)
                {
                    string type = row["type"].ToString().Trim();
                    string code = row["code"].ToString().Trim();
                    decimal shares = Decimal.Parse(row["shares"].ToString().Trim());
                    decimal price = myHKeInvestService.convertPrice(type, code);
                    volume = volume + Convert.ToDecimal(shares * price);
                }
            }

            return Math.Round(volume, 3);
        }

        protected void Date_TextChanged(object sender, EventArgs e)
        {
            string start = DateTime.Parse(DateStart.Text.Trim(), CultureInfo.GetCultureInfo("en-GB")).ToString(CultureInfo.GetCultureInfo("en-GB").DateTimeFormat.ShortDatePattern).Trim();
            string end = DateTime.Parse(DateEnd.Text.Trim(), CultureInfo.GetCultureInfo("en-GB")).AddDays(1).ToString(CultureInfo.GetCultureInfo("en-GB").DateTimeFormat.ShortDatePattern).Trim();

            string sql = "select [referenceNumber], [buyOrSell], [type], [code], [name], [dateSubmitted], [status], [orderAmount] AS [totalShares], orderAmount AS [executedAmount], [serviceFee], [transactionNumber], [executeShares], [executePrice] from [Invoice] where ([status] = 'completed' or [status] = 'cancelled' or [status] = 'partial') and [dateSubmitted] >= CONVERT(date, '" + start + "', 103) and [dateSubmitted] <= CONVERT(date, '" + end + "', 103);";

            DataTable dtOrderHistory = myHKeInvestData.getData(sql);
            if (dtOrderHistory == null) { return; } // If the DataSet is null, a SQL error occurred.
            
            // If no result is returned, then display a message that the account does not hold this type of security.
            if (dtOrderHistory.Rows.Count == 0)
            {
                lblResultMessage4.Text = "No order history for this period.";
                lblResultMessage4.Visible = true;
                gvOrderHistory.Visible = false;
                return;
            }

            sql = "SELECT [code], SUM([executeShares]) AS [totalShares], SUM([executeShares]*[executePrice]) AS [totalAmount] FROM Invoice WHERE (([status] = 'completed' OR [status] = 'cancelled' or [status] = 'partial') AND [dateSubmitted] >= CONVERT(date, '" + start + "', 103) AND [dateSubmitted] <= CONVERT(date, '" + end + "', 103)) GROUP BY [code];";

            DataTable dtTransaction = myHKeInvestData.getData(sql);

            foreach (DataRow row in dtOrderHistory.Rows)
            {
                foreach (DataRow sumrow in dtTransaction.Rows)
                {
                    if (row["code"].ToString().Trim() == sumrow["code"].ToString().Trim())
                    {
                        row["totalShares"] = sumrow["totalShares"];
                    }
                    if (row["code"].ToString().Trim() == sumrow["code"].ToString().Trim() && row["type"].ToString().Trim() == "stock")
                    {
                        row["executedAmount"] = Convert.ToDecimal(sumrow["totalAmount"]) * myHKeInvestService.getCurrency(row["type"].ToString().Trim(), row["code"].ToString().Trim());
                    }
                }
            }

            // Set the initial sort expression and sort direction for sorting the GridView in ViewState.
            ViewState["SortExpression"] = "dateSubmitted";
            ViewState["SortDirection"] = "DESC";

            // Bind the GridView to the DataTable.
            gvOrderHistory.DataSource = dtOrderHistory;
            gvOrderHistory.DataBind();

            // Set the visibility of controls and GridView data.
            gvOrderHistory.Visible = true;
            ddlBuyOrSell.SelectedIndex = 0;
            ddlType.SelectedIndex = 0;
            Code.Text = "";
            ddlStatus.SelectedIndex = 0;
        }

        protected void gvOrderHistory_Sorting(object sender, GridViewSortEventArgs e)
        {
            // Since a GridView cannot be sorted directly, it is first loaded into a DataTable using the helper method 'unloadGridView'.
            // Create a DataTable from the GridView.
            DataTable dtOrderHistory = myHKeInvestCode.unloadGridView(gvOrderHistory);

            // Set the sort expression in ViewState for correct toggling of sort direction,
            // Sort the DataTable and bind it to the GridView.
            string sortExpression = e.SortExpression.ToLower();
            ViewState["SortExpression"] = sortExpression;
            dtOrderHistory.DefaultView.Sort = sortExpression + " " + myHKeInvestCode.getSortDirection(ViewState, e.SortExpression);
            dtOrderHistory.AcceptChanges();

            // Bind the DataTable to the GridView.
            gvOrderHistory.DataSource = dtOrderHistory.DefaultView;
            gvOrderHistory.DataBind();
        }

        protected void filter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlType.SelectedIndex == 0)
            {
                Code.Enabled = false;
                Code.Text = "";
            }
            else
            {
                Code.Enabled = true;
            }
            string start = DateTime.Parse(DateStart.Text.Trim(), CultureInfo.GetCultureInfo("en-GB")).ToString(CultureInfo.GetCultureInfo("en-GB").DateTimeFormat.ShortDatePattern).Trim();
            string end = DateTime.Parse(DateEnd.Text.Trim(), CultureInfo.GetCultureInfo("en-GB")).AddDays(1).ToString(CultureInfo.GetCultureInfo("en-GB").DateTimeFormat.ShortDatePattern).Trim();

            string sql = "select [referenceNumber], [buyOrSell], [type], [code], [name], [dateSubmitted], [status], [orderAmount] AS [totalShares], orderAmount AS [executedAmount], [serviceFee], [transactionNumber], [executeShares], [executePrice] from [Invoice] where ([status] = 'completed' or [status] = 'cancelled' or [status] = 'partial') and [dateSubmitted] >= CONVERT(date, '" + start + "', 103) and [dateSubmitted] <= CONVERT(date, '" + end + "', 103);";

            DataTable dtOrderHistory = myHKeInvestData.getData(sql);
            if (dtOrderHistory == null) { return; } // If the DataSet is null, a SQL error occurred.

            // If no result is returned, then display a message that the account does not hold this type of security.
            if (dtOrderHistory.Rows.Count == 0)
            {
                lblResultMessage4.Text = "No order history for this period.";
                lblResultMessage4.Visible = true;
                gvOrderHistory.Visible = false;
                return;
            }

            sql = "SELECT [code], SUM([executeShares]) AS [totalShares], SUM([executeShares]*[executePrice]) AS [totalAmount] FROM Invoice WHERE (([status] = 'completed' OR [status] = 'cancelled' or [status] = 'partial') AND [dateSubmitted] >= CONVERT(date, '" + start + "', 103) AND [dateSubmitted] <= CONVERT(date, '" + end + "', 103)) GROUP BY [code];";

            DataTable dtTransaction = myHKeInvestData.getData(sql);

            foreach (DataRow row in dtOrderHistory.Rows)
            {
                foreach (DataRow sumrow in dtTransaction.Rows)
                {
                    if (row["code"].ToString().Trim() == sumrow["code"].ToString().Trim())
                    {
                        row["totalShares"] = sumrow["totalShares"];
                    }
                    if (row["code"].ToString().Trim() == sumrow["code"].ToString().Trim() && row["type"].ToString().Trim() == "stock")
                    {
                        row["executedAmount"] = Convert.ToDecimal(sumrow["totalAmount"]) * myHKeInvestService.getCurrency(row["type"].ToString().Trim(), row["code"].ToString().Trim());
                    }
                }
            }

            DataView dv = new DataView(dtOrderHistory);
            string filter = "";
            if (ddlBuyOrSell.SelectedIndex != 0)
            {
                filter = filter + "buyOrSell = '" + ddlBuyOrSell.SelectedValue + "'";
            }
            if (ddlType.SelectedIndex != 0)
            {
                if (filter.Length > 0)
                {
                    filter = filter + " and ";
                }
                filter = filter + "type = '" + ddlType.SelectedValue + "'";
                if (Code.Text.Trim() != "")
                {
                    filter = filter + " and code = '" + Code.Text.Trim().Replace("'", "") + "'";
                }
            }
            if (ddlStatus.SelectedIndex != 0)
            {
                if (filter.Length > 0)
                {
                    filter = filter + " and ";
                }
                filter = filter + "status = '" + ddlStatus.SelectedValue + "'";
            }
            dv.RowFilter = filter;
            ViewState["SortExpression"] = "dateSubmitted";
            ViewState["SortDirection"] = "DESC";
            gvOrderHistory.DataSource = dv;
            gvOrderHistory.DataBind();
        }
    }
}