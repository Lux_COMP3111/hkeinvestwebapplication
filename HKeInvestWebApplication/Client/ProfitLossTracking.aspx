﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProfitLossTracking.aspx.cs" Inherits="HKeInvestWebApplication.Client.ProfitLossTracking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-header"><h2> Securities Profit Loss Tracking (Currency: HKD) </h2></div>
    <%--Largest Div Element--%>
    <div class="form-horizontal panel-group">
        <%--Total Profit / Loss Div Element--%>
        <asp:Panel runat="server" ID="pnTotalPL" CssClass="panel panel-default">
            <div class="panel-heading">Total Sum of Profits and Losses for All Securities</div>
            <div class="panel-body">  
                <%--Client Name and Result Message Labels--%>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" ID="lblClientName" Visible="false" CssClass="label label-primary"></asp:Label>
                    <asp:Label runat="server" ID="lblResultMessage" Visible="false" CssClass="label label-danger"></asp:Label>
                </div>
                <div class="form-group col-md-12">
                    <%--Gridview to display query results--%>
                    <asp:GridView runat="server" ID="gvTotalPL" Visible="False" AutoGenerateColumns="False" OnSorting="gvTotalPL_Sorting" CssClass="table table-bordered table-condensed table-hover">
                        <Columns>
                            <asp:BoundField DataField="totalbuyamount" DataFormatString="{0:n2}" HeaderText="Total Buy Amount" ReadOnly="True" SortExpression="code" />
                            <asp:BoundField DataField="totalsellamount" DataFormatString="{0:n2}" HeaderText="Total Sell Amount" ReadOnly="True" SortExpression="name" />
                            <asp:BoundField DataField="totalfeespaid" DataFormatString="{0:n2}" HeaderText="Total Fees Paid" ReadOnly="True" />
                            <asp:BoundField DataField="totalpl" DataFormatString="{0:n2}" HeaderText="Total Profit / Loss" ReadOnly="True" />
                            <asp:BoundField DataField="currentBalance" DataFormatString="{0:n2}" HeaderText="Current Balance" ReadOnly="True" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </asp:Panel>

        <asp:Panel runat="server" ID="pnTypePL" CssClass="panel panel-default">
            <div class="panel-heading">Profits and Losses for Each Type of Securities</div>
            <div class="panel-body">  
                <%--Client Name and Result Message Labels--%>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" ID="Label1" Visible="false" CssClass="label label-primary"></asp:Label>
                    <asp:Label runat="server" ID="Label2" Visible="false" CssClass="label label-danger"></asp:Label>
                </div>
                <div class="form-group col-md-12">
                    <%--Gridview to display query results--%>
                    <asp:GridView runat="server" ID="gvTypePL" Visible="False" AutoGenerateColumns="False" CssClass="table table-bordered table-condensed table-hover">
                        <Columns>
                            <asp:BoundField DataField="type" HeaderText="Security Type" ReadOnly="True"/>
                            <asp:BoundField DataField="typetotalbuyamount" DataFormatString="{0:n2}" HeaderText="Total Buy Amount" ReadOnly="True"/>
                            <asp:BoundField DataField="typetotalsellamount" DataFormatString="{0:n2}" HeaderText="Total Sell Amount" ReadOnly="True" />
                            <asp:BoundField DataField="typetotalfeespaid" DataFormatString="{0:n2}" HeaderText="Total Fees Paid" ReadOnly="True" />
                            <asp:BoundField DataField="typetotalpl" DataFormatString="{0:n2}" HeaderText="Total Profit / Loss" ReadOnly="True" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </asp:Panel>

        <asp:Panel runat="server" ID="pnIndividualPL" CssClass="panel panel-default">
            <div class="panel-heading">Profits and Losses for Individual Securities</div>
            <div class="panel-body">  
                <%--Client Name and Result Message Labels--%>
                <div class="form-group col-md-12">
                    <asp:Label runat="server" ID="Label3" Visible="false" CssClass="label label-primary"></asp:Label>
                    <asp:Label runat="server" ID="Label4" Visible="false" CssClass="label label-danger"></asp:Label>
                </div>
                <div class="form-group col-md-12">
                    <%--Gridview to display query results--%>
                    <asp:GridView runat="server" ID="gvIndividualPL" Visible="False" AutoGenerateColumns="False" OnSorting="gvIndividualPL_Sorting" CssClass="table table-bordered table-condensed table-hover">
                        <Columns>
                            <asp:BoundField DataField="type" HeaderText="Security Type" ReadOnly="True" SortExpression="type" />
                            <asp:BoundField DataField="code" HeaderText="Security Code" ReadOnly="True" />
                            <asp:BoundField DataField="name" HeaderText="Security Name" ReadOnly="True" />
                            <asp:BoundField DataField="shares" HeaderText="Shares Hold" ReadOnly="True" />
                            <asp:BoundField DataField="totalbuyamount" HeaderText="Total Buy Amount" ReadOnly="True" />
                            <asp:BoundField DataField="totalsellamount" HeaderText="Total Sell Amount" ReadOnly="True" />
                            <asp:BoundField DataField="totalfeespaid" HeaderText="Total Fees Paid" ReadOnly="True" />
                            <asp:BoundField DataField="totalpl" HeaderText="Total Profit / Loss" ReadOnly="True" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </asp:Panel>
    </div>




</asp:Content>
