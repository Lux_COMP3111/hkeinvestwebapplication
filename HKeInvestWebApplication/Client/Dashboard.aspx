﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="HKeInvestWebApplication.Client.ClientMain" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-header">
        <h2>Client Dashboard</h2>
        <asp:Label runat="server" ID="lblClientAccountNumber" CssClass="control-label label label-success" Font-Size="Small">Account Number: KI00000001</asp:Label>
        <asp:Label runat="server" ID="lblClientName" Visible="false" CssClass="label label-info" Font-Size="Small"></asp:Label>
    </div>
    <div class="jumbotron">
        <h2>jumbotron placeholder</h2>
    </div>
    <hr />
    <h4>Client Service</h4>
    <asp:HyperLink runat="server" Text="Search Securities" NavigateUrl="~/SecuritySearch.aspx"></asp:HyperLink>
    <br />
    <asp:HyperLink runat="server" Text="Trade Securities" NavigateUrl="~/Client/TradeSecurity.aspx"></asp:HyperLink>
    <br />
    <asp:HyperLink runat="server" Text="Best Seller" NavigateUrl="~/Client/BestSeller.aspx"></asp:HyperLink>
    <br />
    <asp:HyperLink runat="server" Text="Profit/Loss Tracking" NavigateUrl="~/Client/ProfitLossTracking.aspx"></asp:HyperLink>
    <br />
    <asp:HyperLink runat="server" Text="Currency Conversion" NavigateUrl="~/Client/CurrencyConverter.aspx"></asp:HyperLink>
    <br />
    <asp:HyperLink runat="server" Text="Alarm Setting" NavigateUrl="~/Client/Alert.aspx"></asp:HyperLink>
    <br />
    <asp:HyperLink runat="server" Text="Report Generation" NavigateUrl="~/Client/ReportGeneration.aspx"></asp:HyperLink>
</asp:Content>
