﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using HKeInvestWebApplication.Code_File;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using Microsoft.AspNet.Identity;

namespace HKeInvestWebApplication.Client
{
    public partial class ClientMain : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HKeInvestData myHKeInvestData = new HKeInvestData();
            HKeInvestCode myHKeInvestCode = new HKeInvestCode();
            ExternalFunctions myExternalFunctions = new ExternalFunctions();
            if (!Page.IsPostBack)
            {
                string userName = Context.User.Identity.GetUserName();
                string sql = "select [accountNumber] from [Account] where [userName] ='" + userName + "';";
                string accountNumber = "";

                DataTable dtTemp = myHKeInvestData.getData(sql);
                if (!(dtTemp == null))
                {
                    accountNumber = dtTemp.Rows[0]["accountNumber"].ToString().Trim();
                }

                lblClientAccountNumber.Text = "Account Number: " + accountNumber;
                lblClientAccountNumber.Visible = true;

                sql = "select [lastName], [firstName] from [Client] where ([accountNumber]='" + accountNumber + "')";

                DataTable dtClient = myHKeInvestData.getData(sql);
                string clientName = "Client(s): ";
                int i = 1;
                foreach (DataRow row in dtClient.Rows)
                {
                    clientName = clientName + row["lastName"] + ", " + row["firstName"];
                    if (dtClient.Rows.Count != i)
                    {
                        clientName = clientName + "and ";
                    }
                    i = i + 1;
                }
                lblClientName.Text = clientName;
                lblClientName.Visible = true;
            }
        }
    }
}