﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="HKeInvestWebApplication._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron"><h2>jumbotron placeholder</h2></div>
    <hr />
    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-md-4">
                <asp:Panel runat="server" ID="pnAccounts" CssClass="panel panel-primary">
                    <div class="panel-heading">Accounts & Service</div>
                    <div class="panel-body">
                        <div class="list-group">
                            <a runat="server" href="#" class="list-group-item">Securities Portfolio Account</a>
                            <a runat="server" href="AccountApplicationForm.pdf" class="list-group-item">Securities Account Application<span class="badge">pdf</span></a>
                            <a runat="server" href="HKeInvestFees.pdf" class="list-group-item">HKeInvest Service Fees<span class="badge">pdf</span></a>
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <div class="col-md-4">
                <asp:Panel runat="server" ID="pnInvestments" CssClass="panel panel-primary">
                    <div class="panel-heading">Investments</div>
                    <div class="panel-body">
                        <div class="list-group">
                            <a href="SecuritySearch.aspx" class="list-group-item">Search Securities</a>
                            <a href="Client/BestSeller.aspx" class="list-group-item">Monthly Best Sellers<span class="badge">client only</span></a>
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <div class="col-md-4">
                <asp:Panel runat="server" ID="pnCalculators" CssClass="panel panel-primary">
                    <div class="panel-heading">Calculators</div>
                    <div class="panel-body">
                        <div class="list-group">
                            <a runat="server" href="~/Client/CurrencyConverter" class="list-group-item">Currency Conversion<span class="badge">client only</span></a>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-4">
                <asp:Panel runat="server" ID="pnTrading" CssClass="panel panel-primary">
                    <div class="panel-heading">Trading</div>
                    <div class="panel-body">
                        <div class="list-group">
                            <a runat="server" href="~/Client/TradeSecurity.aspx" class="list-group-item">Trade Securities<span class="badge">client only</span></a>
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <div class="col-md-4">
                <asp:Panel runat="server" ID="pnClients" CssClass="panel panel-primary">
                    <div class="panel-heading">For Clients</div>
                    <div class="panel-body">
                        <div class="list-group">
                            <a href="#" class="list-group-item">Account Management<span class="badge">client only</span></a>
                            <a href="#" class="list-group-item">Portfolio Management<span class="badge">client only</span></a>
                            <a href="#" class="list-group-item">Client Support Tools<span class="badge">client only</span></a>
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <div class="col-md-4">
                <asp:Panel runat="server" ID="pnEmployee" CssClass="panel panel-primary">
                    <div class="panel-heading">For Employees</div>
                    <div class="panel-body">
                        <div class="list-group">
                            <a href="Employee/EmployeeMain.aspx" class="list-group-item">Client Management Tools<span class="badge">employee only</span></a>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
    <hr />

</asp:Content>
