﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="HKeInvestWebApplication.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Your application description page.</h3>
    <p>Use this area to provide additional information.</p>
    <h3>Additional Feature: Search Best Selling Security</h3>
    <h4>Basic Description</h4>
    <p>Search best selling security function is offered exclusively for clients to help them in making security trading
    decisions. When executed, it will automatically compute, query and display top ten best selling securities of the
    month in decreasing order of price per share value.<p>
    <h4>Additional Feature Value</h4>
    <p>1. Differentiates clients from public users by offering exclusive access to valuable information produced by the HKeInvest system.<p>
    <p>2. Feature can be implemented into the system with minimum changes conducted on the database and core program files.<p>
    <p>3. Can be used to implement other computed-value display functions such as 'search worst-selling security'.<p>
</asp:Content>
